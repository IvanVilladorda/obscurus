#ifndef _OBS_MONITOR_H_
#define _OBS_MONITOR_H_

#include <OgreSingleton.h>

class ObscurusMonitor : public Ogre::Singleton<ObscurusMonitor>
{
public:
  ObscurusMonitor() {};
  ~ObscurusMonitor() {};
  static ObscurusMonitor* getSingletonPtr(void);
  static ObscurusMonitor& getSingleton(void);

  void paint();
private:
};

#define gMonitor    ObscurusMonitor::getSingletonPtr()

#endif