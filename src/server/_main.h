// -- MAIN (SERVER)

// remember version format : A.B.R
#define _OBS_VERSION_ "0.0"

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#  include <WinSock2.h> // need to be included now, before windows.h
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  include <tchar.h>
#endif

#include <string>
#include <vector>

#include <OgrePlatform.h>

#include "log.h"
#include "preferences.h"
#include "data.h"
//#include "../base/input.h"
#include "timer.h"
#include "db.h"
#include "logics.h"
#include "network.h"

//#include "monitor.h"
