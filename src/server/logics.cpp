#include "logics.h"

template<> ObscurusLogics* Ogre::Singleton<ObscurusLogics>::msSingleton = 0;
ObscurusLogics* ObscurusLogics::getSingletonPtr(void) { return msSingleton; }
ObscurusLogics& ObscurusLogics::getSingleton(void) { if(!msSingleton) msSingleton = 0; return (*msSingleton); }

void ObscurusLogics::processMessages()
{
}
