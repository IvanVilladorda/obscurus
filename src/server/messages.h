#ifndef _OBS_LOGICS_MESSAGES_H_
#define _OBS_LOGICS_MESSAGES_H_

// all logics message are enumerated here :
enum ObsLogicMsgId
{
  //Graphics <-  Logic
  LOGICFRAME_FINISHED,
  GAME_ENTITY_ADDED,
  GAME_ENTITY_REMOVED,
  //Graphics <-> Logic
  GAME_ENTITY_SCHEDULED_FOR_REMOVAL_SLOT,
  //Graphics  -> Logic
  SDL_EVENT,

  NUM_MSG_IDS // end fleg
};

#endif