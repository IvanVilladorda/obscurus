#ifndef _OBS_LOGICS_H_
#define _OBS_LOGICS_H_

#include <OgreSingleton.h>

#include "messages.h"

class ObscurusLogics : public Ogre::Singleton<ObscurusLogics>
{
public:
  ObscurusLogics() {};
  ~ObscurusLogics() {};
  static ObscurusLogics* getSingletonPtr(void);
  static ObscurusLogics& getSingleton(void);

  void processMessages();
  
private:
};

#define gLogics    ObscurusLogics::getSingletonPtr()

#endif