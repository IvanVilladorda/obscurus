// -- MAIN (SERVER)
#include "_main.h"

#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/filesystem.hpp>
#include <boost/log/attributes/current_process_name.hpp>
#include <boost/thread.hpp>
namespace ip = boost::interprocess;
namespace fs = boost::filesystem;

// -- CLEANMAIN
void cleanMain()
{
  if(gDB)        delete gDB;
//  if(gMonitor)  delete gMonitor;
  if(gNetwork)  delete gNetwork;
//  if(gWorld)    delete gWorld;
  if(gLogics)    delete gLogics;
  if(gTimer)    delete gTimer;
//  if(gInput)    delete gInput;
//  if(gData)    delete gData;
  if(gPrefs)    delete gPrefs;
  if(gLogger)    delete gLogger;
}

// -- KILLEXEC
void killExec()
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  std::string sAppName = boost::log::attributes::current_process_name().get();
  MessageBoxA(NULL, "Runtime failure or interrupt.\nCheck log for more information.", sAppName.c_str(), MB_OK|MB_ICONERROR);
#else
    cerr << "Runtime failure or interrupt.\nCheck log for more information.";
#endif
  cleanMain();
}

// -- MAIN
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPI, LPSTR lpCmdLine, INT iShow)
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
int main(int argc, char **argv)
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
int main(int argc, char *argv[])
#endif
{
  fs::path sPath = boost::log::attributes::current_process_name().get();
  std::string sAppName = sPath.stem().string();
  std::string sExeName = sPath.string();

  // Process check, only one instance allowed
  ip::named_mutex m_Lock(ip::open_or_create, sAppName.c_str());
  ip::scoped_lock<ip::named_mutex> lock(m_Lock, ip::try_to_lock);
  if(!lock) return -1; // silent

  // Log init
  std::string sWhereToLog = sAppName + ".log";
  new ObscurusLog(sWhereToLog);

  // Preferences init
  std::string sIniFile = sAppName + ".ini";
  new ObscurusPrefs(sIniFile); // reads app ini file AND user.ini

  // Timer init
  new ObscurusTimer();

  // Network init
  new ObscurusNetwork();

  // Database init
  new ObscurusDB();

  // Logics init
  new ObscurusLogics();

  // World init
  //new ObscurusWorld();

  // Loop (no app class for server)
  bool bShutDown = false;
  while(!bShutDown)
  {
    gNetwork->listen(); // Network messaging
    gLogics->processMessages(); // Internal messaging
    //gWorld->process(); // Bring life for a tick (different from client's gWorld->update())
    gNetwork->talk(); // Network broadcast from Logics (and World)
    //if(bShutDown)
    // handle broadcast before closing
    boost::this_thread::yield();
  }

  cleanMain();
  return 0;
  //prout; // kill compilation directive (avoids linking stage), do not remove
}
