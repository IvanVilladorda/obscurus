#include "monitor.h"

template<> ObscurusMonitor* Ogre::Singleton<ObscurusMonitor>::msSingleton = 0;
ObscurusMonitor* ObscurusMonitor::getSingletonPtr(void) { return msSingleton; }
ObscurusMonitor& ObscurusMonitor::getSingleton(void) { if(!msSingleton) msSingleton = 0; return (*msSingleton); }

void ObscurusMonitor::paint()
{
}
