#ifndef _OBS_CURRENT_RENDERER_H_
#define _OBS_CURRENT_RENDERER_H_

#include <OgrePrerequisites.h>

#if OGRE_VERSION_MAJOR == 2
#  include "ogre.2.1/renderer.ogre.2.1.h"
#else
#  include "ogre.1.9/renderer.ogre.1.9.h"
#endif

#endif