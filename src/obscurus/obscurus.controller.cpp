// -- CONTROLLER
#include "obscurus.h"
#include "menu.h"
#include "preferences.h"
#include "data.h"
#include "logics.h"

void Obscurus::controlActions() // fill with actions and controllers' calls
{
  switch(eState)
  {
  case OBS::STATE_NONE:
    // set special actions
    gInput->setCtrl("quit", "escape");
    // set javascript binds
    //  - mainmenu
    //    - new      => listmastermods => listmods => launch
    //    - load    => listsaves => getdetails => launch
    //    - connect => register  => login => update => launch
    //    - options  => get => set
    //    - quit
    jsBindFunction("intro_ended", js_intro_ended, 0);
    jsBindFunction("mainmenu_new_listmastermods", js_mainmenu_new_listmastermods, 0);
    jsBindFunction("mainmenu_new_listmods", js_mainmenu_new_listmods, 1);
    jsBindFunction("mainmenu_new_launch", js_mainmenu_new_launch, 1);
    jsBindFunction("mainmenu_load_listsaves", js_mainmenu_load_listsaves, 0);
    jsBindFunction("mainmenu_load_getdetails", js_mainmenu_load_getdetails, 1);
    jsBindFunction("mainmenu_load_launch", js_mainmenu_load_launch, 1);
    jsBindFunction("mainmenu_connect_register", js_mainmenu_connect_register, 0);
    jsBindFunction("mainmenu_connect_login", js_mainmenu_connect_login, 0);
    jsBindFunction("mainmenu_connect_update", js_mainmenu_connect_update, 0);
    jsBindFunction("mainmenu_connect_launch", js_mainmenu_connect_launch, 0);
    jsBindFunction("mainmenu_options_set", js_mainmenu_options_set, 2);
    jsBindFunction("mainmenu_options_get", js_mainmenu_options_get, 1);
    jsBindFunction("mainmenu_quit", js_mainmenu_quit, 0);
    // - load screen
    jsBindFunction("launch_complete", js_launch_complete, 0);
    // - mask
    // launch the first controller
    ___intro();
    break;
  case OBS::STATE_INTRO:
    if(gInput->getCtrl("quit")) // interrupt intro from core
      gMenu->js("_terminateIntro()");
    break;
  case OBS::STATE_MAIN_MENU:
    break;
  case OBS::STATE_LOAD_MOD:
    ___loadmod();
    break;
  case OBS::STATE_START_GAME:
    ___checkStart();
    break;
  case OBS::STATE_GAME:
    if(gInput->getCtrl("quit")) bShutDown = true; // -------------------- TODO : remove me
    break;
  case OBS::STATE_EXIT:
    bShutDown = true;
    break;
  default:
    if(gInput->getCtrl("quit")) bShutDown = true; // -------------------- TODO : remove me
    break;
  }
}
// ---------------------------------------------------------------------------------------- INTRO
void Obscurus::___intro()
{
  gMenu->activate(true);
  if(gPrefs->get("SkipIntro") != true)
  {
    gMenu->load("file:///" + gPrefs->get("_FullPath") + "dat/htm/intro.html");
    eState = OBS::STATE_INTRO;
  }
  else
    ___mainmenu();
}
jsValue JS_CALL Obscurus::js_intro_ended(jsExecState es)
{
  gObscurus->___mainmenu();
  return jsUndefined();
}

// ---------------------------------------------------------------------------------------- MAINMENU
void Obscurus::___mainmenu()
{
  gMenu->activate(true);
  gMenu->load("file:///" + gPrefs->get("_FullPath") + "dat/htm/mainmenu.html");
  gMenu->setTransparency(false);
  eState = OBS::STATE_MAIN_MENU;
}
// ---------------------------------------------------------------------------------------- MAINMENU NEW
jsValue JS_CALL Obscurus::js_mainmenu_new_listmastermods(jsExecState es)
{
  gData->listMasterMods();
  for(size_t i = 0; i < gData->vMasterMods.size(); i++)
    gMenu->js("_insertMasterMod(" + std::to_string(i) + ", '" + gData->vMasterMods[i] + "')");
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_new_listmods(jsExecState es)
{
  size_t _modidx = jsToInt(es, jsArg(es, 0));
  gData->preloadModule(gData->vMasterMods[_modidx], true);
  for(size_t i = 0; i < gData->vMods.size(); i++)
    gMenu->js("_insertMod('" + gData->vMods[i].sName + "')");
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_new_launch(jsExecState es)
{
  size_t _modidx = jsToInt(es, jsArg(es, 0));
  gMenu->load("file:///" + gPrefs->get("_FullPath") + "dat/htm/loadscreen.html");
  gData->preloadModule(gData->vMasterMods[_modidx], true);
  gData->processLoad();
  gObscurus->eState = OBS::STATE_LOAD_MOD;
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- MAINMENU LOAD
jsValue JS_CALL Obscurus::js_mainmenu_load_listsaves(jsExecState es)
{
  gDB->listSaves();
  for(size_t i = 0; i < gDB->vSavegames.size(); i++)
  {
    gMenu->js("_insertSavegame(" + std::to_string(i) + ", '" + gDB->vSavegames[i].sName + "')");
    gMenu->js("_insertSaveScreenshot(" + std::to_string(i) + ", '" + gDB->vSavegames[i].rgbImage + "')");
  }
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_load_getdetails(jsExecState es)
{
  size_t _modidx = jsToInt(es, jsArg(es, 0));
  gDB->open(gDB->vSavegames[_modidx]);
  // list details
  // list mods
  gData->preloadModule(gDB->getMasterMod(), true);
  for(size_t i = 0; i < gData->vMods.size(); i++)
    gMenu->js("_insertSaveMod('" + gData->vMods[i].sName + "')");
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_load_launch(jsExecState es)
{
  size_t _modidx = jsToInt(es, jsArg(es, 0));
  gDB->open(gDB->vSavegames[_modidx]);
  gMenu->load("file:///" + gPrefs->get("_FullPath") + "dat/htm/loadscreen.html");
  gData->preloadModule(gDB->getMasterMod(), true);
  gData->processLoad();
  gObscurus->eState = OBS::STATE_LOAD_MOD;
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- MAINMENU CONNECT
jsValue JS_CALL Obscurus::js_mainmenu_connect_register(jsExecState es)
{
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_connect_login(jsExecState es)
{
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_connect_update(jsExecState es)
{
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_connect_launch(jsExecState es)
{
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- MAINMENU OPTIONS
jsValue JS_CALL Obscurus::js_mainmenu_options_get(jsExecState es)
{
  return jsUndefined();
}
jsValue JS_CALL Obscurus::js_mainmenu_options_set(jsExecState es)
{
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- MAINMENU QUIT
jsValue JS_CALL Obscurus::js_mainmenu_quit(jsExecState es)
{
  gObscurus->eState = OBS::STATE_EXIT;
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- LOADMOD
void Obscurus::___loadmod()
{
  float fProgress = gData->getLoadProgress();
  gMenu->js("_advanceBar('" + std::to_string(fProgress) + "')");
  gData->loadNextFile();
}
jsValue JS_CALL Obscurus::js_launch_complete(jsExecState es)
{
  gMenu->load("file:///" + gPrefs->get("_FullPath") + "dat/htm/mask.html");
  gMenu->setTransparency(true);
  gMenu->activate(false);
  // finalize data
  if(!gData->linkAllObjects())
  {
    gObscurus->eState = OBS::STATE_EXIT;
    gLog("Errors while linking data.", 'E');
    return jsUndefined();
  }
  gObscurus->eState = OBS::STATE_START_GAME;
  // register the player
  OBS::User* pUser = new OBS::User();
  pUser->ip = "127.0.0.1";
  pUser->name = "PLAYER";
  gLogics->registerUser(pUser);
  //gLogics->startScript("GameStart");
  long long id = gLogics->_createCharacter("Hero", "PP_northern_continent", 0, 0);
  //long long id = gLogics->_createCharacter("Hero", "test_World", 0, 0);
  gLogics->_grantCharacterToPlayer(id);
  gLogics->_forcePlayerToIncarnate(id);
  gLogics->_startGame("PLAYER");
  gLogics->vWorlds[0]->exportWorldMap("world_test.png");
  //gObscurus->eState = OBS::STATE_EXIT;
  gRenderer->buildMap(gLogics->vWorlds[0]);
  return jsUndefined();
}
// ---------------------------------------------------------------------------------------- CHECKSTART
void Obscurus::___checkStart()
{
  OBS::LogicsMsg* pMsg = gLogics->getMessage(OBS::DEST_CORE);
  if(pMsg)
  {
    if(pMsg->eId == OBS::MSG_ABORT_EXECUTION)
    {
      eState = OBS::STATE_EXIT;
      bShutDown = true;
    }
    else if(pMsg->eId == OBS::MSG_RETURN_TO_MAIN_MENU)
    {
      gPrefs->set("SkipIntro", "1");
      eState = OBS::STATE_INTRO;
      // require to destroy data & logics
      delete gLogics;
      delete gData;
      new ObscurusData();
      new ObscurusLogics();
    }
    else if(pMsg->eId == OBS::MSG_START_GAME)
    {
      // logics must ensure the prerequisites are met
      eState = OBS::STATE_GAME;
    }
    else
      gLog("CORE : Bad message routing.", 'W');
    delete pMsg;
  }
}

/*
in obscurus :
  jsBindFunction("functionCall", js_functionCall, 1); // 1 == param num
  jsValue JS_CALL Obscurus::js_functionCall(jsExecState es)
  {
    const char* param = jsToString(es, jsArg(es, 0));
    ___doSomething(); // triple underscore
    return jsUndefined();
  }
in js :
  function onFunctionCall() { // always place the interceptor process inside a function
    outputMsg('functionCall\n'); // tell client the name of element to intercept (can be writen on page load, inside a function too)
    functionCall(param.value); // touch off the interceptor now
    // if in event, prevent default behavior before returning
  }
give max liberty to ui, instead of monitoring it. just let it use controllers.
*/
