// -- CLIENT MAIN ENTRY POINT

#include <boost/predef.h>

// os specific headers
#if defined(BOOST_OS_WINDOWS)
#  include <WinSock2.h> // need to be included now, before windows.h
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  include <tchar.h>
#endif

// standardize DEBUG FLAG
#if defined(_DEBUG)
#  define DEBUG
#endif

#include <boost/filesystem.hpp>
#include <boost/log/attributes/current_process_name.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

// remember version format : A.BB
#define _OBS_VERSION_ "0.01"

#include "log.h"
#include "preferences.h"
#include "obscurus.h"

namespace fs = boost::filesystem;
namespace lo = boost::log;
namespace ip = boost::interprocess;

// -- MAIN
#ifdef BOOST_OS_WINDOWS
INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPI, LPSTR lpCmdLine, INT iShow)
#elif BOOST_OS_LINUX
int main(int argc, char **argv)
#elif BOOST_OS_MACOS
int main(int argc, char *argv[])
#endif
{
  // get process name
  std::string sAppName = fs::path(lo::attributes::current_process_name().get()).stem().string();
  // get executable name
  std::string sExeName = fs::path(lo::attributes::current_process_name().get()).string();
  // get base path (below bin)
  std::string sFull = fs::current_path().generic().string().substr(0, fs::current_path().generic().string().find_last_of('/')) + "/";

#ifndef DEBUG
  // do not allow more than one instance in runtime
  ip::named_mutex m_Lock(ip::open_or_create, sAppName.c_str());
  ip::scoped_lock<ip::named_mutex> lock(m_Lock, ip::try_to_lock);
  if(!lock) return -1; // half silent quit
#endif

  {// Log init
    std::string sWhereToLog = sAppName + ".log";
    new ObscurusLog(sWhereToLog);
  }

  {// Preferences init
    std::string sIniFile = sAppName + ".ini";
    new ObscurusPrefs(sIniFile); // reads app ini file AND user.ini
#ifdef DEBUG
    gPrefs->set("DebugMode", "1"); // can be set by ini file (display 'D' log entries)
#else
    if(gPrefs->get("DebugMode") == true)
      gLog("** DEBUG MODE IS ON ** ", 'W');
#endif
    gLogger->setDebugOutput(gPrefs->getAsBool("DebugMode"));
    // add valuable data in prefs
    gPrefs->set("_AppName", sAppName);
    gPrefs->set("_ExeName", sExeName);
    gPrefs->set("_FullPath", sFull);
    if(gPrefs->get("GameMode") == "EDITOR") gLog("Game Mode switching to EDITOR.", 'I');
    else gLog("Game Mode set to standard.", 'I');
  }

  {// save version
    gPrefs->set("_Version", _OBS_VERSION_);
  }

  // os operations
#ifdef BOOST_OS_WINDOWS
  {// XP detection
    OSVERSIONINFO osvi;
    memset(&osvi, 0, sizeof(OSVERSIONINFO));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    GetVersionEx(&osvi);
    if(osvi.dwMajorVersion < 6)
    {
      gLog("XP or less detected.", 'W');
      gPrefs->set("_XPDetected", "1");
    }
  }
  {// load wke.dll manually (need /DELAYLOAD:"wke.dll")
    std::wstring_convert<std::codecvt<wchar_t, char, std::mbstate_t>> conv;
    std::wstring wstr = conv.from_bytes(fs::current_path().string().substr(0, fs::current_path().string().find_last_of('\\')) + "\\dat\\bin\\wke.dll");
    LoadLibrary(wstr.c_str());
  }
#endif

  // Main application class creation and run
  new Obscurus();
  delete gObscurus;
  return 0;
  //prout; // kill compilation directive (avoids linking stage), do not remove
}
