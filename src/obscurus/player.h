#ifndef _OBS_PLAYER_H_
#define _OBS_PLAYER_H_
#include "singleton.h"
#include <vector>

struct ObscurusPlayerCharacter
{
};

struct ViewPoint
{
  float pos[3];
  float orientation[3];
};

class ObscurusPlayer : public Singleton<ObscurusPlayer>
{
public:
  ObscurusPlayer();
  ~ObscurusPlayer();
  static ObscurusPlayer* ObscurusPlayer::getSingleton();

  void update();
  void moveOnMap();
  ViewPoint oMapView;
private:
  std::vector<ObscurusPlayerCharacter> vCharacters;
};

#define gPlayer    ObscurusPlayer::getSingleton()

#endif