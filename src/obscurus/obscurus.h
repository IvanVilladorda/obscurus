#ifndef _OBS_APP_H_
#define _OBS_APP_H_
#include "singleton.h"
#  include <WinSock2.h> // need to be included now, before windows.h
#include <wke.h>

namespace OBS
{
  enum StateFlag
  {
    STATE_NONE,
    STATE_INTRO,
    STATE_MAIN_MENU,
    STATE_LOAD_MOD,
    STATE_START_GAME,
    STATE_GAME,

    STATE_LOGIN,
    STATE_PLACE,
    STATE_COMBAT,
    STATE_EXIT
  };

  enum MenuFlag
  {
    MENU_NONE,
    MENU_CHARACTER,
    MENU_INVENTORY,
    MENU_PARTY,
    MENU_ETC
  };
}

class Obscurus : public Singleton<Obscurus>
{
public:
  Obscurus();
  ~Obscurus();
  static Obscurus* Obscurus::getSingleton();

private:
  void init();
  void run();
  void controlActions();

  OBS::StateFlag eState;
  OBS::MenuFlag eMenu;
  bool bShutDown;

  // all controllers and binds to javascript :
  void ___intro();
  static jsValue JS_CALL js_intro_ended(jsExecState);
  void ___mainmenu();
  static jsValue JS_CALL js_mainmenu_new_listmastermods(jsExecState);
  static jsValue JS_CALL js_mainmenu_new_listmods(jsExecState);
  static jsValue JS_CALL js_mainmenu_new_launch(jsExecState);
  static jsValue JS_CALL js_mainmenu_load_listsaves(jsExecState);
  static jsValue JS_CALL js_mainmenu_load_getdetails(jsExecState);
  static jsValue JS_CALL js_mainmenu_load_launch(jsExecState);
  static jsValue JS_CALL js_mainmenu_connect_register(jsExecState);
  static jsValue JS_CALL js_mainmenu_connect_login(jsExecState);
  static jsValue JS_CALL js_mainmenu_connect_update(jsExecState);
  static jsValue JS_CALL js_mainmenu_connect_launch(jsExecState);
  static jsValue JS_CALL js_mainmenu_options_set(jsExecState);
  static jsValue JS_CALL js_mainmenu_options_get(jsExecState);
  static jsValue JS_CALL js_mainmenu_quit(jsExecState);
  static jsValue JS_CALL js_launch_complete(jsExecState);
  void ___loadmod();
  void ___checkStart();
};
#define gObscurus    Obscurus::getSingleton()

#endif
