#include "player.h"
#include "log.h"
#include "preferences.h"
#include "logics.h"
#include "input.h"
template<> ObscurusPlayer* Singleton<ObscurusPlayer>::inst = 0;
ObscurusPlayer* ObscurusPlayer::getSingleton(){ return inst; }

ObscurusPlayer::ObscurusPlayer()
{
  gLog("-*-*- Player Startup.", 'I');
  gInput->setCtrl("KEY_CAM_FORWARD", "numpad 5");
  gInput->setCtrl("KEY_CAM_BACKWARD", "numpad 2");
  gInput->setCtrl("KEY_CAM_PAN_LEFT", "numpad 1");
  gInput->setCtrl("KEY_CAM_PAN_RIGHT", "numpad 3");
  gInput->setCtrl("KEY_CAM_ROT_LEFT", "numpad 4");
  gInput->setCtrl("KEY_CAM_ROT_RIGHT", "numpad 6");
  gInput->setCtrl("KEY_CAM_ZOOM_IN", "wheel+");
  gInput->setCtrl("KEY_CAM_ZOOM_OUT", "wheel-");
  gInput->setCtrl("KEY_CAM_LOOK_UP", "-");
  gInput->setCtrl("KEY_CAM_LOOK_DOWN", "+");
  memset(&oMapView, 0, sizeof(oMapView));
}

ObscurusPlayer::~ObscurusPlayer()
{
  gLog("*-*-* Player Shutdown.", 'I');
}

void ObscurusPlayer::update()
{
  OBS::LogicsMsg* pMsg = gLogics->getMessage(OBS::DEST_PLAYER);
  if(pMsg)
  {
    if(pMsg->eId == OBS::MSG_ASSIGN_CHARACTER)
    {
      //OBS::LogicsMsgAssignCharacterToPlayer* ppMsg = (OBS::LogicsMsgAssignCharacterToPlayer*) pMsg;
      //LOCK_ENTITIES;
      // get name and other basic informations ...
    }
    else if(pMsg->eId == OBS::MSG_EMBODY_CHARACTER)
    {
      //OBS::LogicsMsgEmbodyCharacter* ppMsg = (OBS::LogicsMsgEmbodyCharacter*) pMsg;
      //LOCK_ENTITIES;
      // get all infos
    }
    else if(pMsg->eId == OBS::MSG_START_GAME)
    {
      //OBS::LogicsMsgEmbodyCharacter* ppMsg = (OBS::LogicsMsgEmbodyCharacter*) pMsg;
      //LOCK_ENTITIES;
      // get all infos
    }
    else
      gLog("PLAYER : Bad message routing.", 'W');
    delete pMsg;
  }
  oMapView.pos[0] *= 0.9f;
  oMapView.pos[1] *= 0.9f;
  oMapView.pos[2] *= 0.9f;
  oMapView.orientation[0] *= 0.9f;
  oMapView.orientation[1] *= 0.9f;
  oMapView.orientation[2] *= 0.9f;
}

void ObscurusPlayer::moveOnMap()
{
  if(gInput->getCtrl("KEY_CAM_FORWARD"))		oMapView.pos[2] -= 5.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_BACKWARD"))		oMapView.pos[2] += 5.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_PAN_LEFT"))		oMapView.pos[0] -= 3.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_PAN_RIGHT"))	oMapView.pos[0] += 3.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_ROT_LEFT"))		oMapView.orientation[0] = 45.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_ROT_RIGHT"))	oMapView.orientation[0] = -45.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_ZOOM_IN"))		oMapView.pos[1] -= 12.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_ZOOM_OUT"))		oMapView.pos[1] += 12.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_LOOK_UP"))		oMapView.orientation[1] = 20.0f * gTime;
  if(gInput->getCtrl("KEY_CAM_LOOK_DOWN"))	oMapView.orientation[1] = -20.0f * gTime;
}

//    if(pInputMgr->bUserRequestedWindowClosing) bShutDown = true;

