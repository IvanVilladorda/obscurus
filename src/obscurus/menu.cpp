// -- MENU
#include "menu.h"
#include "input.h"
#include <boost/algorithm/string.hpp>
template<> ObscurusMenu* Singleton<ObscurusMenu>::inst = 0;
ObscurusMenu* ObscurusMenu::getSingleton(){ return inst; }

// ~~ CONSTRUCTOR
ObscurusMenu::ObscurusMenu()
  :bActive(false)
{
  gLog("-*-*- Menu Startup.", 'I');
  std::string sStr = wkeVersionString();
  boost::replace_all(sStr, "\n", " - ");
  gLog("Menu is using WKE %s.", 'I', sStr.c_str());
  gRenderer->createWebPanel("WKE");

  wkeInit();
  pWebView = wkeCreateWebView();
  pWebView->setTransparent(false);
  pWebView->resize(gPrefs->getAsInt("_ActiveWidth"), gPrefs->getAsInt("_ActiveHeight"));
  pWebView->setDirty(true);
  pWebView->focus();

  gInput->addListener(this);
}

// ~~ DESTRUCTOR
ObscurusMenu::~ObscurusMenu()
{
  gLog("*-*-* Menu Shutdown.", 'I');
  pWebView->destroy();
  wkeShutdown();
}

void ObscurusMenu::paint()
{
  if(pWebView->isDirty())
  {
    gRenderer->renderWebPanel(pWebView, "WKE");
  }
}

void ObscurusMenu::activate(bool _activate)
{
  bActive = _activate;
  gRenderer->showWebPanel("WKE", _activate);
  if(_activate)  pWebView->focus();
  else  pWebView->unfocus();
}

// ~~ LOAD
void ObscurusMenu::load(std::string const& _url)
{
  std::wstring wstr = conv.from_bytes(_url);
  pWebView->loadURL(wstr.c_str());
}

// ~~ JS
void ObscurusMenu::js(std::string const& _code)
{
  std::wstring wstr = conv.from_bytes(_code);
  pWebView->runJS(wstr.c_str());
}

void ObscurusMenu::mouseMoved(const OIS::MouseEvent &arg)
{
  if(!bActive) return;
  if(arg.state.X.abs <= 0 || arg.state.Y.abs <= 0)  return;
  if(arg.state.X.abs >= arg.state.width || arg.state.Y.abs >= arg.state.height)  return;

  int flags = 0;
  if(arg.state.buttonDown(OIS::MB_Left))
    flags |= WKE_LBUTTON;

  if(arg.state.Z.rel)
    pWebView->mouseWheel(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.rel, flags);
  pWebView->mouseEvent(WKE_MSG_MOUSEMOVE, arg.state.X.abs, arg.state.Y.abs, flags);
}

void ObscurusMenu::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
  if(!bActive) return;
  if(arg.state.X.abs <= 0 || arg.state.Y.abs <= 0)  return;
  if(arg.state.X.abs >= arg.state.width || arg.state.Y.abs >= arg.state.height)  return;

  if(id == OIS::MB_Left)  pWebView->mouseEvent(WKE_MSG_LBUTTONDOWN, arg.state.X.abs, arg.state.Y.abs, 0);
  if(id == OIS::MB_Right)  pWebView->mouseEvent(WKE_MSG_RBUTTONDOWN, arg.state.X.abs, arg.state.Y.abs, 0);
}

void ObscurusMenu::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
  if(!bActive) return;
  if(arg.state.X.abs <= 0 || arg.state.Y.abs <= 0)  return;
  if(arg.state.X.abs >= arg.state.width || arg.state.Y.abs >= arg.state.height)  return;

  if(id == OIS::MB_Left)  pWebView->mouseEvent(WKE_MSG_LBUTTONUP, arg.state.X.abs, arg.state.Y.abs, 0);
  if(id == OIS::MB_Right)  pWebView->mouseEvent(WKE_MSG_RBUTTONUP, arg.state.X.abs, arg.state.Y.abs, 0);
}

void ObscurusMenu::keyPressed(const OIS::KeyEvent &arg)
{
  if(!bActive) return;
  UINT vk = VKMap[arg.key];
  if(vk && pWebView->keyDown(vk, 0, false))    return;
  if(arg.text >= 32)  pWebView->keyPress(arg.text, 0, false);
}

void ObscurusMenu::keyReleased(const OIS::KeyEvent &arg)
{
  if(!bActive) return;
  UINT vk = VKMap[arg.key];
  if(vk)  pWebView->keyUp(vk, 0, false);
}

