#include "obscurus.h"
#include "data.h"
#include "input.h"
#include "timer.h"
#include "logics.h"
//#include "world.h"
#include "network.h"
#include "player.h"
#include "renderer.h"
#include "menu.h"
#include "db.h"
#include "preferences.h"
#include <boost/thread/thread.hpp>
template<> Obscurus* Singleton<Obscurus>::inst = 0;
Obscurus* Obscurus::getSingleton(){ return inst; }

// -- CONSTRUCTOR
Obscurus::Obscurus()
  : bShutDown(false),
  eState(OBS::STATE_NONE),
  eMenu(OBS::MENU_NONE)
{
  gLog("-*-*- Obscurus Startup.", 'I');
  gLog("Obscurus is using Boost %s.", 'I', BOOST_LIB_VERSION);
  init();
  run();
}

// -- DESTRUCTOR
Obscurus::~Obscurus()
{
  gLog("*-*-* Obscurus General Shutdown.", 'I');
  gLog("Good bye.", 'I');
  if(gRenderer)  delete gRenderer;
  if(gPlayer)    delete gPlayer;
  if(gLogics)    delete gLogics; // after renderer and player
  if(gNetwork)  delete gNetwork;
  if(gInput)    delete gInput;
  if(gMenu)      delete gMenu; // after gInput
  if(gData)      delete gData;
  if(gPrefs)    delete gPrefs;
  if(gDB)        delete gDB;
  // destroy Timer, then Logger at last
  if(gTimer)    delete gTimer;
  if(gLogger)    delete gLogger;
}

// -- RUN
void Obscurus::run()
{
  // flag loop if we are running in windowed mode
  bool _windowedMode = gPrefs->getAsBool("Fullscreen");
  gTimer->getElapsed(true); // reinit timer

  while(!bShutDown)
  {
    gTimer->getElapsed(true);
    if(gTime > 1.0)  gTimer->setElapsed(1.0); // clamp when slowing

    controlActions(); // act as controller before view

  // process messages for renderer and others
    //processMessages();
    
  // For now, don't use threads
  // we need some results before adapting to multithread
    gNetwork->listen();
    gInput->capture();

    gLogics->update();
    gPlayer->update();
    gPlayer->moveOnMap();
    gRenderer->moveCamera(gPlayer->oMapView.pos[0], gPlayer->oMapView.pos[1], gPlayer->oMapView.pos[2], gPlayer->oMapView.orientation[0], gPlayer->oMapView.orientation[1], 0.0f);
    if(gMenu->bActive)  gMenu->paint();
    gRenderer->draw();

  // yield if in windowed mode (never in fullscreen mode)
    if(_windowedMode) boost::this_thread::yield();
  }
  // wait for destruction...
}

// -- INIT
void Obscurus::init()
{
  // Singletons creation
  new ObscurusTimer();
  new ObscurusData();
  new ObscurusDB(); // need data objects
  new ObscurusNetwork();
  new ObscurusLogics();
  new ObscurusRenderer();
  // need renderer initialization :
  new ObscurusInput();
  new ObscurusPlayer();
  new ObscurusMenu();
  gLog("-*-*- Startup Complete -*-*-", 'I');

  gLog("Obscurus Primitiae Parabola Engine version %s ready.", 'I', gPrefs->getAsString("_Version"));
}
