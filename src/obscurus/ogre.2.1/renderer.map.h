#ifndef _OBS_RENDERER_MAP_H_
#define _OBS_RENDERER_MAP_H_
#include "../renderer.h"

struct MapVertex
{
  Ogre::Vector3 pos; // position
  Ogre::Vector3 nor; // normal
};

#endif
