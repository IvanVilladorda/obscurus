#include "renderer.object.h"

RenderedObject::RenderedObject(Ogre::IdType _id, Ogre::ObjectMemoryManager* _omm, Ogre::SceneManager* _mgr, Ogre::uint8 _rq)
  : Ogre::MovableObject(_id, _omm, _mgr, _rq), Ogre::Renderable()
{
  //bVisible = true;
}

RenderedObject::~RenderedObject()
{
  Ogre::VaoManager *vaoManager = mManager->getDestinationRenderSystem()->getVaoManager();

  Ogre::VertexArrayObjectArray::const_iterator itor = mVaoPerLod[0].begin();
  Ogre::VertexArrayObjectArray::const_iterator end = mVaoPerLod[0].end();
  while(itor != end)
  {
    Ogre::VertexArrayObject *vao = *itor;

    const Ogre::VertexBufferPackedVec &vertexBuffers = vao->getVertexBuffers();
    Ogre::VertexBufferPackedVec::const_iterator itBuffers = vertexBuffers.begin();
    Ogre::VertexBufferPackedVec::const_iterator enBuffers = vertexBuffers.end();

    while(itBuffers != enBuffers)
    {
      vaoManager->destroyVertexBuffer(*itBuffers);
      ++itBuffers;
    }

    if(vao->getIndexBuffer())
      vaoManager->destroyIndexBuffer(vao->getIndexBuffer());
    vaoManager->destroyVertexArrayObject(vao);

    ++itor;
  }
}

void RenderedObject::setBounds(Ogre::Aabb _aabb)
{
  mObjectData.mLocalAabb->setFromAabb(_aabb, mObjectData.mIndex);
  mObjectData.mWorldAabb->setFromAabb(_aabb, mObjectData.mIndex);
    mObjectData.mLocalRadius[mObjectData.mIndex] = std::numeric_limits<Ogre::Real>::max();
    mObjectData.mWorldRadius[mObjectData.mIndex] = std::numeric_limits<Ogre::Real>::max();
}
/*
void RenderedObject::setVisible(bool _v)
{
  bVisible = _v;
}
*/
void RenderedObject::setVAO(Ogre::VertexArrayObject* _vao)
{
  mVaoPerLod[0].push_back(_vao);
  //Use the same geometry for shadow casting. You can optimize performance by creating
  //a different Vao that only uses VES_POSITION, VES_BLEND_INDICES & VES_BLEND_WEIGHTS
  //(packed together to fit the caches) and avoids duplicated vertices (usually
  //needed by normals, UVs, etc)
  mVaoPerLod[1].push_back(_vao);

  mRenderables.push_back(this);
  this->setDatablock(Ogre::Root::getSingleton().getHlmsManager()->getHlms(Ogre::HLMS_PBS)->getDefaultDatablock());
}
