// -- GROMIUM

// -- MENU
#include "menu.h"

template<> ObscurusMenu* Ogre::Singleton<ObscurusMenu>::msSingleton = 0;
ObscurusMenu* ObscurusMenu::getSingletonPtr(void) { return msSingleton; }
ObscurusMenu& ObscurusMenu::getSingleton(void) { if(!msSingleton) msSingleton = 0; return (*msSingleton); }

// ~~ CONSTRUCTOR
ObscurusMenu::ObscurusMenu()
{
	bInitialized = false;
	wkeInit();
	pWebView = wkeCreateWebView();

	gRenderer->

	Ogre::Root* ogre = Ogre::Root::getSingletonPtr();
	Ogre::HlmsManager *hlmsManager = ogre->getHlmsManager();
	Ogre::HlmsUnlit *hlmsUnlit = static_cast<Ogre::HlmsUnlit*>(hlmsManager->getHlms(Ogre::HLMS_UNLIT));

	pTexture = Ogre::TextureManager::getSingleton().createManual("WKETexture", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		Ogre::TEX_TYPE_2D, renderWindow->getWidth(), renderWindow->getHeight(), 0, Ogre::PF_A8R8G8B8, Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
	datablock = static_cast<Ogre::HlmsUnlitDatablock*>(hlmsUnlit->createDatablock("WKEMaterial", "WKEMaterial", 
		Ogre::HlmsMacroblock(), Ogre::HlmsBlendblock(), Ogre::HlmsParamVec()));
	datablock->setTexture(0, 0, pTexture);
	//datablock->setTextureUvSource(0, 0);
	Ogre::HlmsMacroblock macroblock;
	macroblock.mDepthCheck = false;
	macroblock.mDepthWrite = false;
	datablock->setMacroblock(macroblock);

	Ogre::v1::OverlayManager* overlayManager = Ogre::v1::OverlayManager::getSingletonPtr();
	pPanel = static_cast<Ogre::v1::PanelOverlayElement*>(overlayManager->createOverlayElement("Panel", "WKEPanel"));
	pPanel->setMetricsMode(Ogre::v1::GMM_PIXELS);
	pPanel->setDimensions(renderWindow->getWidth(), renderWindow->getHeight());
//		pPanel->setDatablock(datablock);
	pPanel->setMaterialName("WKEMaterial");

	pOverlay = overlayManager->create("WKEOverlay");
	pOverlay->add2D(pPanel);
	pOverlay->show();
	ogre->addFrameListener(this);

	// snap handler
	pWebView->setTransparent(false);
	pWebView->resize(gRenderer->getRenderWindow()->getWidth(), gRenderer->getRenderWindow()->getHeight());
	pWebView->setDirty(true);
	pWebView->focus();

	bInitialized = true;
}

// ~~ DESTRUCTOR
ObscurusMenu::~ObscurusMenu()
{
	if(bInitialized)
		pWebView->destroy();
	wkeShutdown();
}

bool ObscurusMenu::frameStarted(const Ogre::FrameEvent &evt)
{
	if(pWebView->isDirty())
	{
		Ogre::v1::HardwarePixelBufferSharedPtr texBuf = pTexture->getBuffer();
		texBuf->lock(Ogre::v1::HardwareBuffer::HBL_DISCARD);
		pWebView->paint(texBuf->getCurrentLock().data, int(texBuf->getCurrentLock().rowPitch * 4));
/*
			Ogre::Image img;
			Ogre::uchar* data = OGRE_ALLOC_T(Ogre::uchar, std::atoi(gPrefs->get("active_width").c_str()) * std::atoi(gPrefs->get("active_height").c_str()) * 4, Ogre::MEMCATEGORY_GENERAL);
			img.loadDynamicImage(
				data,
				std::atoi(gPrefs->get("active_width").c_str()),
				std::atoi(gPrefs->get("active_height").c_str()),
				1,
				Ogre::PF_A8R8G8B8,  // pixel format
				true               // yes auto delete
				);			//img.resize(std::atoi(gPrefs->get("active_width").c_str()), std::atoi(gPrefs->get("active_height").c_str()));
			pWebView->paint(img.getPixelBox().data, int(texBuf->getCurrentLock().rowPitch * 4));
			img.save("test.png");
*/
		texBuf->unlock();
	}
	return true;
}

void ObscurusMenu::goBack()
{
	pWebView->goBack();
}

void ObscurusMenu::goForward()
{
	pWebView->goForward();
}

// ~~ LOAD
void ObscurusMenu::Load(const char* url)
{
	pWebView->loadURL(url);
}

// ~~ JS
void ObscurusMenu::JS(const std::string& code)
{
	pWebView->runJS(code.c_str());
}
/*
// ~~ SendKey
void ObscurusMenu::SendKey(int key)
{
	pWebView->keyDown(key, 0, false);
	wkeKeyUp(
	CefKeyEvent event;
	event.character = 0;
	event.focus_on_editable_field = true;
	event.is_system_key = false; // WM_SYSKEYDOWN
	event.modifiers = 0;
	event.native_key_code = 0;
	// Notification that a key transitioned from"up" to"down". KEYEVENT_RAWKEYDOWN
	// Notification that a key was pressed. This does not necessarily correspond to a character depending on the key and language. 
	// Use KEYEVENT_CHAR for character input. KEYEVENT_KEYDOWN
	// Notification that a key was released. KEYEVENT_KEYUP
	// Notification that a character was typed. Use this for text input. Key down events may generate 0, 1, or more than one character event depending on the key, locale, and operating system. KEYEVENT_CHAR
	event.type = KEYEVENT_CHAR;
	event.unmodified_character = 0;
	event.windows_key_code = 0;
	browser->GetHost()->SendKeyEvent(event);
}

// ~~ SendMouse
void ObscurusMenu::SendMouse(int _x, int _y)
{
	CefMouseEvent event;
	event.x = _x;
	event.y = _y;
	bool mouseLeave = false;
	browser->GetHost()->SendMouseMoveEvent(event, mouseLeave);
}

// ~~ SendClick
void ObscurusMenu::SendClick(int _x, int _y, int _button, bool _mouseup)
{
	CefMouseEvent event;
	event.x = _x;
	event.y = _y;
	//CefBrowserHost::MouseButtonType type = MBT_LEFT = 0, MBT_MIDDLE, MBT_RIGHT;
	CefBrowserHost::MouseButtonType type = MBT_LEFT;
	bool mouseUp = _mouseup;
	int clickCount = 1;
	browser->GetHost()->SendMouseClickEvent(event, type, mouseUp, clickCount);
}

// ~~ SendWheel
void ObscurusMenu::SendWheel(int _x, int _y, int _dx, int _dy)
{
	CefMouseEvent event;
	event.x = _x;
	event.y = _y;
	int deltaX = _dx;
	int deltaY = _dy;
	browser->GetHost()->SendMouseWheelEvent(event, deltaX, deltaY);
}
*/