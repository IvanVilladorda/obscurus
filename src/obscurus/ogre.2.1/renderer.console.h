#ifndef _OBS_RENDERER_CONSOLE_H_
#define _OBS_RENDERER_CONSOLE_H_
#include "../renderer.h"
#include <queue>

class OgreLogRedirector : public Ogre::LogListener
{
  void messageLogged(const Ogre::String& message, Ogre::LogMessageLevel lml, bool maskDebug, const Ogre::String &logName, bool& skipThisMessage)
  {
    // define base level
    char lvl = 'D';
    if(lml == Ogre::LML_NORMAL) lvl = 'I';
    else if(lml == Ogre::LML_CRITICAL) lvl = 'W';
    // analyse the log line to get clues
    if(message.find("EXCEPTION") != Ogre::String::npos) lvl = 'E';
    else if(message.find("ASSERT") != Ogre::String::npos) lvl = 'E';
    else if(message.find("ERROR") != Ogre::String::npos) lvl = 'E';
    else if(message.find("WARNING") != Ogre::String::npos) lvl = 'W';
    gLog("%s", lvl, message.c_str());
  }
};

struct ObscurusConsoleLine
{
  ObscurusConsoleLine(unsigned char, Ogre::ColourValue);
  ~ObscurusConsoleLine();
  Ogre::v1::TextAreaOverlayElement* pShadow;
  Ogre::v1::TextAreaOverlayElement* pText;
  float fLifeTime;
  Ogre::ColourValue cShadowColour;
  Ogre::ColourValue cTextColour;
};
struct ObscurusConsoleMessage
{
  std::string message;
  Ogre::ColourValue colour;
};

class ObscurusConsole
{
public:
  ObscurusConsole();
  ~ObscurusConsole();
  void draw();
  void addLine(const std::string&, Ogre::ColourValue = Ogre::ColourValue::White);

private:
  void write(const std::string&, Ogre::ColourValue = Ogre::ColourValue::White);

  Ogre::v1::OverlayContainer* pPanel;
  ObscurusConsoleLine* vLines[28]; // 1.0 / (0.025 + 0.01)
  unsigned char lineId;
  float fLineLifeTime;
  std::queue<ObscurusConsoleMessage> qMsg;
};

#endif