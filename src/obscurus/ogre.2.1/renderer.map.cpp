#include "renderer.map.h"
// --------------------------------------------------------------------------------------------------- BUILD 3D WORLD MAP
void ObscurusRenderer::buildMap(OBS::World* _world)
{
  gLog("Start building world map.", 'I');
  if(_world && !_world->bitmap) _world->openWorldMap();
  if(!_world | !_world->bitmap | !_world->chunkMap.size())
  {
    gLog("Unable to build world map.", 'W');
    return;
  }

  std::vector<Ogre::uint32> vIndices;
  std::vector<MapVertex> vVertices;
  float hFactor = _world->elevationMult / 128.0f;// 256.0f;

  // reserve maximum memory
  vVertices.reserve((_world->mapWidth * 4 + 1) * (_world->mapHeight * 4 + 1));
  vIndices.reserve((_world->mapWidth * 4 + 1) * (_world->mapHeight * 4 + 1) * 6);

  // now we scan the chunk map and build if necessary
  size_t offsetNum, offsetSup;
  Ogre::Vector3 A, B, C, D, E, F, G, H, N, Min(100,100,100), Max(0,0,0);
  Ogre::Real offsetX, offsetY;
  MapVertex oVertex;
  for(size_t x = 0; x < _world->mapWidth; x++)
  {
    offsetX = x * 4.0f;
    for(size_t y = 0; y < _world->mapHeight; y++)
    {
      if(!_world->chunkMap[x][y].empty) // an empty cell is surrounded by null cells
      {
        offsetY = y * 4.0f;
        // push all vertices from chunkmap
        oVertex.pos = Ogre::Vector3(offsetX, offsetY, _world->chunkMap[x][y].pt[0][0] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX, offsetY + 1, _world->chunkMap[x][y].pt[0][1] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX, offsetY + 2, _world->chunkMap[x][y].pt[0][2] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX, offsetY + 3, _world->chunkMap[x][y].pt[0][3] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 1, offsetY, _world->chunkMap[x][y].pt[1][0] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 1, offsetY + 1, _world->chunkMap[x][y].pt[1][1] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 1, offsetY + 2, _world->chunkMap[x][y].pt[1][2] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 1, offsetY + 3, _world->chunkMap[x][y].pt[1][3] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 2, offsetY, _world->chunkMap[x][y].pt[2][0] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 2, offsetY + 1, _world->chunkMap[x][y].pt[2][1] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 2, offsetY + 2, _world->chunkMap[x][y].pt[2][2] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 2, offsetY + 3, _world->chunkMap[x][y].pt[2][3] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 3, offsetY, _world->chunkMap[x][y].pt[3][0] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 3, offsetY + 1, _world->chunkMap[x][y].pt[3][1] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 3, offsetY + 2, _world->chunkMap[x][y].pt[3][2] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        oVertex.pos = Ogre::Vector3(offsetX + 3, offsetY + 3, _world->chunkMap[x][y].pt[3][3] * hFactor);
        Max.makeCeil(oVertex.pos); Min.makeFloor(oVertex.pos);
        vVertices.push_back(oVertex);
        // that is, there is 16 vertices by cell
      }
    }
  }
  for(size_t x = 0; x < _world->mapWidth; x++)
  {
    for(size_t y = 0; y < _world->mapHeight; y++)
    {
      if(!_world->chunkMap[x][y].empty)
      {
        // to fill indices, we eventually link against [x+1][y], [x][y+1] and [x+1][y+1] by using offsets from chunkmap
        // if anything is missing, we do not link against it, neither create additional vertices, we ignore it
        offsetNum = _world->chunkMap[x][y].offset * 16;
        // first, the links between internal vertices
        if(vVertices[offsetNum].pos.z + vVertices[offsetNum + 4].pos.z + vVertices[offsetNum + 1].pos.z != 0.0f) // eliminate flat faces underwater
        {
          vIndices.push_back(offsetNum); vIndices.push_back(offsetNum + 4); vIndices.push_back(offsetNum + 1);
        }
        if(vVertices[offsetNum + 5].pos.z + vVertices[offsetNum + 1].pos.z + vVertices[offsetNum + 4].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 5); vIndices.push_back(offsetNum + 1); vIndices.push_back(offsetNum + 4); // 0;0
        }
        if(vVertices[offsetNum + 4].pos.z + vVertices[offsetNum + 9].pos.z + vVertices[offsetNum + 5].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 4); vIndices.push_back(offsetNum + 9); vIndices.push_back(offsetNum + 5);
        }
        if(vVertices[offsetNum + 4].pos.z + vVertices[offsetNum + 8].pos.z + vVertices[offsetNum + 9].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 4); vIndices.push_back(offsetNum + 8); vIndices.push_back(offsetNum + 9); // 1;0
        }
        if(vVertices[offsetNum + 8].pos.z + vVertices[offsetNum + 12].pos.z + vVertices[offsetNum + 9].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 8); vIndices.push_back(offsetNum + 12); vIndices.push_back(offsetNum + 9);
        }
        if(vVertices[offsetNum + 13].pos.z + vVertices[offsetNum + 9].pos.z + vVertices[offsetNum + 12].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 13); vIndices.push_back(offsetNum + 9); vIndices.push_back(offsetNum + 12); // 2;0
        }
        if(vVertices[offsetNum + 1].pos.z + vVertices[offsetNum + 6].pos.z + vVertices[offsetNum + 2].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 1); vIndices.push_back(offsetNum + 6); vIndices.push_back(offsetNum + 2);
        }
        if(vVertices[offsetNum + 1].pos.z + vVertices[offsetNum + 5].pos.z + vVertices[offsetNum + 6].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 1); vIndices.push_back(offsetNum + 5); vIndices.push_back(offsetNum + 6); // 0;1
        }
        if(vVertices[offsetNum + 5].pos.z + vVertices[offsetNum + 9].pos.z + vVertices[offsetNum + 6].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 5); vIndices.push_back(offsetNum + 9); vIndices.push_back(offsetNum + 6);
        }
        if(vVertices[offsetNum + 10].pos.z + vVertices[offsetNum + 6].pos.z + vVertices[offsetNum + 9].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 10); vIndices.push_back(offsetNum + 6); vIndices.push_back(offsetNum + 9); // 1;1
        }
        if(vVertices[offsetNum + 9].pos.z + vVertices[offsetNum + 14].pos.z + vVertices[offsetNum + 10].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 9); vIndices.push_back(offsetNum + 14); vIndices.push_back(offsetNum + 10);
        }
        if(vVertices[offsetNum + 9].pos.z + vVertices[offsetNum + 13].pos.z + vVertices[offsetNum + 14].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 9); vIndices.push_back(offsetNum + 13); vIndices.push_back(offsetNum + 14); // 2;1
        }
        if(vVertices[offsetNum + 2].pos.z + vVertices[offsetNum + 6].pos.z + vVertices[offsetNum + 3].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 2); vIndices.push_back(offsetNum + 6); vIndices.push_back(offsetNum + 3);
        }
        if(vVertices[offsetNum + 7].pos.z + vVertices[offsetNum + 3].pos.z + vVertices[offsetNum + 6].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 7); vIndices.push_back(offsetNum + 3); vIndices.push_back(offsetNum + 6); // 0;2
        }
        if(vVertices[offsetNum + 6].pos.z + vVertices[offsetNum + 11].pos.z + vVertices[offsetNum + 7].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 6); vIndices.push_back(offsetNum + 11); vIndices.push_back(offsetNum + 7);
        }
        if(vVertices[offsetNum + 6].pos.z + vVertices[offsetNum + 10].pos.z + vVertices[offsetNum + 11].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 6); vIndices.push_back(offsetNum + 10); vIndices.push_back(offsetNum + 11); // 1;2
        }
        if(vVertices[offsetNum + 10].pos.z + vVertices[offsetNum + 14].pos.z + vVertices[offsetNum + 11].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 10); vIndices.push_back(offsetNum + 14); vIndices.push_back(offsetNum + 11);
        }
        if(vVertices[offsetNum + 15].pos.z + vVertices[offsetNum + 11].pos.z + vVertices[offsetNum + 14].pos.z != 0.0f)
        {
          vIndices.push_back(offsetNum + 15); vIndices.push_back(offsetNum + 11); vIndices.push_back(offsetNum + 14); // 2;0
        }
        if((y < _world->mapHeight - 1) && (!_world->chunkMap[x][y + 1].empty))
        {
          // link against y+1
          offsetSup = _world->chunkMap[x][y + 1].offset * 16;
          if(vVertices[offsetNum + 3].pos.z + vVertices[offsetSup + 4].pos.z + vVertices[offsetSup + 1].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 3); vIndices.push_back(offsetSup + 4); vIndices.push_back(offsetSup + 1);
          }
          if(vVertices[offsetNum + 3].pos.z + vVertices[offsetNum + 7].pos.z + vVertices[offsetSup + 4].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 3); vIndices.push_back(offsetNum + 7); vIndices.push_back(offsetSup + 4); // 0;3
          }
          if(vVertices[offsetNum + 7].pos.z + vVertices[offsetNum + 11].pos.z + vVertices[offsetSup + 4].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 7); vIndices.push_back(offsetNum + 11); vIndices.push_back(offsetSup + 4);
          }
          if(vVertices[offsetSup + 8].pos.z + vVertices[offsetSup + 4].pos.z + vVertices[offsetNum + 11].pos.z != 0.0f)
          {
            vIndices.push_back(offsetSup + 8); vIndices.push_back(offsetSup + 4); vIndices.push_back(offsetNum + 11); // 1;3
          }
          if(vVertices[offsetNum + 11].pos.z + vVertices[offsetSup + 12].pos.z + vVertices[offsetSup + 8].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 11); vIndices.push_back(offsetSup + 12); vIndices.push_back(offsetSup + 8);
          }
          if(vVertices[offsetNum + 11].pos.z + vVertices[offsetNum + 15].pos.z + vVertices[offsetSup + 12].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 11); vIndices.push_back(offsetNum + 15); vIndices.push_back(offsetSup + 12); // 2;3
          }
        }
        if((x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty))
        {
          // link against x+1
          offsetSup = _world->chunkMap[x + 1][y].offset * 16;
          if(vVertices[offsetNum + 12].pos.z + vVertices[offsetSup + 1].pos.z + vVertices[offsetNum + 13].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 12); vIndices.push_back(offsetSup + 1); vIndices.push_back(offsetNum + 13);
          }
          if(vVertices[offsetNum + 12].pos.z + vVertices[offsetSup].pos.z + vVertices[offsetSup + 1].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 12); vIndices.push_back(offsetSup); vIndices.push_back(offsetSup + 1); // 3;0
          }
          if(vVertices[offsetNum + 13].pos.z + vVertices[offsetSup + 1].pos.z + vVertices[offsetNum + 14].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 13); vIndices.push_back(offsetSup + 1); vIndices.push_back(offsetNum + 14);
          }
          if(vVertices[offsetSup + 2].pos.z + vVertices[offsetNum + 14].pos.z + vVertices[offsetSup + 1].pos.z != 0.0f)
          {
            vIndices.push_back(offsetSup + 2); vIndices.push_back(offsetNum + 14); vIndices.push_back(offsetSup + 1); // 3;1
          }
          if(vVertices[offsetNum + 14].pos.z + vVertices[offsetSup + 3].pos.z + vVertices[offsetNum + 15].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 14); vIndices.push_back(offsetSup + 3); vIndices.push_back(offsetNum + 15);
          }
          if(vVertices[offsetNum + 14].pos.z + vVertices[offsetSup + 2].pos.z + vVertices[offsetSup + 3].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 14); vIndices.push_back(offsetSup + 2); vIndices.push_back(offsetSup + 3); // 3;2
          }
        }
        if((y < _world->mapHeight - 1) && (x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y + 1].empty) && (!_world->chunkMap[x][y + 1].empty) && (!_world->chunkMap[x + 1][y].empty))
        {
          // link against [x+1][y+1] (only one face, but four cells involved)
          offsetSup = _world->chunkMap[x + 1][y + 1].offset * 16;
          size_t offsetUp = _world->chunkMap[x][y + 1].offset * 16;
          size_t offsetRight = _world->chunkMap[x + 1][y].offset * 16;
          if(vVertices[offsetNum + 15].pos.z + vVertices[offsetRight + 3].pos.z + vVertices[offsetUp + 12].pos.z != 0.0f)
          {
            vIndices.push_back(offsetNum + 15); vIndices.push_back(offsetRight + 3); vIndices.push_back(offsetUp + 12);
          }
          if(vVertices[offsetSup].pos.z + vVertices[offsetUp + 12].pos.z + vVertices[offsetRight + 3].pos.z != 0.0f)
          {
            vIndices.push_back(offsetSup); vIndices.push_back(offsetUp + 12); vIndices.push_back(offsetRight + 3); // 3;3
          }
        }
        // now, the normals, whose may interrogate x-1 and y-1
        //
        // \ | / | \ | / | \
        // - 3 - 7 -11 -15 -
        // / | \ | / | \ | /
        // - 2 - 6 -10 -14 -
        // \ | / | \ | / | \
        // - 1 - 5 - 9 -13 -
        // / | \ | / | \ | /
        // - 0 - 4 - 8 -12 -
        // \ | / | \ | / | \
        //
        // 0 -- start
        A = vVertices[offsetNum + 1].pos - vVertices[offsetNum].pos;
        B = vVertices[offsetNum + 4].pos - vVertices[offsetNum].pos;
        if((y > 0) && (x > 0) && (!_world->chunkMap[x - 1][y].empty) && (!_world->chunkMap[x][y - 1].empty))
        {
          C = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 3].pos - vVertices[offsetNum].pos;
          D = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 12].pos - vVertices[offsetNum].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else if((y > 0) && (!_world->chunkMap[x][y - 1].empty))
        {
          C = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 3].pos - vVertices[offsetNum].pos;
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        }
        else if((x > 0) && (!_world->chunkMap[x - 1][y].empty))
        {
          C = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 12].pos - vVertices[offsetNum].pos;
          N = (A.crossProduct(C) + B.crossProduct(A)) / 2.0f;
        }
        else
          N = B.crossProduct(A);
        vVertices[offsetNum].nor = N.normalisedCopy();
        // 1 -- start
        A = vVertices[offsetNum + 2].pos - vVertices[offsetNum + 1].pos;
        B = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 1].pos;
        C = vVertices[offsetNum + 5].pos - vVertices[offsetNum + 1].pos;
        D = vVertices[offsetNum + 4].pos - vVertices[offsetNum + 1].pos;
        E = vVertices[offsetNum].pos - vVertices[offsetNum + 1].pos;
        if((x > 0) && (!_world->chunkMap[x - 1][y].empty))
        {
          F = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 12].pos - vVertices[offsetNum + 1].pos;
          G = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 13].pos - vVertices[offsetNum + 1].pos;
          H = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 14].pos - vVertices[offsetNum + 1].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 1].nor = N.normalisedCopy();
        // 2 -- start
        A = vVertices[offsetNum + 3].pos - vVertices[offsetNum + 2].pos;
        B = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 2].pos;
        C = vVertices[offsetNum + 1].pos - vVertices[offsetNum + 2].pos;
        if((x > 0) && (!_world->chunkMap[x - 1][y].empty))
        {
          D = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 14].pos - vVertices[offsetNum + 2].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 2].nor = N.normalisedCopy();
        // 3 -- start
        A = vVertices[offsetNum + 7].pos - vVertices[offsetNum + 3].pos;
        B = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 3].pos;
        C = vVertices[offsetNum + 2].pos - vVertices[offsetNum + 3].pos;
        if((y < _world->mapHeight - 1) && (x > 0) && (!_world->chunkMap[x - 1][y].empty) && (!_world->chunkMap[x][y + 1].empty) && (!_world->chunkMap[x - 1][y + 1].empty))
        {
          D = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 14].pos - vVertices[offsetNum + 3].pos;
          E = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 15].pos - vVertices[offsetNum + 3].pos;
          F = vVertices[_world->chunkMap[x - 1][y + 1].offset * 16 + 12].pos - vVertices[offsetNum + 3].pos;
          G = vVertices[_world->chunkMap[x][y + 1].offset * 16].pos - vVertices[offsetNum + 3].pos;
          H = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 4].pos - vVertices[offsetNum + 3].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else if((x > 0) && (!_world->chunkMap[x - 1][y].empty))
        {
          D = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 14].pos - vVertices[offsetNum + 3].pos;
          E = vVertices[_world->chunkMap[x - 1][y].offset * 16 + 15].pos - vVertices[offsetNum + 3].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        }
        else if((y < _world->mapHeight - 1) && (!_world->chunkMap[x][y + 1].empty))
        {
          G = vVertices[_world->chunkMap[x][y + 1].offset * 16].pos - vVertices[offsetNum + 3].pos;
          H = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 4].pos - vVertices[offsetNum + 3].pos;
          N = (H.crossProduct(G) + A.crossProduct(H) + B.crossProduct(A) + C.crossProduct(B)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 3].nor = N.normalisedCopy();
        // 4 -- start
        A = vVertices[offsetNum].pos - vVertices[offsetNum + 4].pos;
        B = vVertices[offsetNum + 1].pos - vVertices[offsetNum + 4].pos;
        C = vVertices[offsetNum + 5].pos - vVertices[offsetNum + 4].pos;
        D = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 4].pos;
        E = vVertices[offsetNum + 8].pos - vVertices[offsetNum + 4].pos;
        if((y > 0) && (!_world->chunkMap[x][y - 1].empty))
        {
          F = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 11].pos - vVertices[offsetNum + 4].pos;
          G = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 7].pos - vVertices[offsetNum + 4].pos;
          H = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 3].pos - vVertices[offsetNum + 4].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 4].nor = N.normalisedCopy();
        // 5 -- start
        A = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 5].pos;
        B = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 5].pos;
        C = vVertices[offsetNum + 4].pos - vVertices[offsetNum + 5].pos;
        D = vVertices[offsetNum + 1].pos - vVertices[offsetNum + 5].pos;
        N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 5].nor = N.normalisedCopy();
        // 6 -- start
        A = vVertices[offsetNum + 7].pos - vVertices[offsetNum + 6].pos;
        B = vVertices[offsetNum + 11].pos - vVertices[offsetNum + 6].pos;
        C = vVertices[offsetNum + 10].pos - vVertices[offsetNum + 6].pos;
        D = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 6].pos;
        E = vVertices[offsetNum + 5].pos - vVertices[offsetNum + 6].pos;
        F = vVertices[offsetNum + 1].pos - vVertices[offsetNum + 6].pos;
        G = vVertices[offsetNum + 2].pos - vVertices[offsetNum + 6].pos;
        H = vVertices[offsetNum + 3].pos - vVertices[offsetNum + 6].pos;
        N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        vVertices[offsetNum + 6].nor = N.normalisedCopy();
        // 7 -- start
        A = vVertices[offsetNum + 11].pos - vVertices[offsetNum + 7].pos;
        B = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 7].pos;
        C = vVertices[offsetNum + 3].pos - vVertices[offsetNum + 7].pos;
        if((y < _world->mapHeight - 1) && (!_world->chunkMap[x][y + 1].empty))
        {
          D = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 4].pos - vVertices[offsetNum + 7].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 7].nor = N.normalisedCopy();
        // 8 -- start
        A = vVertices[offsetNum + 4].pos - vVertices[offsetNum + 8].pos;
        B = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 8].pos;
        C = vVertices[offsetNum + 12].pos - vVertices[offsetNum + 8].pos;
        if((y > 0) && (!_world->chunkMap[x][y - 1].empty))
        {
          D = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 11].pos - vVertices[offsetNum + 8].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 8].nor = N.normalisedCopy();
        // 9 -- start
        A = vVertices[offsetNum + 10].pos - vVertices[offsetNum + 9].pos;
        B = vVertices[offsetNum + 14].pos - vVertices[offsetNum + 9].pos;
        C = vVertices[offsetNum + 13].pos - vVertices[offsetNum + 9].pos;
        D = vVertices[offsetNum + 12].pos - vVertices[offsetNum + 9].pos;
        E = vVertices[offsetNum + 8].pos - vVertices[offsetNum + 9].pos;
        F = vVertices[offsetNum + 4].pos - vVertices[offsetNum + 9].pos;
        G = vVertices[offsetNum + 5].pos - vVertices[offsetNum + 9].pos;
        H = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 9].pos;
        N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        vVertices[offsetNum + 9].nor = N.normalisedCopy();
        // 10 -- start
        A = vVertices[offsetNum + 11].pos - vVertices[offsetNum + 10].pos;
        B = vVertices[offsetNum + 14].pos - vVertices[offsetNum + 10].pos;
        C = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 10].pos;
        D = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 10].pos;
        N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 10].nor = N.normalisedCopy();
        // 11 -- start
        A = vVertices[offsetNum + 15].pos - vVertices[offsetNum + 11].pos;
        B = vVertices[offsetNum + 14].pos - vVertices[offsetNum + 11].pos;
        C = vVertices[offsetNum + 10].pos - vVertices[offsetNum + 11].pos;
        D = vVertices[offsetNum + 6].pos - vVertices[offsetNum + 11].pos;
        E = vVertices[offsetNum + 7].pos - vVertices[offsetNum + 11].pos;
        if((y < _world->mapHeight - 1) && (!_world->chunkMap[x][y + 1].empty))
        {
          F = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 4].pos - vVertices[offsetNum + 11].pos;
          G = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 8].pos - vVertices[offsetNum + 11].pos;
          H = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 12].pos - vVertices[offsetNum + 11].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 11].nor = N.normalisedCopy();
        // 12 -- start
        A = vVertices[offsetNum + 8].pos - vVertices[offsetNum + 12].pos;
        B = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 12].pos;
        C = vVertices[offsetNum + 13].pos - vVertices[offsetNum + 12].pos;
        if((y > 0) && (x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty) && (!_world->chunkMap[x][y - 1].empty) && (!_world->chunkMap[x + 1][y - 1].empty))
        {
          D = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 1].pos - vVertices[offsetNum + 12].pos;
          E = vVertices[_world->chunkMap[x + 1][y].offset * 16].pos - vVertices[offsetNum + 12].pos;
          F = vVertices[_world->chunkMap[x + 1][y - 1].offset * 16 + 3].pos - vVertices[offsetNum + 12].pos;
          G = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 15].pos - vVertices[offsetNum + 12].pos;
          H = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 11].pos - vVertices[offsetNum + 12].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else if((x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty))
        {
          D = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 1].pos - vVertices[offsetNum + 12].pos;
          E = vVertices[_world->chunkMap[x + 1][y].offset * 16].pos - vVertices[offsetNum + 12].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        }
        else if((y > 0) && (!_world->chunkMap[x][y - 1].empty))
        {
          G = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 15].pos - vVertices[offsetNum + 12].pos;
          H = vVertices[_world->chunkMap[x][y - 1].offset * 16 + 11].pos - vVertices[offsetNum + 12].pos;
          N = (H.crossProduct(G) + A.crossProduct(H) + B.crossProduct(A) + C.crossProduct(B)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 12].nor = N.normalisedCopy();
        // 13 -- start
        A = vVertices[offsetNum + 12].pos - vVertices[offsetNum + 13].pos;
        B = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 13].pos;
        C = vVertices[offsetNum + 14].pos - vVertices[offsetNum + 13].pos;
        if((x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty))
        {
          D = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 1].pos - vVertices[offsetNum + 13].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        vVertices[offsetNum + 13].nor = N.normalisedCopy();
        // 14 -- start
        A = vVertices[offsetNum + 13].pos - vVertices[offsetNum + 14].pos;
        B = vVertices[offsetNum + 9].pos - vVertices[offsetNum + 14].pos;
        C = vVertices[offsetNum + 10].pos - vVertices[offsetNum + 14].pos;
        D = vVertices[offsetNum + 11].pos - vVertices[offsetNum + 14].pos;
        E = vVertices[offsetNum + 15].pos - vVertices[offsetNum + 14].pos;
        if((x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty))
        {
          F = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 3].pos - vVertices[offsetNum + 14].pos;
          G = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 2].pos - vVertices[offsetNum + 14].pos;
          H = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 1].pos - vVertices[offsetNum + 14].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D) + F.crossProduct(E) + G.crossProduct(F) + H.crossProduct(G) + A.crossProduct(H)) / 8.0f;
        }
        else
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + E.crossProduct(D)) / 4.0f;
        vVertices[offsetNum + 14].nor = N.normalisedCopy();
        // 15 -- start
        A = vVertices[offsetNum + 14].pos - vVertices[offsetNum + 15].pos;
        B = vVertices[offsetNum + 11].pos - vVertices[offsetNum + 15].pos;
        if((y < _world->mapHeight - 1) && (x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty) && (!_world->chunkMap[x][y + 1].empty))
        {
          C = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 12].pos - vVertices[offsetNum + 15].pos;
          D = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 3].pos - vVertices[offsetNum + 15].pos;
          N = (B.crossProduct(A) + C.crossProduct(B) + D.crossProduct(C) + A.crossProduct(D)) / 4.0f;
        }
        else if((y < _world->mapHeight - 1) && (!_world->chunkMap[x][y + 1].empty))
        {
          C = vVertices[_world->chunkMap[x][y + 1].offset * 16 + 12].pos - vVertices[offsetNum + 15].pos;
          N = (B.crossProduct(A) + C.crossProduct(B)) / 2.0f;
        }
        else if((x < _world->mapWidth - 1) && (!_world->chunkMap[x + 1][y].empty))
        {
          D = vVertices[_world->chunkMap[x + 1][y].offset * 16 + 3].pos - vVertices[offsetNum + 15].pos;
          N = (A.crossProduct(D) + B.crossProduct(A)) / 2.0f;
        }
        else
          N = B.crossProduct(A);
        vVertices[offsetNum + 15].nor = N.normalisedCopy();
      }
    }
  }

  Ogre::Vector3 nmBB = (Min + Max) / 2.0f;

  oWorld = new RenderedObject(Ogre::Id::generateNewId<Ogre::MovableObject>(), &pSceneManager->_getEntityMemoryManager(Ogre::SCENE_STATIC), pSceneManager, 0);
  //oWorld->setBounds(Ogre::Aabb(nmBB, nmBB));
  oWorld->setBounds();

  Ogre::VertexElement2Vec vertexElements;
  vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_POSITION));
  vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_NORMAL));
  //vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES));
  //vertexElements.push_back(Ogre::VertexElement2(Ogre::VET_FLOAT3, Ogre::VES_DIFFUSE));

  Ogre::RenderSystem* renderSystem = Ogre::Root::getSingleton().getRenderSystem();
  Ogre::VaoManager* vaoManager = renderSystem->getVaoManager();

  size_t vertexSize = vaoManager->calculateVertexSize(vertexElements);

  Ogre::Real* vertexData = static_cast<Ogre::Real*>(OGRE_MALLOC_SIMD(vertexSize * vVertices.size(), Ogre::MEMCATEGORY_GEOMETRY));
  Ogre::Real* pVertex = reinterpret_cast<Ogre::Real*>(vertexData);
  Ogre::uint32* indexData = static_cast<Ogre::uint32*>(OGRE_MALLOC_SIMD(vIndices.size() * sizeof(Ogre::uint32), Ogre::MEMCATEGORY_GEOMETRY));
  Ogre::uint32* pIndex = reinterpret_cast<Ogre::uint32*>(indexData);

  for(size_t i = 0; i < vVertices.size(); i++)
  {
    *pVertex++ = vVertices[i].pos.x;
    *pVertex++ = vVertices[i].pos.y;
    *pVertex++ = vVertices[i].pos.z;
    *pVertex++ = vVertices[i].nor.x;
    *pVertex++ = vVertices[i].nor.y;
    *pVertex++ = vVertices[i].nor.z;
    //*pVertex++ = x / (xNumVertices - 1);
    //*pVertex++ = y / (yNumVertices - 1);
    //*pVertex++ = 1.0f;
    //*pVertex++ = 1.0f;
    //*pVertex++ = 1.0f;
  }
  for(size_t i = 0; i < vIndices.size(); i++)
    *pIndex++ = vIndices[i];
  //memcpy(indexData, vIndices.data(), sizeof(vIndices.data()));

  Ogre::VertexBufferPacked *vertexBuffer = vaoManager->createVertexBuffer(vertexElements, vVertices.size(), Ogre::BT_IMMUTABLE, vertexData, false);
  Ogre::IndexBufferPacked *indexBuffer = vaoManager->createIndexBuffer(Ogre::IndexBufferPacked::IT_32BIT, vIndices.size(), Ogre::BT_IMMUTABLE, indexData, false);

  Ogre::VertexBufferPackedVec vertexBuffers;
  vertexBuffers.push_back(vertexBuffer);
  Ogre::VertexArrayObject *vao = vaoManager->createVertexArrayObject(vertexBuffers, indexBuffer, Ogre::v1::RenderOperation::OT_TRIANGLE_LIST);
  oWorld->setVAO(vao);

  Ogre::SceneNode *mapNode = pSceneManager->getRootSceneNode()->createChildSceneNode(Ogre::SCENE_STATIC);
  Ogre::SceneNode *sceneNode = mapNode->createChildSceneNode(Ogre::SCENE_STATIC);
  sceneNode->attachObject(oWorld);
  gLog("World map build :", 'I');
  gLog("\t - num vertices : %d", 'I', vVertices.size());
  gLog("\t - num indices : %d", 'I', vIndices.size());
  // TEMP
  Ogre::Light *light = pSceneManager->createLight();
  Ogre::SceneNode *lightNode = mapNode->createChildSceneNode();
  lightNode->attachObject(light);
  light->setPowerScale(Ogre::Math::PI); //Since we don't do HDR, counter the PBS' division by PI
  light->setType(Ogre::Light::LT_DIRECTIONAL);
  light->setDirection(Ogre::Vector3(0, 0.5, -0.75).normalisedCopy());
  pSceneManager->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f), Ogre::ColourValue(0.1f, 0.1f, 0.1f), Ogre::Vector3::UNIT_Z);
}
