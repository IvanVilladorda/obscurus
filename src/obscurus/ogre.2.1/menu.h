// -- MENU HEADER
#ifndef _OBS_MENU_H_
#define _OBS_MENU_H_
#include "preferences.h"

#include <wke.h>

#include <Ogre.h>
#if OGRE_VERSION_MAJOR == 2
#	include "ogre.2.1/renderer.h"
#else
#	include "renderer.h"
#endif

#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreOverlay.h>

#include <OgreHlmsManager.h>
#include <OgreHlmsTextureManager.h>
#include <OgreHlmsUnlit.h>
#include <OgreHlmsUnlitDatablock.h>

class ObscurusMenu : public Ogre::Singleton<ObscurusMenu>, public Ogre::FrameListener
{
public:
	ObscurusMenu(Ogre::RenderWindow* renderWindow);
	~ObscurusMenu();
	static ObscurusMenu* getSingletonPtr(void);
	static ObscurusMenu& getSingleton(void);

	bool frameStarted(const Ogre::FrameEvent &evt);
	/*
	bool keyPressed(const OIS::KeyEvent &arg);
	bool keyReleased(const OIS::KeyEvent &arg);
	// OIS::MouseListener
	bool mouseMoved(const OIS::MouseEvent &arg);
	bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	*/
	void Load(const char* url);
	void JS(const std::string& code);
	void goBack();
	void goForward();

	Ogre::TexturePtr pTexture;
	Ogre::v1::Overlay* pOverlay;
	Ogre::v1::PanelOverlayElement* pPanel;
	Ogre::HlmsUnlitDatablock* datablock;
	wke::IWebView* pWebView;

private:
	wkeClientHandler clientHandler;
	bool bInitialized;
	int frameCounter, paintCounter;
};

#define gMenu		ObscurusMenu::getSingletonPtr()

#endif