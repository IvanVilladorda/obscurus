#ifndef _OBS_RENDERER_H_
#define _OBS_RENDERER_H_

#include <OgreSingleton.h>
#include <Ogre.h>

#include <OgreD3D11Plugin.h>
#include <OgreGL3PlusPlugin.h>

#include <OgreHlmsManager.h>
#include <OgreHlmsTextureManager.h>
#include <OgreHlmsPbs.h>
#include <OgreHlmsUnlit.h>
#include <OgreHlmsUnlitDatablock.h>
#include <OgreArchiveManager.h>
#include <Compositor/OgreCompositorManager2.h>

#include <OgreOverlaySystem.h>
#include <OgreFontManager.h>
#include <OgreOverlayManager.h>
#include <OgrePanelOverlayElement.h>
#include <OgreOverlay.h>
#include <OgreOverlayContainer.h>
#include <OgreTextAreaOverlayElement.h>

#include <wke.h>
#include "log.h"
#include "preferences.h"
#include "timer.h"
#include "renderer.console.h"
#include "logics/world.h"
#include "logics/cell.h"
#include "renderer.object.h"
class RenderedObject;
#include "renderer.map.h"
#include "renderer.cell.h"

class ObscurusRenderer : public Ogre::Singleton<ObscurusRenderer>, public ObscurusLogListener
{
public:
  ObscurusRenderer();
  ~ObscurusRenderer();
  static ObscurusRenderer* getSingletonPtr(void);
  static ObscurusRenderer& getSingleton(void);

  void log(const char*, const char*);
  void buildMap(OBS::World*);
  void buildCell(OBS::Cell*);
  void moveCamera(float, float, float, float, float, float);
  void draw();

  // specialized methods :
  void createWebPanel(const std::string&); // WKE WebView : texture + overlay + panel
  void renderWebPanel(wkeWebView, const std::string&); // WKE WebView : fill texture with webkit output
  void renderWebPanel(wkeWebView, const std::string&, const std::string&); // WKE WebView : fill image with webkit output
  void showWebPanel(const std::string&, bool);

private:
  void init();

  OgreLogRedirector*          pLogRedirect;
  Ogre::Root*                 pRoot;
  Ogre::RenderWindow*         pWindow;
  Ogre::SceneManager*         pSceneManager;
  Ogre::Camera*               pCamera;
  Ogre::CompositorWorkspace*  pWorkspace;
  Ogre::v1::OverlaySystem*    pOverlaySystem;
  ObscurusConsole*            pConsole;

  Ogre::String                sResourcePath;
  Ogre::String                sWorldName;
  RenderedObject*             oWorld;
  RenderedObject*             oTest;
  RenderedObject*             oCells[5][5];
};

#define gRenderer    ObscurusRenderer::getSingletonPtr()

#endif