#include "../renderer.h"
//#include "webpanel.h" // struct

void ObscurusRenderer::createWebPanel(const std::string& _name)
{
  Ogre::HlmsManager *pHlmsManager = pRoot->getHlmsManager();
  Ogre::HlmsUnlit *pHlmsUnlit = static_cast<Ogre::HlmsUnlit*>(pHlmsManager->getHlms(Ogre::HLMS_UNLIT));

  Ogre::v1::Overlay* pOverlay;
  Ogre::v1::PanelOverlayElement* pPanel;
  Ogre::HlmsUnlitDatablock* pDatablock;

  Ogre::TexturePtr pTexture = Ogre::TextureManager::getSingleton().createManual(_name + "Texture", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
    Ogre::TEX_TYPE_2D, pWindow->getWidth(), pWindow->getHeight(), 0, Ogre::PF_A8R8G8B8, Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);
  pDatablock = static_cast<Ogre::HlmsUnlitDatablock*>(pHlmsUnlit->createDatablock(_name + "Material", _name + "Material",
    Ogre::HlmsMacroblock(), Ogre::HlmsBlendblock(), Ogre::HlmsParamVec()));
  pDatablock->setTexture(0, 0, pTexture);
  //datablock->setTextureUvSource(0, 0);
  Ogre::HlmsMacroblock pMacroblock;
  pMacroblock.mDepthCheck = false;
  pMacroblock.mDepthWrite = false;
  pDatablock->setMacroblock(pMacroblock);

  Ogre::v1::OverlayManager* pOverlayManager = Ogre::v1::OverlayManager::getSingletonPtr();
  pPanel = static_cast<Ogre::v1::PanelOverlayElement*>(pOverlayManager->createOverlayElement("Panel", _name + "Panel"));
  pPanel->setMetricsMode(Ogre::v1::GMM_PIXELS);
  pPanel->setDimensions(pWindow->getWidth(), pWindow->getHeight());
  //    pPanel->setDatablock(datablock);
  pPanel->setMaterialName(_name + "Material");

  pOverlay = pOverlayManager->create(_name + "Overlay");
  pOverlay->add2D(pPanel);
  pOverlay->setZOrder(252);
  pOverlay->show();
}

void ObscurusRenderer::renderWebPanel(wkeWebView _wv, const std::string& _name)
{
  Ogre::v1::HardwarePixelBufferSharedPtr pPixelBuffer = Ogre::TextureManager::getSingleton().getByName(_name + "Texture")->getBuffer();
  pPixelBuffer->lock(Ogre::v1::HardwareBuffer::HBL_DISCARD);
  _wv->paint(pPixelBuffer->getCurrentLock().data, int(pPixelBuffer->getCurrentLock().rowPitch * 4));
  pPixelBuffer->unlock();
}

void ObscurusRenderer::renderWebPanel(wkeWebView _wv, const std::string& _name, const std::string& _fileName)
{
  // name is unused here
  Ogre::Image oImg;
  Ogre::uchar* data = OGRE_ALLOC_T(Ogre::uchar, pWindow->getWidth() * pWindow->getHeight() * 4, Ogre::MEMCATEGORY_GENERAL);
  oImg.loadDynamicImage(data, pWindow->getWidth(), pWindow->getHeight(), 1, Ogre::PF_A8R8G8B8, true);
  _wv->paint(oImg.getPixelBox().data, 0); // pitch = 0 => full width*height*4
  oImg.save(_fileName);
}

void ObscurusRenderer::showWebPanel(const std::string& _name, bool _show)
{
  Ogre::v1::Overlay* pOverlay = Ogre::v1::OverlayManager::getSingletonPtr()->getByName(_name + "Overlay");
  if(_show) pOverlay->show();
  else    pOverlay->hide();
}
