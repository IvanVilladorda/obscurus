#ifndef _OBS_RENDERER_OBJECT_H_
#define _OBS_RENDERER_OBJECT_H_
#include "../renderer.h"

class RenderedObject : public Ogre::MovableObject, public Ogre::Renderable
{
public:
  RenderedObject(Ogre::IdType, Ogre::ObjectMemoryManager*, Ogre::SceneManager*, Ogre::uint8);
  ~RenderedObject();

  void setBounds(Ogre::Aabb _aabb = Ogre::Aabb::BOX_INFINITE);
  //void setVisible(bool _v = true);
  void setVAO(Ogre::VertexArrayObject*);

  // movable :
  const Ogre::String&  getMovableType() const { return Ogre::BLANKSTRING; }
  // renderable :
  /* v1 */ void getRenderOperation(Ogre::v1::RenderOperation&, bool) { OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, "Ogre V1 forbidden.", "RenderedObject::getRenderOperation"); }
  /* v1 */ void getWorldTransforms(Ogre::Matrix4*) const { OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, "Ogre V1 forbidden.", "RenderedObject::getWorldTransforms"); }
  /* v1 */ bool getCastsShadows(void) const { OGRE_EXCEPT(Ogre::Exception::ERR_NOT_IMPLEMENTED, "Ogre V1 forbidden.", "RenderedObject::getCastsShadows"); }
  const Ogre::LightList& getLights() const { return this->queryLights(); }

private:
 // bool bVisible;
};

#endif