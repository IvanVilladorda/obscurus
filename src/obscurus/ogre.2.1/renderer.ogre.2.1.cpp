#include "../renderer.h"
#include "resources.h"
template<> ObscurusRenderer* Ogre::Singleton<ObscurusRenderer>::msSingleton = 0;
ObscurusRenderer* ObscurusRenderer::getSingletonPtr(void) { return msSingleton; }
ObscurusRenderer& ObscurusRenderer::getSingleton(void) { if(!msSingleton) msSingleton = 0; return (*msSingleton); }

// --------------------------------------------------------------------------------------------------- CTOR
ObscurusRenderer::ObscurusRenderer() :
  pLogRedirect(0),
  pRoot(0),
  pWindow(0),
  pSceneManager(0),
  pCamera(0),
  pWorkspace(0),
  pOverlaySystem(0),
  pConsole(0)
{
  gLog("-*-*- Renderer Startup.", 'I');
  std::string sStr = std::to_string(OGRE_VERSION_MAJOR) + "." + std::to_string(OGRE_VERSION_MINOR) + " - " + OGRE_VERSION_NAME + " (" + OGRE_VERSION_SUFFIX + ")";
  gLog("Renderer is using Ogre %s.", 'I', sStr.c_str());
  memset(&oCells, 0, sizeof(oCells));
  oWorld = oTest = 0;
  init();
}

// --------------------------------------------------------------------------------------------------- DTOR
ObscurusRenderer::~ObscurusRenderer()
{
  gLog("*-*-* Renderer Shutdown.", 'I');
  delete pConsole;
  gLogger->removeListener(this);
  if(oWorld) delete oWorld;
  if(oTest) delete oTest;
  for(short x = 0; x < 5; x++)
  {
    for(short y = 0; y < 5; y++)
    {
      if(oCells[x][y]) delete oCells[x][y];
    }
  }
  if(pSceneManager) pSceneManager->removeRenderQueueListener(pOverlaySystem);
  OGRE_DELETE pOverlaySystem;
  // plugins are unloaded during root' dtor, so simply kill it
  if(pRoot) OGRE_DELETE pRoot;
  if(pLogRedirect) delete pLogRedirect;
}

// --------------------------------------------------------------------------------------------------- DRAW SOMETHING
void ObscurusRenderer::draw()
{
  Ogre::WindowEventUtilities::messagePump();
  pConsole->draw();

  if(pWindow->isVisible())
    pRoot->renderOneFrame();
  else
    Ogre::Threads::Sleep(500);
}

// --------------------------------------------------------------------------------------------------- INIT OGRE
void ObscurusRenderer::init()
{
  // create root
  pRoot = OGRE_NEW Ogre::Root("", "", "");

  // redirect all ogre log messages to our log class (gLog)
  pLogRedirect = new OgreLogRedirector();
  Ogre::Log* plog = Ogre::LogManager::getSingleton().createLog("", true, false, true);
  plog->addListener(pLogRedirect);

  // plug rendersystem
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  if(gPrefs->get("_XPDetected") == false) // do not allow D11 on XP
  {
    Ogre::D3D11Plugin* pD3D11 = OGRE_NEW Ogre::D3D11Plugin();
    pRoot->installPlugin(pD3D11);
  }
  else // GL3+ for XP
  {
    Ogre::GL3PlusPlugin* pOpenGL3Plus = OGRE_NEW Ogre::GL3PlusPlugin();
    pRoot->installPlugin(pOpenGL3Plus);
  }
#else
  Ogre::GL3PlusPlugin* pOpenGL3Plus = OGRE_NEW Ogre::GL3PlusPlugin();
  pRoot->installPlugin(pOpenGL3Plus);
#endif

  // init root
  pRoot->setRenderSystem(pRoot->getAvailableRenderers().front());
  pRoot->getRenderSystem()->setConfigOption("sRGB Gamma Conversion", "Yes");
  pRoot->initialise(false, gPrefs->get("_AppName"));

// --------------------------------------------------------------------------------------------------- WINDOWING
  // prepare
  Ogre::NameValuePairList mParams;
  mParams["border"] = "none";
  mParams["title"] = gPrefs->get("_AppName");
  mParams["FSAA"] = !gPrefs->get("FSAA") ? "0" : gPrefs->get("FSAA");

  int iWidth, iHeight, iDepth, iFreq;
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 // get display information on Windows
  {
    DEVMODE dm;
    dm.dmSize = sizeof(dm); dm.dmDriverExtra = 0;
    EnumDisplaySettings(0, ENUM_CURRENT_SETTINGS, &dm);
    iWidth = dm.dmPelsWidth;
    iHeight = dm.dmPelsHeight;
    iDepth = dm.dmBitsPerPel;
    iFreq = dm.dmDisplayFrequency;
  }
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX // get display information on Linux
  {
    int num_sizes;
    Rotation current_rotation;
    Display *dpy = XOpenDisplay(NULL);
    Window root = RootWindow(dpy, 0);
    XRRScreenSize *xrrs = XRRSizes(dpy, 0, &num_sizes);
    XRRScreenConfiguration *conf = XRRGetScreenInfo(dpy, root);
    iFreq = XRRConfigCurrentRate(conf);
    SizeID current_size_id = XRRConfigCurrentConfiguration(conf, &current_rotation);
    iWidth = xrrs[current_size_id].width;
    iHeight = xrrs[current_size_id].height;
    XCloseDisplay(dpy);
    iDepth = 32; // win32 specific, so go on
  }
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE // get display information on Mac
  // I don't know
  mParams["Full Screen"]            = gPrefs->get("Fullscreen") == true ? "true":"false";
  mParams["macAPI"]                = "cocoa";
  mParams["macAPICocoaUseNSView"]  = "true";
#endif

  mParams["gamma"] = "true";
  mParams["colourDepth"] = Ogre::StringConverter::toString(iDepth);
  mParams["vsync"] = gPrefs->get("VSync") == true ? "true" : "false";

  // keep desktop rez for fullscreen, but not in windowed mode
  if(gPrefs->get("Fullscreen") == false)
  {
    // reduce desktop rez to 80% if no prefs
    if((0 == gPrefs->getAsInt("ScreenWidth")) | (0 == gPrefs->getAsInt("ScreenHeight")))
    {
      iWidth = (int)(iWidth * 0.8f);
      iHeight = (int)(iHeight * 0.8f);
    }
    else // set information given in prefs
    {
      iWidth = gPrefs->getAsInt("ScreenWidth");
      iHeight = gPrefs->getAsInt("ScreenHeight");
    }
    if(gPrefs->get("VSync") == false) // switch vsync on in windowed mode
    {
      gLog("Turning vsync ON in windowed mode.",'I');
      mParams["vsync"] = "true";
    }
  }

  // create now the renderwindow with all parameters settled
  pWindow = pRoot->createRenderWindow(gPrefs->get("_AppName"), iWidth, iHeight, gPrefs->getAsBool("Fullscreen"), &mParams);

  // save window dimensions
  gPrefs->set("_ActiveWidth", std::to_string(iWidth));
  gPrefs->set("_ActiveHeight", std::to_string(iHeight));

  size_t hwnd;
  pWindow->getCustomAttribute("WINDOW", &hwnd);
  // save window handle as string (yes!)
  gPrefs->set("_RenderWindow", std::to_string(hwnd));

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 // set window icon from internal resource
  HINSTANCE hinst = (HINSTANCE)GetModuleHandle(NULL);
  SetClassLong((HWND)hwnd, GCL_HICONSM, (LONG)LoadIcon(hinst, MAKEINTRESOURCE(IDI_ICON)));
  SetClassLong((HWND)hwnd, GCL_HICON, (LONG)LoadIcon(hinst, MAKEINTRESOURCE(IDI_ICON)));
#elif OGRE_PLATFORM == OGRE_PLATFORM_LINUX
  // use desktop entry file by installer
#elif OGRE_PLATFORM == OGRE_PLATFORM_APPLE
  // I don't know
#endif

// --------------------------------------------------------------------------------------------------- EXTERNAL RESOURCES & HLMS
  // point to dat directory for external resources (exclude hlms subdir in any case)
  //Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../dat", "FileSystem", "General");
//  Ogre::ResourceGroupManager::getSingleton().createResourceGroup("Textures");
  Ogre::ResourceGroupManager::getSingleton().addResourceLocation(gPrefs->get("_FullPath") + "dat/tex", "FileSystem", "Textures");
//  Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Textures");
//  Ogre::ResourceGroupManager::getSingleton().createResourceGroup("Sounds");
  Ogre::ResourceGroupManager::getSingleton().addResourceLocation(gPrefs->get("_FullPath") + "dat/snd", "FileSystem", "Sounds");
//  Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Sounds");
//  Ogre::ResourceGroupManager::getSingleton().createResourceGroup("Scripts");
  Ogre::ResourceGroupManager::getSingleton().addResourceLocation(gPrefs->get("_FullPath") + "dat/scr", "FileSystem", "Scripts");
//  Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Scripts");
//  Ogre::ResourceGroupManager::getSingleton().createResourceGroup("Fonts");
  Ogre::ResourceGroupManager::getSingleton().addResourceLocation(gPrefs->get("_FullPath") + "dat/fnt", "FileSystem", "Fonts");
//  Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Fonts");
  //Ogre::ResourceGroupManager::getSingleton().addResourceLocation("../dat/zip/pack.zip", "Zip", "General");

  // register HLMS now
  Ogre::String sDataFolder = gPrefs->get("_FullPath") + "dat/hlms/";
  Ogre::RenderSystem* pRenderSystem = pRoot->getRenderSystem();
  Ogre::String sShaderSyntax = "glsl";
  if(pRenderSystem->getName() == "Direct3D11 Rendering Subsystem") sShaderSyntax = "hlsl";
  Ogre::ArchiveVec vArchive;
  Ogre::Archive* pHlmsArchive = Ogre::ArchiveManager::getSingletonPtr()->load(sDataFolder + "common_" + sShaderSyntax + ".zip", "Zip", true);
  vArchive.push_back(pHlmsArchive);
  Ogre::Archive* pHlmsArchiveUnlit = Ogre::ArchiveManager::getSingletonPtr()->load(sDataFolder + "unlit_" + sShaderSyntax + ".zip", "Zip", true);
  Ogre::HlmsUnlit* pHlmsUnlit = OGRE_NEW Ogre::HlmsUnlit(pHlmsArchiveUnlit, &vArchive);
  pRoot->getHlmsManager()->registerHlms(pHlmsUnlit);
  Ogre::Archive* pHlmsArchivePbs = Ogre::ArchiveManager::getSingletonPtr()->load(sDataFolder + "pbs_" + sShaderSyntax + ".zip", "Zip", true);
  Ogre::HlmsPbs* pHlmsPbs = OGRE_NEW Ogre::HlmsPbs(pHlmsArchivePbs, &vArchive);
  pRoot->getHlmsManager()->registerHlms(pHlmsPbs);
  if(pRenderSystem->getName() == "Direct3D11 Rendering Subsystem")
  {
    // Ogre team :
    // " Set lower limits 512kb instead of the default 4MB per Hlms in D3D 11.0
    //    and below to avoid saturating AMD's discard limit (8MB) or
    //    saturate the PCIE bus in some low end machines."
    bool bSupportsNoOverwriteOnTextureBuffers;
    pRenderSystem->getCustomAttribute("MapNoOverwriteOnDynamicBufferSRV", &bSupportsNoOverwriteOnTextureBuffers);
    if(!bSupportsNoOverwriteOnTextureBuffers)
    {
      pHlmsPbs->setTextureBufferDefaultSize(512 * 1024);
      pHlmsUnlit->setTextureBufferDefaultSize(512 * 1024);
    }
  }

// --------------------------------------------------------------------------------------------------- GFX OBJECTS INITS
  // Ogre team :
  //  "When to use INSTANCING_CULLING_SINGLETHREAD?
  //    > If your scene doesn't use HW Basic or HW VTF instancing techniques, or you have very few Instanced entities compared to the amount of regular Entities.
  //    > Turning threading on, you'll be wasting your time traversing the list from multiple threads in search of InstanceBatchHW & InstanceBatchHW_VTF."
  //  "When to use INSTANCING_CULLING_THREADED?
  //    > If your scene makes intensive use of HW Basic and/or HW VTF instancing techniques.
  //    > Note that threaded culling is performed in SCENE_STATIC instances too.
  //    > The most advantage is seen when the instances per batch is very high and when doing many PASS_SCENE, 
  //      which require frustum culling multiple times per frame (eg. pssm shadows, multiple light sources with shadows, very advanced compositing, etc)
  //  "Note that you can switch between methods at any time at runtime."
  Ogre::InstancingThreadedCullingMethod eThreadedCullingMethod = Ogre::INSTANCING_CULLING_SINGLETHREAD;
#if OGRE_DEBUG_MODE
  //Debugging multithreaded code is a PITA, disable it.
  const size_t nThreads = 1;
#else
  //getNumLogicalCores() may return 0 if couldn't detect
  const size_t nThreads = std::max<size_t>(1, Ogre::PlatformInformation::getNumLogicalCores());
  //keep as it is for now
  //if(numThreads > 1)
  //  threadedCullingMethod = Ogre::INSTANCING_CULLING_THREADED;
#endif

  // set scene manager
  pSceneManager = pRoot->createSceneManager(Ogre::ST_GENERIC, nThreads, eThreadedCullingMethod, "DefaultSceneManager");

  // set shadow limits
  float fSvd = gPrefs->getAsFloat("ShadowVolumeDistance");
  float fSfd = gPrefs->getAsFloat("ShadowFarDistance");
  pSceneManager->setShadowDirectionalLightExtrusionDistance(0.0f != fSvd ? fSvd : 500.0f);
  pSceneManager->setShadowFarDistance(0.0f != fSfd ? fSfd : 500.0f);

  // instantiate overlay system for any future use and notify scene manager
  pOverlaySystem = OGRE_NEW Ogre::v1::OverlaySystem();
  pSceneManager->addRenderQueueListener(pOverlaySystem);

  // load resources
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  // set default camera
  pCamera = pSceneManager->createCamera("MainCamera");
  pCamera->setAutoAspectRatio(true);
  pCamera->setNearClipDistance(1.0f);
  pCamera->setFarClipDistance(10000.0f);
  pCamera->setFixedYawAxis(true, Ogre::Vector3::UNIT_Z);
  pCamera->setPosition(1000, 0, 250);
  //pCamera->setPosition(0, 0, 10);
  //pCamera->lookAt(1000, 500, 0);

  // set up a very basic compositor
  Ogre::IdString sWorkspaceName("DefaultWorkspace");
  pRoot->getCompositorManager2()->createBasicWorkspaceDef(sWorkspaceName, Ogre::ColourValue(0.1f, 0.1f, 0.8f), Ogre::IdString());
  pWorkspace = pRoot->getCompositorManager2()->addWorkspace(pSceneManager, pWindow, pCamera, sWorkspaceName, true); // eq. start rendering
  // complexe composition will require a func.

  // build console output
  pConsole = new ObscurusConsole();
  gLogger->addListener(this);
}

// --------------------------------------------------------------------------------------------------- REDIRECT LOG TO CONSOLE
void ObscurusRenderer::log(const char* _lvl, const char* _msg)
{
  if(pConsole == 0) return;
  std::string sLvl(_lvl);
  if(!gPrefs->getAsBool("DebugMode") && (sLvl == "INFO" || sLvl == "DEBG")) return;
  Ogre::String sText = "[" + Ogre::String(_lvl) + "] " + Ogre::String(_msg);
  Ogre::ColourValue cColour;
  if(sLvl == "ERRR") cColour = Ogre::ColourValue::Red;
  if(sLvl == "WARN") cColour = Ogre::ColourValue(1.0f, 0.4f, 0.0f);
  pConsole->addLine(sText, cColour);
}

//
void ObscurusRenderer::moveCamera(float _xVel, float _yVel, float _zVel, float _y, float _p, float _r)
{
  pCamera->moveRelative(Ogre::Vector3(_xVel, _yVel, _zVel));
  pCamera->yaw(Ogre::Degree(_y));
  pCamera->pitch(Ogre::Degree(_p));
  pCamera->roll(Ogre::Degree(_r));
}
