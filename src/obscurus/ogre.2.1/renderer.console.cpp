#include "../renderer.h"

ObscurusConsoleLine::ObscurusConsoleLine(unsigned char _id, Ogre::ColourValue _textColour)
  : fLifeTime(0.0f)
{
  cTextColour = _textColour;
  cShadowColour = Ogre::ColourValue::Black;
  Ogre::v1::OverlayManager* pOverlayManager = Ogre::v1::OverlayManager::getSingletonPtr();
  pText = static_cast<Ogre::v1::TextAreaOverlayElement*>(pOverlayManager->createOverlayElement("TextArea", "ConsoleTextLine" + Ogre::StringConverter::toString(_id)));
  pShadow = static_cast<Ogre::v1::TextAreaOverlayElement*>(pOverlayManager->createOverlayElement("TextArea", "0ConsoleShadowLine" + Ogre::StringConverter::toString(_id)));
  pText->setFontName("ConsoleFontBold");
  pText->setCharHeight(0.025f);
  pText->setColour(cTextColour);
  pShadow->setFontName("ConsoleFontBold");
  pShadow->setCharHeight(0.025f);
  pShadow->setColour(cShadowColour);
  pShadow->setLeft(0.0025f);
}
ObscurusConsoleLine::~ObscurusConsoleLine()
{
  Ogre::v1::OverlayManager::getSingletonPtr()->destroyOverlayElement(pText);
  Ogre::v1::OverlayManager::getSingletonPtr()->destroyOverlayElement(pShadow);
}

ObscurusConsole::ObscurusConsole()
  : lineId(0)
{
  memset(&vLines, 0, sizeof(vLines));
  Ogre::v1::OverlayManager* pOverlayManager = Ogre::v1::OverlayManager::getSingletonPtr();
  Ogre::v1::Overlay* pOverlay = pOverlayManager->create("ConsoleOverlay");
  pPanel = static_cast<Ogre::v1::OverlayContainer*>(pOverlayManager->createOverlayElement("Panel", "ConsolePanel"));
  pPanel->setPosition(0.01f, 0.0f);
  pOverlay->add2D(pPanel);
  pOverlay->show();
  fLineLifeTime = gPrefs->getAsFloat("ConsoleLinesLifeTime") == 0.0f ? 10.0f : gPrefs->getAsFloat("ConsoleLinesLifeTime");
}

ObscurusConsole::~ObscurusConsole()
{
  for(int i = 0; i < 28; i++)
  {
    if(vLines[i])
      delete vLines[i];
  }
}

void ObscurusConsole::write(const std::string& _message, Ogre::ColourValue _colour)
{
  // first, lower the panel and raise the lines
  pPanel->setTop(0.035f); // 0.025 (line height) + 0.01 (line separator)
  if(vLines[27])
  {
    delete vLines[27];
    vLines[27] = 0;
  }
  for(int i = 26; i > -1; i--)
  {
    if(vLines[i])
    {
      vLines[i]->pText->setTop(vLines[i]->pText->getTop() - 0.035f);
      vLines[i]->pShadow->setTop(vLines[i]->pShadow->getTop() - 0.035f);
      vLines[i + 1] = vLines[i];
    }
  }
  vLines[0] = new ObscurusConsoleLine(lineId++, _colour);
  vLines[0]->pText->setTop(1.0f - 0.035f);
  vLines[0]->pShadow->setTop(1.0f - 0.035f + 0.0025f);
  vLines[0]->pText->setCaption(_message);
  vLines[0]->pShadow->setCaption(_message);
  pPanel->addChild(vLines[0]->pText);
  pPanel->addChild(vLines[0]->pShadow);
}

void ObscurusConsole::draw()
{
  // darken all lines
  Ogre::ColourValue colour;
  for(int i = 0; i < 28; i++)
  {
    if(vLines[i])
    {
      vLines[i]->fLifeTime += (float)gTime;
      if(vLines[i]->fLifeTime > fLineLifeTime)
      {
        delete vLines[i];
        vLines[i] = 0;
      }
      else
      {
        float f = (vLines[i]->fLifeTime == 0.0f ? 0.00001f : vLines[i]->fLifeTime) / fLineLifeTime;
        colour = vLines[i]->pText->getColour();
        colour.a = 1.0f - f;
        vLines[i]->pText->setColour(colour);
        colour = vLines[i]->pShadow->getColour();
        colour.a = 1.0f - f;
        vLines[i]->pShadow->setColour(colour);
      }
    }
  }
  // raise the panel if lowered
  if(pPanel->getTop() > 0.0f)
  {
    float f = pPanel->getTop() - ((float)gTime / 3);
    f < 0.0f ? pPanel->setTop(0.0f) : pPanel->setTop(f);
  }
  else // inject new message
  {
    if(qMsg.size())
    {
      write(qMsg.front().message, qMsg.front().colour);
      qMsg.pop();
    }
  }
}

void ObscurusConsole::addLine(const std::string& _message, Ogre::ColourValue _colour)
{
  ObscurusConsoleMessage msg;
  msg.message = _message;
  msg.colour = _colour;
  qMsg.push(msg);
}
