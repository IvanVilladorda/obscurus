#include "preferences.h"
#include <boost/filesystem.hpp>
#include "log.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
template<> ObscurusPrefs* Singleton<ObscurusPrefs>::inst = 0;
ObscurusPrefs* ObscurusPrefs::getSingleton(){ return inst; }

#define _OBS_BASE_CONFIG_FILE_ "cfg.ini"
#define _OBS_USER_CONFIG_FILE_ "config_user.ini"

namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

ObscurusPrefs::ObscurusPrefs(const std::string& _cfgFileApp, const std::string& _cfgFileUser)
{
  gLog("-*-*- Preferences Startup.", 'I');
  std::string sStr = _cfgFileApp;
  // Default Preferences
  if(sStr.empty()) sStr = _OBS_BASE_CONFIG_FILE_;
  loadConfigFile(sStr);

  // User Preferences override
  //sStr = _cfgFileUser;
  //if(sStr.empty()) sStr = _OBS_USER_CONFIG_FILE_;
  //loadConfigFile(sStr);
}

ObscurusPrefs::~ObscurusPrefs()
{
  gLog("*-*-* Preferences Shutdown.", 'I');
  gLog("Outputting all preferences set.", 'D');
  for(std::map<std::string, std::string>::iterator it = vPreferences.begin(); it != vPreferences.end(); ++it)
    gLog("\t %s = %s", 'D', it->first.c_str(), it->second.c_str());
}

void ObscurusPrefs::loadConfigFile(const std::string& _filename)
{
  if(!fs::exists(_filename))
  {
    gLog("Preferences file %s does not exists. Creating it.", 'W', _filename.c_str());
    FILE* file = fopen(_filename.c_str(), "w");
    fclose(file);
  }
  else
  {
    gLog("Loading preferences from %s.", 'I', _filename.c_str());
    int _numPrefs = 0;
    pt::ptree pt;
    pt::ini_parser::read_ini(_filename, pt);
    for(pt::ptree::iterator itSection = pt.begin(); itSection != pt.end(); ++itSection)
    {
      for(pt::ptree::iterator itKey = itSection->second.begin(); itKey != itSection->second.end(); ++itKey)
      {
        std::string name = itKey->first;
        name.erase(std::remove(name.begin(), name.end(), '#'), name.end());
        name.erase(std::remove(name.begin(), name.end(), ';'), name.end());
        if(name.size() && name.at(0) != '_') // never load execution-time preferences (underscores mark them)
        {
          std::string value = itKey->second.get_value<std::string>();
          value.erase(std::remove(value.begin(), value.end(), '#'), value.end());
          value.erase(std::remove(value.begin(), value.end(), ';'), value.end());
          if(value.size())
          {
            boost::trim(name);
            boost::trim(value);
            vPreferences[name] = value;
            gLog("\t- %s = %s", 'I', name.c_str(), value.c_str());
            _numPrefs++;
          }
        }
      }
    }
    gLog("%d preferences loaded.", 'I', _numPrefs);
  }
}

void ObscurusPrefs::saveConfigFile(const std::string& _filename)
{
  gLog("Saving preferences in %s.", 'I', _filename.c_str());
  FILE* file = fopen(_filename.c_str(), "w");
  if(!file)
  {
    gLog("Saving canceled : file cannot be opened.", 'E');
    return;
  }

  for(std::map<std::string, std::string>::iterator it = vPreferences.begin(); it != vPreferences.end(); ++it)
  {
    fprintf(file, "%s = %s\n", it->first, it->second);
  }
  fclose(file);
}

void ObscurusPrefs::set(const std::string& _key, const std::string& _value)
{
  if(_key.at(0) == '_') gLog("Setting preference %s to %s.", 'D', _key.c_str(), _value.c_str());
  else gLog("Setting preference %s to %s.", 'I', _key.c_str(), _value.c_str());
  vPreferences[_key] = _value;
}

std::string ObscurusPrefs::get(const std::string& _key)
{
  return vPreferences[_key]; // empty if non existent
}
int ObscurusPrefs::getAsInt(const std::string& _key)
{
  return std::atoi(vPreferences[_key].c_str());
}
long ObscurusPrefs::getAsLong(const std::string& _key)
{
  return std::atol(vPreferences[_key].c_str());
}
bool ObscurusPrefs::getAsBool(const std::string& _key)
{
  return vPreferences[_key] == true;
}
float ObscurusPrefs::getAsFloat(const std::string& _key)
{
  return (float)std::atof(vPreferences[_key].c_str());
}
double ObscurusPrefs::getAsDouble(const std::string& _key)
{
  return std::atof(vPreferences[_key].c_str());
}
const char* ObscurusPrefs::getAsString(const std::string& _key)
{
  return vPreferences[_key].c_str();
}

