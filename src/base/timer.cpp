// -- TIMER 
#include "timer.h"
#include "log.h"
#include <ctime>

template<> ObscurusTimer* Singleton<ObscurusTimer>::inst = 0;
ObscurusTimer* ObscurusTimer::getSingleton(){ return inst; }

ObscurusTimer::ObscurusTimer()
  : dElapsed(0),
  dAppTime(0)
{
  gLog("-*-*- Timer Startup.", 'I');
  oTime = std::chrono::system_clock::now();
}

ObscurusTimer::~ObscurusTimer()
{
  gLog("*-*-* Timer Shutdown.", 'I');
  getElapsed(); // final one
  gLog("Execution finished after %f seconds of execution.", 'I', dAppTime);
}

// -- APP TIMING METHODS
double ObscurusTimer::getElapsed(bool _switch)
{
  if(_switch)
  {
    auto now = std::chrono::system_clock::now();
    std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(now - oTime);
    dElapsed = time_span.count();
    dAppTime += dElapsed;
    oTime = now;
  }
  return dElapsed;
}

int ObscurusTimer::getCurrentYear()
{
  std::string str;
  auto now = std::chrono::system_clock::now();
  std::time_t now_c = std::chrono::system_clock::to_time_t(now);
  struct tm *parts = std::localtime(&now_c);
  return parts->tm_year + 1900;
}

// -- CALENDAR METHODS
namespace OBS
{
  void Calendar::setProtoYear(int _realYear, int _protoYear, unsigned char _numSecPerMin)
  {
    if(_protoYear == 0)
    {
      gLog("Proto year cannot be zero. Adjusting to 1.", 'W');
      _protoYear = 1;
    }
    iProtoYear = _protoYear;
    iRealYear = _realYear;
    iSecInMin = _numSecPerMin;
    iSecInHour = iSecInMin * 50;
    iSecInDay = iSecInHour * 20;
    iSecInWeek = iSecInDay * 5;
    iSecInMonth = iSecInWeek * 4;
    iSecInYear = iSecInMonth * 10;
  }

  long Calendar::getTimestamp()
  {
    return 0;
  }
}
/*
void ObscurusTimer::getImperialTime(ImperialTime* iT)
{
  long iSecs = getTimestamp();

  iT->year = iProtoYear;
  iT->nolYear = false;
  while(iSecs >= iSecInYear)  {  iT->year++;    if(iT->year == 0) iT->year++;  iSecs-=iSecInYear;  }
  while(iSecs >= iSecInMonth)  {  iT->month++;                                iSecs-=iSecInMonth;  }
  while(iSecs >= iSecInWeek)  {  iT->week++;                                  iSecs-=iSecInWeek;  }
  while(iSecs >= iSecInDay)    {  iT->day++;                                  iSecs-=iSecInDay;    }
  while(iSecs >= iSecInHour)  {  iT->hour++;                                  iSecs-=iSecInHour;  }
  while(iSecs >= iSecInMin)    {  iT->min++;                                  iSecs-=iSecInMin;    }
  iT->sec = iSecs;

  // need to be improved to solar system time (200.0625 days a year) with Nol day each cycle
  // lunar cycle is exact :
  // - 0 year   : moons in same axis => Nol of Ayon
  // - 16 years : moons opposed => Nol of Ayon
  // - 32 years : moons reunited => Nol of Ayon
  // BUT the first Nol of Ayon was in the year 8
  if(!iT->year % 8)
  {
    if((iT->year == 8)|(iT->year == -8))  iT->nolYear = true;
    else if(!iT->year % 16)              iT->nolYear = true;
  }
}
*/
