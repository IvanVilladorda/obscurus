#include "logics.h"
// -------------------------------------------------------------------------------------------- DEBUG OUTPUT
void ObscurusLogics::_debugOutput(const std::string& _scriptName)
{
  ObscurusScriptData* _c = (ObscurusScriptData*)gData->get(OBS::OBJ_SCRIPT, _scriptName);
  LOCK_DATA;
  if(_c  && !_c->has_been_outputted)
  {
    std::string _fileName = _c->file_name + ".cpl";
    FILE* file = fopen(_fileName.c_str(), "w");
    if(!file)
    {
      addMsgToQueue(new OBS::LogicsMsgLog("Debug output of script \"" + _scriptName + "\" denied : cannot write file.", 'W'), OBS::DEST_LOGICS);
      return;
    }
    fprintf(file, _c->compiled.c_str());
    fclose(file);
    _c->has_been_outputted = true;
  }
}
// -------------------------------------------------------------------------------------------- CREATE CHARACTER
// create a character in database
// x, y are grid point in worldspace
long long ObscurusLogics::_createCharacter(const std::string& _characterDataName, const std::string& _worldDataName, int _x, int _y)
{
  ObscurusCharacterData* charData = nullptr;
  if(!_characterDataName.empty()) // pass "" to create a null character
  {
    // otherwise, the name must match
    charData = (ObscurusCharacterData*)gData->get(OBS::OBJ_CHARACTER, _characterDataName);
    if(!charData)
    {
      addMsgToQueue(new OBS::LogicsMsgLog("Creating character \"" + _characterDataName + "\" denied : not defined.", 'E'), OBS::DEST_LOGICS);
      return 0;
    }
    // if defined as unique, prevent re-creation
    if(charData->unique)
    {
      std::string sql = "SELECT chId FROM character WHERE chData = \"" + _characterDataName + "\"";
      if(gDB->exec(sql)->numRows)
      {
        addMsgToQueue(new OBS::LogicsMsgLog("Creating character \"" + _characterDataName + "\" denied : must be unique.", 'E'), OBS::DEST_LOGICS);
        return 0;
      }
    }
  }
  // get the world
  size_t worldIdx = _loadWorld(_worldDataName);
  if(worldIdx == SIZE_MAX)
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Creating character \"" + _characterDataName + "\" denied : cannot load world \"" + _worldDataName + "\".", 'E'), OBS::DEST_LOGICS);
    return 0;
  }
  // create the character
  OBS::Character* newCharacter = new OBS::Character(_worldDataName, _x, _y);
  if(charData) newCharacter->copyFromData(charData);
  // inject in database now
  gDB->insert(newCharacter);
  // grab auto created id
  long long id = newCharacter->id;
  // check active space
  if(_isInActiveRange(_x, _y))
  {
    // add to arrays
    _addEntity((OBS::Entity*)newCharacter);
//    size_t characterIdx = vEntities.size();
//    vEntities.push_back(newCharacter);
//    mEntityIds[newCharacter->id] = characterIdx;
  }
  else
    delete newCharacter;
  return id;
}
// -------------------------------------------------------------------------------------------- LOAD WORLD
size_t ObscurusLogics::_loadWorld(const std::string& _worldDataName)
{
  LOCK_WORLDS;
  size_t _worldIdx = SIZE_MAX;
  // check first if the world exists in memory
  try
  {
    _worldIdx = mWorldNames.at(_worldDataName); // already loaded ?
    return _worldIdx;
  }
  catch(...) {}
  // the world must have been defined
  ObscurusWorldData* worldData = (ObscurusWorldData*)gData->get(OBS::OBJ_WORLD, _worldDataName);
  if(!worldData)
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Loading world \"" + _worldDataName + "\" denied : not defined.", 'E'), OBS::DEST_LOGICS);
    return SIZE_MAX;
  }
  OBS::World* _world = new OBS::World();
  _world->copyFromData(worldData);
  _worldIdx = vWorlds.size();
  vWorlds.push_back(_world);
  mWorldNames[_worldDataName] = _worldIdx;
  return _worldIdx;
}
// -------------------------------------------------------------------------------------------- IS IN ACTIVE RANGE
bool ObscurusLogics::_isInActiveRange(int _x, int _y)
{
  /*
  for(size_t i = 0; i < vCells.size(); i++)
  {
    if(vCells[i]->
  }
  */
  return false;
}
// -------------------------------------------------------------------------------------------- IS ACTIVE
bool ObscurusLogics::_isActive(long long _entityId)
{
  try
  {
    size_t entityIdx = mEntityIds.at(_entityId);
    for(size_t i = 0; i < vEntityActives.size(); i++)
    {
      if(vEntityActives[i] == entityIdx)
        return true;
    }
  }
  catch(...) {}
  return false;
}
// -------------------------------------------------------------------------------------------- GRANT CHARACTER TO PLAYER
void ObscurusLogics::_grantCharacterToPlayer(long long _id, const std::string& _userName)
{
  std::string sql = "SELECT chUserName, chName FROM character WHERE chId = " + std::to_string(_id);
  auto rows = gDB->exec(sql);
  if(!rows->numRows)
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Assign character denied : id not found.", 'E'), OBS::DEST_LOGICS);
    return;
  }
  if(!rows->rows[0]["chUserName"].empty())
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Assign character \"" + rows->rows[0]["chName"] + "\" denied : already owned by \"" + rows->rows[0]["chUserName"] + "\".", 'E'), OBS::DEST_LOGICS);
    return;
  }
  sql = "UPDATE character SET chUserName = \"" + _userName + "\" WHERE chId = " + std::to_string(_id);
  gDB->exec(sql);
  try
  {
    size_t userIdx = mUserNames[_userName];
    bool bFound = false;
    for(size_t i = 0; i < vUserCharacters[userIdx].size(); i++)
    {
      if(vUserCharacters[userIdx][i] == _id)
        bFound = true;
    }
    if(!bFound)
      vUserCharacters[userIdx].push_back(_id);
  }
  catch(...) {}
  if(_userName == "PLAYER")
    addMsgToQueue(new OBS::LogicsMsgAssignCharacterToPlayer(_id), OBS::DEST_PLAYER);
}
// -------------------------------------------------------------------------------------------- FORCE PLAYER TO INCARNATE
void ObscurusLogics::_forcePlayerToIncarnate(long long _id, const std::string& _userName)
{
  LOCK_ENTITIES;
  // first, remove existing active entity (only check the current character, if any)
  try
  {
    size_t userIdx = mUserNames[_userName];
    if(vEntityActives.at(vUserCurrentCharacter.at(userIdx)))
      vEntityActives[vUserCurrentCharacter[userIdx]] = 0;
  }
  catch(...) { }
  // set the current character and made it active
  try
  {
    size_t userIdx = mUserNames[_userName];
    bool bFound = false;
    for(size_t i = 0; i < vUserCharacters[userIdx].size(); i++)
    {
      if(vUserCharacters[userIdx][i] == _id)
        bFound = true;
    }
    if(bFound)
    {
      bFound = false;
      vUserCurrentCharacter[userIdx] = _id;
      for(size_t i = 0; i < vEntityActives.size(); i++)
      {
        if(vEntityActives[i] == 0)
        {
          bFound = true;
          vEntityActives[i] = _id;
        }
      }
      if(!bFound)
        vEntityActives.push_back(_id);
    }
  }
  catch(...)
  {
    return;
  }
  if(_userName == "PLAYER")
    addMsgToQueue(new OBS::LogicsMsgEmbodyCharacter(_id), OBS::DEST_PLAYER);
}
// -------------------------------------------------------------------------------------------- START GAME
void ObscurusLogics::_startGame(const std::string& _userName)
{
  try
  {
    long long id = vUserCurrentCharacter.at(mUserNames.at(_userName));
    addMsgToQueue(new OBS::LogicsMsgLog("Starting game.", 'I'), OBS::DEST_LOGICS);
    addMsgToQueue(new OBS::LogicsMsgGameStart(), OBS::DEST_CORE);
    addMsgToQueue(new OBS::LogicsMsgGameStart(), OBS::DEST_PLAYER);
  }
  catch(...)
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Cannot start game : prerequisites not met.", 'W'), OBS::DEST_LOGICS);
    addMsgToQueue(new OBS::LogicsMsgReturnToMainMenu(), OBS::DEST_CORE);
  }
}
// -------------------------------------------------------------------------------------------- GET CELL
size_t ObscurusLogics::_getCell(const std::string& _world, short _x, short _y) // _x and _y are grid coords (1 meter step)
{
  size_t worldIdx = _loadWorld(_world);
  for(size_t i = 0; i < vCells.size(); i++)
  {
    if(vCells[i] && vCells[i]->world == vWorlds[worldIdx])
    {
      if(vCells[i]->coordX == _x && vCells[i]->coordY == _y)
        return i;
    }
  }
  LOCK_CELLS;
  // lookup world map to get terrain data
  ObscurusTerrainData* terrainData = vWorlds[worldIdx]->getTerrain(_x, _y);
  // create and add a new cell
  OBS::Cell* cell = new OBS::Cell(vWorlds[worldIdx], _x, _y, terrainData);
  // check for a free slot
  for(size_t i = 0; i < vCells.size(); i++)
  {
    if(vCells[i] == 0)
    {
      vCells[i] = cell;
      return i;
    }
  }
  // else push it
  if(vCells.capacity() - vCells.size() == 1) vCells.reserve(vCells.capacity() + 9);
  size_t cellIdx = vCells.size();
  vCells.push_back(cell);
  return cellIdx;
}
// -------------------------------------------------------------------------------------------- GET ENTITY
OBS::Entity* ObscurusLogics::_getEntity(long long _id)
{
  OBS::Entity* entity = 0;
  try
  {
    size_t entityIdx = mEntityIds.at(_id);
    entity = vEntities.at(entityIdx);
    if(entity)
      return entity;
  }
  catch(...) {}
  // load it
  std::string sql = "SELECT * FROM entity WHERE enId = " + std::to_string(_id);
  auto rowEntity = gDB->exec(sql);
  if(!rowEntity->numRows)
  {
    addMsgToQueue(new OBS::LogicsMsgLog("Unknown entity's id : " + std::to_string(_id) + ".", 'W'), OBS::DEST_LOGICS);
    return 0;
  }
  std::string sType = rowEntity->rows.at(0)["enTypeName"];
  if(sType == "") // pure entity
  {
    OBS::Entity* entity = new OBS::Entity(rowEntity->rows.at(0));
    _addEntity(entity);
    return entity;
  }
  else if(sType == "CHARACTER")
  {
    sql = "SELECT * FROM entity INNER JOIN character ON chId = enId WHERE enId = " + std::to_string(_id);
    auto rowCharacter = gDB->exec(sql);
    OBS::Character* character = new OBS::Character(rowEntity->rows.at(0));
    _addEntity((OBS::Entity*)character);
    return (OBS::Entity*)character;
  }
  return 0;
}
// -------------------------------------------------------------------------------------------- ADD ENTITY
void ObscurusLogics::_addEntity(OBS::Entity* _entity)
{
  if(!_entity->id) return;
  LOCK_ENTITIES;
  for(size_t i = 0; i < vEntities.size(); i++)
    if(vEntities[i] == _entity) return;
  if(vEntities.capacity() - vEntities.size() == 1) vEntities.reserve(vEntities.capacity() + 1000);
  size_t entityIdx = vEntities.size();
  vEntities.push_back(_entity);
  mEntityIds[_entity->id] = entityIdx;
}
