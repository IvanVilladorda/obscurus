#ifndef _OBS_SCRIPT_H_
#define _OBS_SCRIPT_H_
#include <unordered_map>
#include <mutex>
#include <vector>
#include <thread>
#include <boost/any.hpp>
#include "data.h"
#include "messages.h"

namespace OBS
{
  enum ScriptCode
  {
    SC_INIT,      // script has not launched now
    SC_COMPILING,  // script is compiling
    SC_EXECUTING,  // script parsing and interpreting
    SC_SUCCESS,    // script has reached the end of contents
    SC_FAILURE,    // script has failed somewhere
    SC_NONAME,    // script has no name
    SC_UNKNOWN,    // script has no content
    SC_ABORTED,    // script has been forced to leave
    SC_BRACES_MISMATCH,        // script compilation leads to braces mismatch
    SC_PARENTHESIS_MISMATCH,  // script compilation leads to parenthesis mismatch
    SC_BRACKETS_MISMATCH,      // script compilation leads to brackets mismatch
    SC_GS_MM,      // special GameStart : return to main menu
    SC_GS_OK      // special GameStart : success and ready to start game
  };
  enum ScriptType
  {
    ST_NUMBER,
    ST_STRING,
    ST_ARRAY,
    ST_CHARACTER,
    ST_ENTITY,
    ST_WORLD,
    ST_NULL        // special value (not found, etc.) cannot be equal to itself (for objects comparison)
  };
  struct ScriptValue
  {
    ScriptValue() { type = ST_NULL; }
    ScriptType type;
    boost::any value;
  };
  class Script
  {
  public:
    Script(const std::string& _scriptName, bool _internal = false);
    void exec();  // execute script in a thread
    void call();  // call script unthreaded
    void interrupt();

    std::thread oThread; // needed to join
    ScriptValue oReturn;
    std::vector<boost::any> vParameters; // use vParameters.push_back(what);
  private:
    void interpretScript();
    void replaceOccurences(std::string&, const std::vector<std::string>&, const std::vector<std::string>&, bool _recur = false);
    bool compileScript(std::string&);
    void interpretLine();
    void finishExecution();

    size_t nLine;
    bool bInternal;      // only for scripts called by scripts (in child)
    bool bInterrupt;
    bool bThreaded;
    std::mutex mutex; // mutex covers bInterrupt
    ScriptCode eCode; // will be sent to logics (see LogicsMsgScriptStatus below)
    std::string sScriptName;
    std::vector<std::vector<std::string>> vTokens;
    std::unordered_map<std::string, ScriptValue> mVariables;
  };
  struct LogicsMsgScriptStatus : LogicsMsg
  {
    LogicsMsgScriptStatus(const std::string& _scriptName, ScriptCode _eCode, bool _threaded = true) { eId = MSG_SCRIPT_STATUS; eCode = _eCode; sScriptName = _scriptName; bThreaded = _threaded; }
    ScriptCode eCode;
    std::string sScriptName;
    bool bThreaded;
  };
}
#endif