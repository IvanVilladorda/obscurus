#ifndef _OBS_ADDRESS_H_
#define _OBS_ADDRESS_H_

namespace OBS
{
  struct Address
  {
    Address() { chunkIndex = 0; parcelIndex = tileIndex = spotIndex = pointIndex = -1; }
    //size_t cellIndex; do not use since pixel data is chunks
    size_t chunkIndex;
    char parcelIndex;
    char tileIndex;
    char spotIndex;
    char pointIndex;
  };
}
#endif