#ifndef _OBS_SCORE_H_
#define _OBS_SCORE_H_

namespace OBS
{
  struct Score
  {
    Score() { val = max = exp = 0.0f; }
    float val, max, exp;
  };
}
#endif
