#ifndef _OBS_ANYTHING_H_
#define _OBS_ANYTHING_H_
#include "data/data.object.h"
namespace OBS
{
  struct Anything
  {
    Anything() {}
    virtual void copyFromData(Object*) = 0;
  };
}
#endif