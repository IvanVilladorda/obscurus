#include "cell.h"

namespace OBS
{
  Cell::Cell(World* _w, short _x, short _y, ObscurusTerrainData* _t)
  {
    memset(&hMap, 0, sizeof(hMap)); memset(&rMap, 0, sizeof(rMap)); memset(&neighbors, 0, sizeof(neighbors));
    coordX = _x; coordY = _y;
    timeOut = 0.0f;
    world = _w;
    terrain = _t;
    state = CL_UNBUILT;
    tube = CL_OUT;
  }

  void Cell::buildCell()
  {
    state = CL_BUILDING;
    float O, E, V, K, L, H;
    float Os, Hs, Vs;
    float C = terrain->elevation * world->elevationMult;
    hMap[128][128] = C;
    ObscurusTerrainData* other;
    // heightmap slopes merge
    // the simpliests (N, E, S, W)
    // explanation with estearn cell :
    //   C -- I -- A -- E -- O
    // where :
    //  C = current cell elevation
    //  O = other cell elevation
    //  A = average elevation on edge between I and E
    //  I = interior elevation at half distance to the edge (sloped)
    //  E = exterior elevation at half distance to the edge (sloped)
    // and :
    //  C = elevation * elevationFactor
    //  O = elevation * elevationFactor
    //  I = (C + O) / 2 + C-slope * (C - (3 * C + O) / 4)
    //  E = (O + C) / 2 + O-slope * (O - (3 * O + C) / 4)
    //  A = (I + E) / 2
    if(neighbors[DIR_N]) other = neighbors[DIR_N]->terrain;  else other = world->getTerrain(coordX, coordY + 1);
    O = other->elevation * world->elevationMult;
    E = (O + C) * 0.5f + other->slope * (O - (3 * O + C) * 0.25f);
    hMap[128][64] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[128][0] = (hMap[128][64] + E) * 0.5f;
    if(neighbors[DIR_S]) other = neighbors[DIR_S]->terrain;  else other = world->getTerrain(coordX, coordY - 1);
    O = other->elevation * world->elevationMult;
    E = (O + C) * 0.5f + other->slope * (O - (3 * O + C) * 0.25f);
    hMap[128][192] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[128][256] = (hMap[128][192] + E) * 0.5f;
    if(neighbors[DIR_E]) other = neighbors[DIR_E]->terrain;  else other = world->getTerrain(coordX + 1, coordY);
    O = other->elevation * world->elevationMult;
    E = (O + C) * 0.5f + other->slope * (O - (3 * O + C) * 0.25f);
    hMap[192][128] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[256][128] = (hMap[192][128] + E) * 0.5f;
    if(neighbors[DIR_W]) other = neighbors[DIR_W]->terrain;  else other = world->getTerrain(coordX - 1, coordY);
    O = other->elevation * world->elevationMult;
    E = (O + C) * 0.5f + other->slope * (O - (3 * O + C) * 0.25f);
    hMap[64][128] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[0][128] = (hMap[64][128] + E) * 0.5f;
    // the trickiest (NW, NE, SW, SE)
    // explanation on NE corner :
    //   V               O
    //   :           
    //   x---K---x---E
    //   :   :   :   :
    //   x___B1__A---x
    //   |   |   |   :
    //   x___I___B2--L
    //   |   |   |   :
    //   C___x___x---x---H
    // where :
    // xs are ignored
    // I = (C + O) / 2 + C-slope * (C - (3 * C + O) / 4)
    // E = (O + C) / 2 + O-slope * (O - (3 * O + C) / 4)
    // K = (V + H) / 2 + V-slope * (V - (3 * V + H) / 4)
    // L = (H + V) / 2 + H-slope * (H - (3 * H + V) / 4)
    // A = average of I, E, K and L
    // B1 = average of I and K
    // B2 = average of I and L
    if(neighbors[DIR_NW]) other = neighbors[DIR_NW]->terrain;  else other = world->getTerrain(coordX - 1, coordY + 1);
    O = other->elevation * world->elevationMult;      Os = other->slope;
    if(neighbors[DIR_N]) other = neighbors[DIR_N]->terrain;  else other = world->getTerrain(coordX, coordY + 1);
    V = other->elevation * world->elevationMult;      Vs = other->slope;
    if(neighbors[DIR_W]) other = neighbors[DIR_W]->terrain;  else other = world->getTerrain(coordX - 1, coordY);
    H = other->elevation * world->elevationMult;      Hs = other->slope;
    E = (O + C) * 0.5f + Os * (O - (3 * O + C) * 0.25f);
    K = (V + H) * 0.5f + Vs * (V - (3 * V + H) * 0.25f);
    L = (H + V) * 0.5f + Hs * (H - (3 * H + V) * 0.25f);
    hMap[64][64] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[0][0] = (hMap[64][64] + E + K + L) * 0.25f;
    hMap[64][0] = (hMap[64][64] + K) * 0.5f;
    hMap[0][64] = (hMap[64][64] + L) * 0.5f;
    if(neighbors[DIR_NE]) other = neighbors[DIR_NE]->terrain;  else other = world->getTerrain(coordX + 1, coordY + 1);
    O = other->elevation * world->elevationMult;      Os = other->slope;
    if(neighbors[DIR_N]) other = neighbors[DIR_N]->terrain;  else other = world->getTerrain(coordX, coordY + 1);
    V = other->elevation * world->elevationMult;      Vs = other->slope;
    if(neighbors[DIR_E]) other = neighbors[DIR_E]->terrain;  else other = world->getTerrain(coordX + 1, coordY);
    H = other->elevation * world->elevationMult;      Hs = other->slope;
    E = (O + C) * 0.5f + Os * (O - (3 * O + C) * 0.25f);
    K = (V + H) * 0.5f + Vs * (V - (3 * V + H) * 0.25f);
    L = (H + V) * 0.5f + Hs * (H - (3 * H + V) * 0.25f);
    hMap[192][64] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[256][0] = (hMap[192][64] + E + K + L) * 0.25f;
    hMap[192][0] = (hMap[192][64] + K) * 0.5f;
    hMap[256][64] = (hMap[192][64] + L) * 0.5f;
    if(neighbors[DIR_SW]) other = neighbors[DIR_SW]->terrain;  else other = world->getTerrain(coordX - 1, coordY - 1);
    O = other->elevation * world->elevationMult;      Os = other->slope;
    if(neighbors[DIR_S]) other = neighbors[DIR_S]->terrain;  else other = world->getTerrain(coordX, coordY - 1);
    V = other->elevation * world->elevationMult;      Vs = other->slope;
    if(neighbors[DIR_W]) other = neighbors[DIR_W]->terrain;  else other = world->getTerrain(coordX - 1, coordY);
    H = other->elevation * world->elevationMult;      Hs = other->slope;
    E = (O + C) * 0.5f + Os * (O - (3 * O + C) * 0.25f);
    K = (V + H) * 0.5f + Vs * (V - (3 * V + H) * 0.25f);
    L = (H + V) * 0.5f + Hs * (H - (3 * H + V) * 0.25f);
    hMap[64][192] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[0][256] = (hMap[64][192] + E + K + L) * 0.25f;
    hMap[64][256] = (hMap[64][192] + K) * 0.5f;
    hMap[0][192] = (hMap[64][192] + L) * 0.5f;
    if(neighbors[DIR_SE]) other = neighbors[DIR_SE]->terrain;  else other = world->getTerrain(coordX + 1, coordY - 1);
    O = other->elevation * world->elevationMult;      Os = other->slope;
    if(neighbors[DIR_S]) other = neighbors[DIR_S]->terrain;  else other = world->getTerrain(coordX, coordY - 1);
    V = other->elevation * world->elevationMult;      Vs = other->slope;
    if(neighbors[DIR_E]) other = neighbors[DIR_E]->terrain;  else other = world->getTerrain(coordX + 1, coordY);
    H = other->elevation * world->elevationMult;      Hs = other->slope;
    E = (O + C) * 0.5f + Os * (O - (3 * O + C) * 0.25f);
    K = (V + H) * 0.5f + Vs * (V - (3 * V + H) * 0.25f);
    L = (H + V) * 0.5f + Hs * (H - (3 * H + V) * 0.25f);
    hMap[192][192] = (C + O) * 0.5f + terrain->slope * (C - (3 * C + O) * 0.25f);
    hMap[256][256] = (hMap[192][192] + E + K + L) * 0.25f;
    hMap[192][256] = (hMap[192][192] + K) * 0.5f;
    hMap[256][192] = (hMap[192][192] + L) * 0.5f;

    // heightmap bilinear interpolation
    // 16 chunks to proceed
    size_t x1, x2, y1, y2;
    for(size_t y = 0; y < 4; y++)
    {
      for(size_t x = 0; x < 4; x++)
      {
        x1 = x * 64;
        x2 = x * 64 + 64;
        y1 = y * 64;
        y2 = y * 64 + 64;
        for(size_t i = 0; i < 65; i++)
        {
          for(size_t j = 0; j < 65; j++)
          {
            L = ((x2 - j) / (x2 - x1)) * hMap[x1][y2] + ((j - x1) / (x2 - x1)) * hMap[x2][y2];
            H = ((x2 - j) / (x2 - x1)) * hMap[x1][y1] + ((j - x1) / (x2 - x1)) * hMap[x2][y1];
            hMap[x1 + j][y1 + i] = ((y2 - i) / (y2 - y1)) * L + ((i - y1) / (y2 - y1)) * H;
          }
        }
      }
    }

    // Noise building
    if(state < OBS::CL_EDGING) state = CL_BUILT;
  }
}
