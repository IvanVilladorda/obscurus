#include "script.h"
#include "logics.h"
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <ctype.h>

using namespace OBS;

Script::Script(const std::string& _scriptName, bool _internal)
  : bInterrupt(false),
  eCode(SC_INIT),
  nLine(0),
  sScriptName(_scriptName),
  bInternal(_internal),
  bThreaded(false)
{  
}

// --- LAUNCH
void Script::exec()
{
  bThreaded = true;
  call();
}

// -- CALL
void Script::call()
{
  if(vTokens.size())
    return interpretScript();

  // must have a name
  if(sScriptName.empty())
  {
    eCode = SC_NONAME;
    finishExecution();
    return;
  }
  // get data
  ObscurusScriptData* _c = (ObscurusScriptData*)gData->get(OBS::OBJ_SCRIPT, sScriptName);
  if(!_c)
  {
    eCode = SC_UNKNOWN;
    finishExecution();
    return;
  }
  else
  {
    LOCK_DATA;
    // get compiled status
    if(_c->compiled.empty())
    {
      _c->compiled = gData->loadScript(sScriptName);
      eCode = SC_COMPILING;
      if(!compileScript(_c->compiled))
      {
        finishExecution();
        return;
      }
    }
    std::vector<std::string> vLines;
    boost::split(vLines, _c->compiled, boost::is_any_of("\n"));
    for(size_t i = 0; i < vLines.size(); i++)
    {
      boost::tokenizer<boost::escaped_list_separator<char>> tokens(vLines[i], boost::escaped_list_separator<char>('\\', ' ', '\"'));
      std::vector<std::string> vToks;
      for(boost::tokenizer<boost::escaped_list_separator<char> >::iterator it = tokens.begin(); it != tokens.end(); ++it)
        vToks.push_back(*it);
      vTokens.push_back(vToks);
    }
  }

  // process to thread launch
#ifdef _DEBUG
  bool bUseNoThread = true;
#else
  bool bUseNoThread = false;
#endif
  if(!bUseNoThread && bThreaded)
    oThread = std::thread(&Script::interpretScript, this);
  else
    interpretScript();
}

// --- INTERRUPT
void Script::interrupt()
{
  std::lock_guard<std::mutex> lock(mutex);
  bInterrupt = true;
}

// --- INTERPRETSCRIPT
void Script::interpretScript()
{
  Script* oChild = nullptr;
  eCode = SC_EXECUTING;
  nLine = 0;
  while(nLine != SIZE_MAX)
  {
    interpretLine();
    //
    std::lock_guard<std::mutex> lock(mutex); // will be destroyed each while iteration
    if(bInterrupt)
    {
      if(oChild)
      {
        oChild->interrupt();
        oChild->oThread.join();
      }
      return; // cease exec at once
    }
  }
  finishExecution();
}

void Script::replaceOccurences(std::string& _string, const std::vector<std::string>& _chars, const std::vector<std::string>& _reps, bool _recur)
{
  // _chars are characters (or words) looked for replacement
  // _reps are characters (or words) to use in replacement
  // if something must be deleted instead of being replaced, make sure to place it at the end of _chars with no correspondance in _reps
  for(size_t i = 0; i < _string.size(); i++)
  {
    for(size_t k = 0; k < _chars.size(); k++)
    {
      if(_chars[k].size() == 1 && _string[i] == _chars[k][0])
      {
        if(k >= _reps.size()) _string.erase(i, 1);
        else if(_reps[k].size() == 1) _string[i] = _reps[k][0];
        else
        {
          _string.erase(i, 1);
          _string.insert(i, _reps[k]);
          if(!_recur) i += _reps[k].size() - 1;
          else if(i != 0) i--;
        }
      }
      else if(_chars[k].size() > 1 && _string.substr(i, _chars[k].size()) == _chars[k])
      {
        _string.erase(i, _chars[k].size());
        if(k < _reps.size())
        {
          _string.insert(i, _reps[k]);
          if(!_recur) i += _reps[k].size() - 1;
          else if(i != 0) i--;
        }
      }
    }
  }
}

// --- COMPILESCRIPT
bool Script::compileScript(std::string& sScript)
{
  // script compilation explode script in tokens, by line
  //   it takes some time, but we take it
  // -----------------------------------------------------------------------------------------------------------
  // first, useless characters are deleted
  replaceOccurences(sScript, 
  { "\t", "\\\"", "\r" },
  { " ",  "\"\"" });
  // -----------------------------------------------------------------------------------------------------------
  // second, comments are deleted
  size_t nBegin, nClose, nPos, nNum;
  while(1) // multilines comments
  {
    nBegin = sScript.find("/*");
    nClose = sScript.find("*/");
    if(nBegin == std::string::npos) break;
    sScript.erase(nBegin, nClose + 2 - nBegin);
  }
  while(1) // single line comments
  {
    nBegin = sScript.find("//");
    nClose = sScript.find("\n", nBegin);
    if(nBegin == std::string::npos) break;
    sScript.erase(nBegin, nClose - nBegin);
  }
  // -----------------------------------------------------------------------------------------------------------
  // third, break blocs
  replaceOccurences(sScript,
  { "{",     "}" },
  { "\n{\n", "\n}\n" });
  // fourth, control blocs braces match
  nPos = sScript.find('{'); // speed up start
  nBegin = nClose = 0;
  while(nPos != std::string::npos)
  {
    if(nPos == sScript.size())
    {
      if(nBegin != nClose) // compilation error : braces mismatch
      {
        eCode = SC_BRACES_MISMATCH;
        return false;
      }
      break;
    }
    if(sScript[nPos] == '{') nBegin++;
    else if(sScript[nPos] == '}') nClose++;
    nPos++;
    if(sScript.find('}', nPos) == std::string::npos) nPos = sScript.size(); // speed up end
  }
  // -----------------------------------------------------------------------------------------------------------
  // fifth, realign expressions () and []
  nPos = 0;
  while(1)
  {
    if(nPos == sScript.size()) break;
    nBegin = sScript.find('(', nPos);
    nClose = sScript.find('[', nPos);
    nPos = std::min(nBegin, nClose);
    if(nPos == std::string::npos) break;
    char op1 = nBegin < nClose ? '(' : '[';
    char op2 = nBegin < nClose ? ')' : ']';
    nClose = nPos + 1;
    nNum = 1;
    while(1)
    {
      if(nClose == sScript.size()) // compilation error : expression mismatch
      {
        if(op1 == '(') eCode = SC_PARENTHESIS_MISMATCH;
        else eCode = SC_BRACKETS_MISMATCH;
        return false;
      }
      if(sScript[nClose] == op1) nNum++;
      else if(sScript[nClose] == op2) nNum--;
      if(nNum == 0) // we reach the end of the expression
      {
        nPos = nClose;
        break;
      }
      if(sScript[nClose] == '\n')
        sScript.erase(nClose);
      else
        nClose++;
    }
    nPos++;
  }// now, all expressions () and [] are on a same line
  // -----------------------------------------------------------------------------------------------------------
  // sixth, the tricky one : separate expressions within expressions
  // ex :
  //   x = 4*PI / ( getAngle(getSunOrientation  ( 11.25))+ 3*getRand(50.0 * (    2+ 1)))
  // must become :
  //   val1 = getRand(50.0 * (    2+ 1))
  //   val2 = getSunOrientation  ( 11.25)
  //   val3 = getAngle(val2)
  //   val4 = val3 + 3*val1
  //   x = 4*PI / val4
  // result obtained :                      - ok, validated -
  //   val2 = getSunOrientation  ( 11.25)
  //   val4 =     2+ 1
  //   val3 = getRand(50.0 * val4)
  //   val1 =  getAngle(val2) + 3*val3
  //   x = 4*PI / val1
  nBegin = nPos = 0;
  size_t n, nOld = 0;
  size_t nVal = 0;
  while(nPos < sScript.size())
  {
    bool bDecompose = false;
    bool bExcludeParenthesis = false;
    size_t nStart = SIZE_MAX;
    nClose = sScript.find('\n', nPos);
    nBegin = sScript.find('(', nPos);
    if(nClose < nBegin)
    {
      nPos = nClose + 1;
    }
    else
    {
      n = nBegin - 1;
      while(sScript[n] == ' ' && n > nPos) n--;
      if(std::isalnum(sScript[n], std::locale("C")) || sScript[n] == '_') // function mark
      {
        nOld = nBegin;
        nBegin = sScript.find('(', nBegin + 1);
        if(nBegin > nClose) // that's fine
          nPos = nClose + 1;
        else
        {
          n = nBegin - 1;
          while(sScript[n] == ' ' && n > nOld) n--;
          if(std::isalnum(sScript[n], std::locale("C")) || sScript[n] == '_') // function mark (not tolerated)
          {
            while((std::isalnum(sScript[n], std::locale("C")) || sScript[n] == '_') && n > nOld) n--;
            // n holds now the start
            bDecompose = true;
            bExcludeParenthesis = false;
            nStart = n;
            n = nBegin + 1;
          }
          else // arithmetic (not tolerated)
          {
            bDecompose = true;
            bExcludeParenthesis = true;
            nStart = nBegin;
            n = nStart + 1;
          }
        }
      }
      else // arithmetic (not tolerated)
      {
        bDecompose = true;
        bExcludeParenthesis = true;
        nStart = nBegin;
        n = nStart + 1;
      }
      if(bDecompose)
      {
        nNum = 1;
        while(1)
        {
          if(sScript[n] == '(') nNum++;
          else if(sScript[n] == ')') nNum--;
          if(nNum == 0) // we reach the end of the expression
          {
            nVal++;
            std::string substr;
            if(bExcludeParenthesis)
            {
              substr = "val" + std::to_string(nVal) + " = " + sScript.substr(nStart + 1, n - nStart - 1) + "\n";
              sScript.erase(nStart, n - nStart + 1);
              sScript.insert(nStart, "val" + std::to_string(nVal));
            }
            else
            {
              substr = "val" + std::to_string(nVal) + " = " + sScript.substr(nStart + 1, n - nStart) + "\n"; // keep the parenthesis at the end
              sScript.erase(nStart + 1, n - nStart);
              sScript.insert(nStart + 1, "val" + std::to_string(nVal));
            }
            sScript.insert(nPos, substr);
            break;
          }
          n++;
        }
      }
    }
  } // now, each parenthesis has one and only one end parenthesis in the line
  // -----------------------------------------------------------------------------------------------------------
  // seventh, general formatting
  std::vector<std::string> chars = { "=", "(", ")", "[", "]", "-", "+", "%", "*", "!", ":", ";", ",", "?", "<", ">", "/", "&&", "|" };
  std::vector<std::string> reps = { " = ", " ( ", " ) ", " [ ", " ] ", " - ", " + ", " % ", " * ", " ! ", " : ", " ; ", " , ", " ? ", " < ", " > ", " / ", " && ", " | " };
  replaceOccurences(sScript, chars, reps);
  replaceOccurences(sScript, { " .", "=  =", "!  =", "+  +", "+  =", "-  -", "-  =", "<  =", ">  =", "|  |", "  ", "\n\n", " \n" }, { "0.", " == ", " != ", "++", " += ", "--", " -= ", " <= ", " >= ", " || ", " ", "\n", "\n" }, true);
  // -----------------------------------------------------------------------------------------------------------
  // heigth, replace constants (don't touch globals)
  nPos = 0;
  while(nPos < sScript.size())
  {
    nBegin = sScript.find('@', nPos);
    if(nBegin != std::string::npos)
    {
      nClose = sScript.find(' ', nBegin);
      std::string substr = sScript.substr(nBegin + 1, nClose - nBegin - 1);
      ObscurusDefineData* _d = (ObscurusDefineData*)gData->get(OBS::OBJ_DEFINE, substr);
      if(_d && !_d->global)
      {
        sScript.erase(nBegin, nClose - nBegin);
        sScript.insert(nBegin, _d->value);
      }
    }
    nPos = nBegin;
  }
  // -----------------------------------------------------------------------------------------------------------
  // final result : 
  //  val2 = getSunOrientation ( 11.25 )
  //  val4 = 2 + 1
  //  val3 = getRand ( 50.0 * val4 )
  //  val1 = getAngle ( val2 ) + 3 * val3
  //  x = 4 * PI / val1
  if(sScript[0] == '\n') sScript.erase(0, 1);
  if(sScript[sScript.size() - 1] == '\n') sScript.erase(sScript.size() - 1);
  return true;
}

// --- INTERPRETLINE
void Script::interpretLine()
{
  if(vTokens[nLine][1] == "(")
  {
    // only for now
    if(vTokens[nLine][0] == "DebugOutput") gLogics->_debugOutput(sScriptName);
    else if(vTokens[nLine][0] == "GrantCharacterToPlayer") gLogics->_grantCharacterToPlayer(std::atoll(vTokens[nLine][2].c_str()));
    else if(vTokens[nLine][0] == "ForcePlayerToIncarnate") gLogics->_forcePlayerToIncarnate(std::atoll(vTokens[nLine][2].c_str()));
  }
  else if(vTokens[nLine][1] == "=")
  {
    if(vTokens[nLine][2] == "CreateCharacter")
    {
      mVariables[vTokens[nLine][0]].type = ST_CHARACTER;
      mVariables[vTokens[nLine][0]].value = gLogics->_createCharacter(vTokens[nLine][4], vTokens[nLine][6], std::atoi(vTokens[nLine][8].c_str()), std::atoi(vTokens[nLine][10].c_str()));
    }
  }
  else
  {
    // syntax error
  }
  nLine++;
}

// --- FINISHEXECUTION
void Script::finishExecution()
{
  if(!bInternal)
    gLogics->addMsgToQueue(new OBS::LogicsMsgScriptStatus(sScriptName, eCode, bThreaded), OBS::DEST_LOGICS);
  // if internal, the parent is waiting the child to join
}

/*
// we need a character
_loadWorld("NullWorld");
//_createMarker("NullMarker", 0, 0, 0, 0)
_loadMarker("NullMarker");
_createCharacterAtMarker("NullCharacter", "NullMarker");
_assignCharacterToPlayer("NullCharacter");
_forcePlayerToIncarnate("NullCharacter");
// dummy script :
// _char = createCharacter("name")
// grantCharacterToPlayer(_char)
// _char.strength += 8
// _char.name = "dummy"
// setPos(_char.name, 0, 0)
// set Addr("name", 13245678936)
*/
