#ifndef _OBS_HEADER_H_
#define _OBS_HEADER_H_
#include "anything.h"
#include "position.h"
#include "address.h"
#include "entity.h"
#include "item.h"
#include "character.h"
#include "script.h"
#include "world.h"
#include "cell.h"
#include "user.h"
#include "marker.h"
#endif