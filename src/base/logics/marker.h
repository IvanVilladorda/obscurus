#ifndef _OBS_MARKER_H_
#define _OBS_MARKER_H_
#include "entity.h"
namespace OBS
{
  struct Marker : Entity
  {
    Marker(std::unordered_map<std::string, std::string>& _map) : Entity(_map)
    {
    }
    Marker(const std::string& _world, int _x, int _y, double _z = 0.0)
      : Entity(_world, _x, _y, _z) { }

    void copyFromData(OBS::Object* _obj) {}
  };
}
#endif