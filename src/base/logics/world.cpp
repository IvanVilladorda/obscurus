#include "world.h"

namespace OBS
{
  World::World() { bitmap = 0; }
  World::~World() { if(bitmap) FreeImage_Unload(bitmap); }

  void World::copyFromData(OBS::Object* _obj)
  {
    ObscurusWorldData* _world = (ObscurusWorldData*)_obj;
    seaLevel = _world->sea_level;
    origin[0] = _world->origin[0];
    origin[1] = _world->origin[1];
    mapFile = _world->world_map;
    elevationMult = _world->elevation_factor;
  }

  void World::openWorldMap()
  {
    gLog("Opening world map %s.", 'I', mapFile.c_str());
    mapWidth = mapHeight = 0;
    FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
    fif = FreeImage_GetFileType(mapFile.c_str(), 0);
    if(fif == FIF_UNKNOWN)  fif = FreeImage_GetFIFFromFilename(mapFile.c_str());
    if((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif))
    {
      bitmap = FreeImage_Load(fif, mapFile.c_str());
      mapWidth = FreeImage_GetWidth(bitmap);
      mapHeight = FreeImage_GetHeight(bitmap);
      RGBQUAD rgb;
      char sColour[7];
      // first, fill pixel map
      for(short y = 0; y < mapHeight; y++)
      {
        for(short x = 0; x < mapWidth; x++)
        {
          FreeImage_GetPixelColor(bitmap, x, y, &rgb);
          sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
          try  {
            pixelMap.at(sColour);
          }
          catch(...) {
            pixelMap[sColour] = gData->getTerrainByColor(rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
          }
        }
      }
      // populate
      int C, NE, N, NW, S, SW, SE, E, W, nonEmpty = 0;
      ObscurusTerrainData *pC;
      short edgeRight = mapWidth - 1;
      short edgeTop = mapHeight -1;
      size_t memCount = 0;
      chunkMap.reserve(mapWidth);
      for(short x = 0; x < mapWidth; x++)
      {
        chunkMap.push_back(std::vector<ChunkElevation>());
        chunkMap[x].reserve(mapHeight);
        for(short y = 0; y < mapHeight; y++)
        {
          // current :
          FreeImage_GetPixelColor(bitmap, x, y, &rgb);
          sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
          pC = pixelMap[sColour];
          C = pC->elevation;
          // neighbors :
          NE = N = NW = S = SW = SE = E = W = -1;
          if(x == 0) SW = W = NW = 0; else if(x == edgeRight) NE = E = SE = 0;
          if(y == 0) SW = S = SE = 0; else if(y == edgeTop) NW = N = NE = 0;
          if(NW == -1)
          {
            FreeImage_GetPixelColor(bitmap, x - 1, y + 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            NW = pixelMap[sColour]->elevation;
          }
          if(N == -1)
          {
            FreeImage_GetPixelColor(bitmap, x, y + 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            N = pixelMap[sColour]->elevation;
          }
          if(NE == -1)
          {
            FreeImage_GetPixelColor(bitmap, x + 1, y + 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            NE = pixelMap[sColour]->elevation;
          }
          if(W == -1)
          {
            FreeImage_GetPixelColor(bitmap, x - 1, y, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            W = pixelMap[sColour]->elevation;
          }
          if(E == -1)
          {
            FreeImage_GetPixelColor(bitmap, x + 1, y, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            E = pixelMap[sColour]->elevation;
          }
          if(SW == -1)
          {
            FreeImage_GetPixelColor(bitmap, x - 1, y - 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            SW = pixelMap[sColour]->elevation;
          }
          if(S == -1)
          {
            FreeImage_GetPixelColor(bitmap, x, y - 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            S = pixelMap[sColour]->elevation;
          }
          if(SE == -1)
          {
            FreeImage_GetPixelColor(bitmap, x + 1, y - 1, &rgb);
            sprintf(sColour, "%.2X%.2X%.2X", rgb.rgbRed, rgb.rgbGreen, rgb.rgbBlue);
            SE = pixelMap[sColour]->elevation;
          }
          if(C + NE + N + NW + S + SW + SE + E + W == 0)
            chunkMap[x].push_back(ChunkElevation());
          else
          {
            chunkMap[x].push_back(ChunkElevation(nonEmpty++));
            if(pC->slope == 0.0f)
            {
              chunkMap[x][y].pt[1][3] = (C + NW) * 0.5f;
              chunkMap[x][y].pt[2][3] = (C + N) * 0.5f;
              chunkMap[x][y].pt[3][3] = (C + NE) * 0.5f;
              chunkMap[x][y].pt[1][2] = (C + W) * 0.5f;
              chunkMap[x][y].pt[2][2] = C;
              chunkMap[x][y].pt[3][2] = (C + E) * 0.5f;
              chunkMap[x][y].pt[1][1] = (C + SW) * 0.5f;
              chunkMap[x][y].pt[2][1] = (C + S) * 0.5f;
              chunkMap[x][y].pt[3][1] = (C + SE) * 0.5f;
            }
            else
            {
              // I = (C + O) / 2 + C-slope * (C - (3 * C + O) / 4)
              // all elevations below are 8bits based, they need to be multiplied by world's elevation_factor
              chunkMap[x][y].pt[1][3] = (C + NW) * 0.5f + pC->slope * (C - (3 * C + NW) * 0.25f);
              chunkMap[x][y].pt[2][3] = (C + N) * 0.5f + pC->slope * (C - (3 * C + N) * 0.25f);
              chunkMap[x][y].pt[3][3] = (C + NE) * 0.5f + pC->slope * (C - (3 * C + NE) * 0.25f);
              chunkMap[x][y].pt[1][2] = (C + W) * 0.5f + pC->slope * (C - (3 * C + W) * 0.25f);
              chunkMap[x][y].pt[2][2] = C;
              chunkMap[x][y].pt[3][2] = (C + E) * 0.5f + pC->slope * (C - (3 * C + E) * 0.25f);
              chunkMap[x][y].pt[1][1] = (C + SW) * 0.5f + pC->slope * (C - (3 * C + SW) * 0.25f);
              chunkMap[x][y].pt[2][1] = (C + S) * 0.5f + pC->slope * (C - (3 * C + S) * 0.25f);
              chunkMap[x][y].pt[3][1] = (C + SE) * 0.5f + pC->slope * (C - (3 * C + SE) * 0.25f);
            }
            if(x == 0 && y == 0)                         chunkMap[x][y].pt[0][0] = chunkMap[x][y].pt[1][1] * 0.25f;
            else if(x == 0 && chunkMap[x][y - 1].empty)  chunkMap[x][y].pt[0][0] = chunkMap[x][y].pt[1][1] * 0.25f;
            else if(x == 0)                              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x][y - 1].pt[1][3]) * 0.25f;
            else if(y == 0 && chunkMap[x - 1][y].empty)  chunkMap[x][y].pt[0][0] = chunkMap[x][y].pt[1][1] * 0.25f;
            else if(y == 0)                              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1]) * 0.25f;
            else if(chunkMap[x][y - 1].empty && chunkMap[x - 1][y].empty && chunkMap[x - 1][y - 1].empty)
              chunkMap[x][y].pt[0][0] = chunkMap[x][y].pt[1][1] * 0.25f;
            else if(chunkMap[x][y - 1].empty && chunkMap[x - 1][y].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] +  chunkMap[x - 1][y - 1].pt[3][3]) * 0.25f;
            else if(chunkMap[x - 1][y - 1].empty && chunkMap[x - 1][y].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x][y - 1].pt[1][3]) * 0.25f;
            else if(chunkMap[x - 1][y - 1].empty && chunkMap[x][y - 1].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1]) * 0.25f;
            else if(chunkMap[x - 1][y - 1].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1] + chunkMap[x][y - 1].pt[1][3]) * 0.25f;
            else if(chunkMap[x][y - 1].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1] + chunkMap[x - 1][y - 1].pt[3][3]) * 0.25f;
            else if(chunkMap[x - 1][y].empty)
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x][y - 1].pt[1][3] + chunkMap[x - 1][y - 1].pt[3][3]) * 0.25f;
            else
              chunkMap[x][y].pt[0][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1] + chunkMap[x][y - 1].pt[1][3] + chunkMap[x - 1][y - 1].pt[3][3]) * 0.25f;
            if((x > 0) && (!chunkMap[x - 1][y].empty))
            {
              chunkMap[x][y].pt[0][1] = (chunkMap[x][y].pt[1][1] + chunkMap[x - 1][y].pt[3][1]) * 0.5f;
              chunkMap[x][y].pt[0][2] = (chunkMap[x][y].pt[1][2] + chunkMap[x - 1][y].pt[3][2]) * 0.5f;
              chunkMap[x][y].pt[0][3] = (chunkMap[x][y].pt[1][3] + chunkMap[x - 1][y].pt[3][3]) * 0.5f;
            }
            else
            {
              chunkMap[x][y].pt[0][1] = chunkMap[x][y].pt[1][1] * 0.5f;
              chunkMap[x][y].pt[0][2] = chunkMap[x][y].pt[1][2] * 0.5f;
              chunkMap[x][y].pt[0][3] = chunkMap[x][y].pt[1][3] * 0.5f;
            }
            if((y > 0) && (!chunkMap[x][y - 1].empty))
            {
              chunkMap[x][y].pt[1][0] = (chunkMap[x][y].pt[1][1] + chunkMap[x][y - 1].pt[1][3]) * 0.5f;
              chunkMap[x][y].pt[2][0] = (chunkMap[x][y].pt[2][1] + chunkMap[x][y - 1].pt[2][3]) * 0.5f;
              chunkMap[x][y].pt[3][0] = (chunkMap[x][y].pt[3][1] + chunkMap[x][y - 1].pt[3][3]) * 0.5f;
            }
            else
            {
              chunkMap[x][y].pt[1][0] = chunkMap[x][y].pt[1][1] * 0.5f;
              chunkMap[x][y].pt[2][0] = chunkMap[x][y].pt[2][1] * 0.5f;
              chunkMap[x][y].pt[3][0] = chunkMap[x][y].pt[3][1] * 0.5f;
            }
          }
          // update memory consumption
          memCount += sizeof(chunkMap[x][y]);
          if(!chunkMap[x][y].empty)
            memCount += sizeof(chunkMap[x][y].pt.capacity() * chunkMap[x][y].pt[0].capacity());
        }
      }
      gLog("Map ready (%i bytes).", 'I', memCount);
    }
  }

  void World::exportWorldMap(const char* sFilename)
  {
    if(!bitmap) openWorldMap();
    if(!bitmap) return;
    gLog("Exporting world map %s.", 'I', mapFile.c_str());
    size_t width = mapWidth * 4;
    size_t height = mapHeight * 4;
    FIBITMAP* image = FreeImage_Allocate(width, height, 24);
    RGBQUAD color;
    // WNW  0   1   2 
    // W    3   C   4 
    // WSW  5   6   7 
    // SW  SSW  S  SSE
    for(int x = 0; x < mapWidth; x++)
    {
      for(int y = 0; y < mapHeight; y++)
      {
        // SW
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[0][0];
        FreeImage_SetPixelColor(image, x * 4, y * 4, &color);
        // SSW
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[1][0];
        FreeImage_SetPixelColor(image, x * 4 + 1, y * 4, &color);
        // S
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[2][0];
        FreeImage_SetPixelColor(image, x * 4 + 2, y * 4, &color);
        // SSE
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[3][0];
        FreeImage_SetPixelColor(image, x * 4 + 3, y * 4, &color);
        // WSW
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[0][1];
        FreeImage_SetPixelColor(image, x * 4, y * 4 + 1, &color);
        // 5
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[1][1];
        FreeImage_SetPixelColor(image, x * 4 + 1, y * 4 + 1, &color);
        // 6
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[2][1];
        FreeImage_SetPixelColor(image, x * 4 + 2, y * 4 + 1, &color);
        // 7
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[3][1];
        FreeImage_SetPixelColor(image, x * 4 + 3, y * 4 + 1, &color);
        // W
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[0][2];
        FreeImage_SetPixelColor(image, x * 4, y * 4 + 2, &color);
        // 3
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[1][2];
        FreeImage_SetPixelColor(image, x * 4 + 1, y * 4 + 2, &color);
        // C
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[2][2];
        FreeImage_SetPixelColor(image, x * 4 + 2, y * 4 + 2, &color);
        // 4
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[3][2];
        FreeImage_SetPixelColor(image, x * 4 + 3, y * 4 + 2, &color);
        // WNW
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[0][3];
        FreeImage_SetPixelColor(image, x * 4, y * 4 + 3, &color);
        // 0
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[1][3];
        FreeImage_SetPixelColor(image, x * 4 + 1, y * 4 + 3, &color);
        // 1
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[2][3];
        FreeImage_SetPixelColor(image, x * 4 + 2, y * 4 + 3, &color);
        // 2
        if(chunkMap[x][y].empty) color.rgbRed = color.rgbGreen = color.rgbBlue = 0.0f;
        else color.rgbRed = color.rgbGreen = color.rgbBlue = chunkMap[x][y].pt[3][3];
        FreeImage_SetPixelColor(image, x * 4 + 3, y * 4 + 3, &color);
      }
    }
    FreeImage_Save(FIF_PNG, image, sFilename, 0);
    gLog("Exported as %s.", 'I', sFilename);
  }

  ObscurusTerrainData* World::getTerrain(short _x, short _y, bool _origin) // _x and _y are grid cell coords
  {
    int x = _x;
    int y = _y;
    if(_origin) // if !_origin = already in image space system
    {
      // translate to image xy system :
      x += origin[0]; // _x+ is east
      y += origin[1]; // _y+ is north
    }
    if(x < 0) return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // out of left bound
    if(y < 0) return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // out of lower bound
    // open file and check pixel color
    if(!bitmap) openWorldMap();
    if(!bitmap) return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // no map
    if(x > mapWidth) return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // out of right bound
    if(y > mapHeight) return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // out of upper bound
    RGBQUAD pixel;
    FreeImage_GetPixelColor(bitmap, x, y, &pixel);
    //ObscurusTerrainData* data = gData->getTerrainByColor(pixel.rgbRed, pixel.rgbBlue, pixel.rgbGreen);
    char sColour[7];
    sprintf(sColour, "%.2X%.2X%.2X", pixel.rgbRed, pixel.rgbGreen, pixel.rgbBlue);
    try  {
      return pixelMap.at(sColour);
    }
    catch(...) {
    }
    return (ObscurusTerrainData*)gData->selectNullObject(OBS::OBJ_TERRAIN); // unknown
  }
} // - namespace
