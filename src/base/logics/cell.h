#ifndef _OBS_CELL_H_
#define _OBS_CELL_H_
#include <thread>
#include "world.h"

namespace OBS
{
  enum cardinal
  {
    DIR_NW,
    DIR_N,
    DIR_NE,
    DIR_W,
    DIR_E,
    DIR_SW,
    DIR_S,
    DIR_SE,
    DIR_LASTAZIMUT
  };
  enum cellState
  {
    CL_UNBUILT,
    CL_BUILDING,
    CL_BUILT,
    CL_EDGING,
    CL_DONE
  };
  enum cellTube
  {
    CL_OUT,
    CL_UNWRAPPED,
    CL_WRAPPED,
    CL_READY
  };

  struct Cell
  {
    Cell(World*, short, short, ObscurusTerrainData*);

    ObscurusTerrainData* terrain;
    World* world;
    Cell* neighbors[8];
    std::thread thread;
    cellState state;
    cellTube tube;
    unsigned short rMap[256][256]; // represent grid squares
    float hMap[257][257]; // represent grid edges
    float timeOut; // activity thresold
    short coordX, coordY; // coordinates on map toward the world's origin

    void buildCell();
  };
}
#endif