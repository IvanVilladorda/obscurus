#ifndef _OBS_CHARACTER_H_
#define _OBS_CHARACTER_H_
#include "entity.h"
#include "messages.h"
#include "data/data.character.h"
namespace OBS
{
  struct Character : Entity
  {
    Character(std::unordered_map<std::string, std::string>& _map) : Entity(_map)
    {
      name = _map["chName"];
      sex = (OBS::Sex)std::atoi(_map["chSex"].c_str());
      user = _map["chUserName"];
    }
    Character(const std::string& _world, int _x, int _y, double _z = 0.0)
      : Entity(_world, _x, _y, _z) { }

    void copyFromData(OBS::Object* _obj)
    {
      Entity::copyFromData(_obj); // call ancestor
      ObscurusCharacterData* data = (ObscurusCharacterData*)_obj;
      name = data->name;
      sex = data->sex;
      user = data->user;
    }

    std::string name;
    OBS::Sex sex;
    std::string user;
  };

  // MESSAGES
  struct LogicsMsgAssignCharacterToPlayer : LogicsMsg
  {
    LogicsMsgAssignCharacterToPlayer(long long _id) { eId = MSG_ASSIGN_CHARACTER; character = _id; }
    long long character;
  };
  struct LogicsMsgEmbodyCharacter : LogicsMsg
  {
    LogicsMsgEmbodyCharacter(long long _id) { eId = MSG_EMBODY_CHARACTER; character = _id; }
    long long character;
  };
}
#endif