#ifndef _OBS_POSITION_H_
#define _OBS_POSITION_H_
#include "world.h"
#include "cell.h"
#include "address.h"

namespace OBS
{
  struct Position
  {
    Position() { x = y = z = 0; o = 0; pWorld = 0; pCell = 0; }
    int x, y, z;
    unsigned char o;
    World* pWorld;
    Cell* pCell;
    Address address;
  };
}
#endif