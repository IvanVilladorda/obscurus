#ifndef _OBS_USER_H_
#define _OBS_USER_H_

namespace OBS
{
  struct User
  {
    User() {}
    std::string name;
    std::string ip;
  };
}
#endif