#ifndef _OBS_WORLD_H_
#define _OBS_WORLD_H_
#include "anything.h"
#include "data.h"
#include <FreeImage.h>
#include <map>
#include "log.h"

namespace OBS
{
  // y3 WNW  0  1  2
  // y2  W   3     4
  // y1 WSW  5  6  7
  // y0  SW SSW S SSE
  //    x0  x1  x2 x3
  struct ChunkElevation
  {
    std::vector<std::vector<float>> pt;
    int offset;
    bool empty;

    ChunkElevation(int _offset = -1)
    {
      offset = _offset;
      empty = true;
      if(offset > -1)
      {
        empty = false;
        pt.reserve(4);
        pt.push_back({ 0.0f, 0.0f, 0.0f, 0.0f });
        pt.push_back({ 0.0f, 0.0f, 0.0f, 0.0f });
        pt.push_back({ 0.0f, 0.0f, 0.0f, 0.0f });
        pt.push_back({ 0.0f, 0.0f, 0.0f, 0.0f });
      }
    }
  };

  struct World: public Anything
  {
    World();
    ~World();
    FIBITMAP *bitmap;
    std::string mapFile;
    size_t mapWidth, mapHeight;
    unsigned char seaLevel, elevationMult;
    short origin[2];
    std::map<std::string, ObscurusTerrainData*> pixelMap;
    std::vector<std::vector<ChunkElevation> > chunkMap;

    void copyFromData(OBS::Object*);
    void openWorldMap();
    void exportWorldMap(const char*);
    ObscurusTerrainData* getTerrain(short _x, short _y, bool _origin = true);
  };
}
#endif