#ifndef _OBS_ENTITY_H_
#define _OBS_ENTITY_H_
#include "anything.h"
#include <unordered_map>

namespace OBS
{
  static long long uniqueId = 0; // only valid for entities
  struct Entity : public Anything
  {
    Entity(std::unordered_map<std::string, std::string>& _map) // from save (fill fields with db record)
    {
      id = std::atoll(_map["enId"].c_str());
      typeName = _map["enTypeName"];
      type = DataType(typeName.c_str());
      dataName = _map["enDataName"];
      worldName = _map["enWorldName"];
      gridX = std::atoi(_map["enGridX"].c_str());
      gridY = std::atoi(_map["enGridY"].c_str());
      posX = std::atof(_map["enPosX"].c_str()); 
      posY = std::atof(_map["enPosY"].c_str());
      posZ = std::atof(_map["enPosZ"].c_str());
      yaw = (float)std::atof(_map["enYaw"].c_str());
    }
    Entity(const std::string& _world, int _x, int _y, double _z = 0.0) // new one
    {
      id = ++uniqueId; // reserve 0
      worldName = _world;
      gridX = _x; posX = _x;
      gridY = _y; posY = _y;
      posZ = _z;
    }

    // use Entity::copyFromData or fill the function entirely
    void copyFromData(Object* _obj)
    {
      type = _obj->type;
      typeName = DataTypeNames(type);
      dataName = _obj->data_name;
      // data provides no localisation
    }
    // loc function
    void setPos(int _x, int _y, double _z = 0.0)
    {
      gridX = _x; posX = _x;
      gridY = _y; posY = _y;
      posZ = _z;
    }
    void setPos(double _x, double _y, double _z = 0.0)
    {
      setPos((int)_x, (int)_y);
      posX = _x; posY = _y, posZ = _z;
    }
    static ObjectType DataType(const char* _objType) // all types of object type names
    {
      if(!strcmp(_objType, "ENTITY")) return OBJ_ENTITY;
      else if(!strcmp(_objType, "DEFINE")) return OBJ_DEFINE;
      else if(!strcmp(_objType, "DECLARE")) return OBJ_DECLARE;
      else if(!strcmp(_objType, "EPOCH")) return OBJ_EPOCH;
      else if(!strcmp(_objType, "WORLD")) return OBJ_WORLD;
      else if(!strcmp(_objType, "SCRIPT")) return OBJ_SCRIPT;
      else if(!strcmp(_objType, "CHARACTER")) return OBJ_CHARACTER;
      else if(!strcmp(_objType, "MARKER")) return OBJ_MARKER;
      else if(!strcmp(_objType, "LANDMARK")) return OBJ_LANDMARK;
      else if(!strcmp(_objType, "CULTURE")) return OBJ_CULTURE;
      else if(!strcmp(_objType, "LANGUAGE")) return OBJ_LANGUAGE;
      else if(!strcmp(_objType, "TERRAIN")) return OBJ_TERRAIN;
      else if(!strcmp(_objType, "SOIL")) return OBJ_SOIL;
      else if(!strcmp(_objType, "FORMULA")) return OBJ_FORMULA;
      else if(!strcmp(_objType, "REGION")) return OBJ_WORLDREGION;
      else if(!strcmp(_objType, "REGIONALTERRAIN")) return OBJ_REGIONALTERRAIN;
      return OBJ_UNKNOWN;
    }

    long long id; // auto increment
    OBS::ObjectType type;
    std::string typeName;
    std::string dataName;
    std::string worldName;
    int gridX, gridY; // 2d positionning (1 meter step)
    double posX, posY, posZ; // 3d positionning
    float yaw;
  };
}
#endif