#ifndef _OBS_DATAPARSER_H_
#define _OBS_DATAPARSER_H_
#include <zzip\lib.h>
#include <string>
#include <vector>

struct ObscurusDataParser
{
  ObscurusDataParser(ZZIP_FILE*, const std::string&, const std::string&, const std::string&);
  bool getCommand();

  std::string sCommand;
  std::string sFile;
  std::string sModule;
  std::string sPath;
  std::vector<std::string> vParameters;
  size_t nLine;
  ZZIP_FILE* pFile;
};

#endif
