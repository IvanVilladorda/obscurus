// -- LOGGER HEADER
#ifndef _OBS_LOG_H_
#define _OBS_LOG_H_

#include "singleton.h"
#include <string>
#include <vector>

class ObscurusLogListener;
class ObscurusLog : public Singleton<ObscurusLog>
{
public:
  ObscurusLog(const std::string&);
  ~ObscurusLog();
  static ObscurusLog* ObscurusLog::getSingleton();

  void log(const std::string&, char, ...);
  void log(char*, char, ...);
  void setDebugOutput(bool);
  void addListener(ObscurusLogListener*);
  void removeListener(ObscurusLogListener*);

private:
  void init();
  void prepare(char _lvl = 'I');
  void write();

  std::vector<ObscurusLogListener*> vListeners;
  char vBuf[32];
  char vBufferD[16];
  char vBufferT[16];
  char vLvl[8];
  char vPath[64];
  char vMsg[2048];
  char vBuffer[2048];
  bool bReady;
  char bDebug;
};
#define gLog(...)  ObscurusLog::getSingleton()->log(__VA_ARGS__)
#define gLogger    ObscurusLog::getSingleton()
class ObscurusLogListener
{
public:
  ObscurusLogListener() {}
  ~ObscurusLogListener() {}
  virtual void log(const char*, const char*) = 0;
};
#endif