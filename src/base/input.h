#ifndef _OBS_INPUT_H_
#define _OBS_INPUT_H_

#include "preferences.h"
#include "log.h"

#include "singleton.h"
#include <OIS.h>

namespace OBS
{
  enum Shift { OBSI_NONE = 0, OBSI_SHIFT = 1, OBSI_CTRL = 2, OBSI_SHIFT_CTRL = 3, OBSI_ALT = 4, OBSI_SHIFT_ALT = 5, OBSI_CTRL_ALT = 6, OBSI_SHIT_CTRL_ALT = 7 };
  enum MouseButton { OBSI_MOUSE_NONE, OBSI_MOUSE_RIGHT, OBSI_MOUSE_LEFT, OBSI_MOUSE_MIDDLE, OBSI_MOUSE_DELTAUP, OBSI_MOUSE_DELTADOWN };
  struct Action // describe an unnamed action, either by mouse click or key press (mouse move is separate)
  {
    Shift eShift; // with shift,alt,ctrl condition
    MouseButton eMouse;
    OIS::KeyCode key;
    Action() { key = OIS::KC_UNASSIGNED; eShift = OBSI_NONE; eMouse = OBSI_MOUSE_NONE; }
  };

  enum MouseMode { OBSI_MENU, OBSI_VIEW }; // used to update mouse position values (menu is absolute, view is relative)
}

class ObscurusInputListener
{
public:
  ObscurusInputListener() {}
  ~ObscurusInputListener() {}
  virtual void mouseMoved(const OIS::MouseEvent &arg) = 0;
  virtual void mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;
  virtual void mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;
  virtual void keyPressed(const OIS::KeyEvent &arg) = 0;
  virtual void keyReleased(const OIS::KeyEvent &arg) = 0;
};

class ObscurusInput : public Singleton<ObscurusInput>, public OIS::MouseListener, public OIS::KeyListener
{
public:
  ObscurusInput();
  ~ObscurusInput();
  static ObscurusInput* ObscurusInput::getSingleton();

  void addListener(ObscurusInputListener*);
  void capture();
  bool getCtrl(std::string const& _action);
  void setCtrl(std::string const& _action, std::string const& _control);
  void setMouseMode(OBS::MouseMode);

private:
  void init();
  bool mouseMoved(const OIS::MouseEvent &arg);
  bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
  bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
  bool keyPressed(const OIS::KeyEvent &arg);
  bool keyReleased(const OIS::KeyEvent &arg);

  OIS::InputManager* oisInputManager;
  OIS::Keyboard* oisKeyboard;
  OIS::Mouse* oisMouse;

  bool keys[256]; // example : keys[actionTable["my action"].key] ? true : false
  bool mouseb[8];
  struct
  {
    int absx, absy;
    int oldx, oldy;
    int relx, rely;
    int delta;
  } oMouse;
  OBS::MouseMode eMouseMode;
  std::map<std::string, OBS::Action> mActionTable;
  std::vector<ObscurusInputListener*> vListeners;
};

#define gInput    ObscurusInput::getSingleton()

#endif