// -- DB (CLIENT/LOCAL)
#ifndef _OBS_DB_H_ // also prevent server/client dbsys mix
#define _OBS_DB_H_

#include "singleton.h"
#include "sqlite/sqlite3.h"
#include "logics/header.h"
#include <vector>
#include <unordered_map>
#include <memory>
#include <mutex>

struct ObscurusSave
{
  std::string sName;
  char* rgbImage;
};
struct ObscurusRows // basic mapping string-to-string (same as PHP)
{
  ObscurusRows() { numRows = 0; }
  int callback(void* _param, int argc, char **argv, char **azColName)
  {
    std::unordered_map<std::string, std::string> vCols;
    for(int i = 0; i < argc; i++)
      vCols[azColName[i]] = argv[i];
    rows.push_back(vCols);
    numRows++;
    return 0;
  }
  int numRows;
  std::vector<std::unordered_map<std::string, std::string>> rows;
};

class ObscurusDB : public Singleton<ObscurusDB>
{
public:
  ObscurusDB();
  ~ObscurusDB();
  static ObscurusDB* ObscurusDB::getSingleton();

  void listSaves();
  void open(ObscurusSave);
  std::string getMasterMod();
  std::unique_ptr<ObscurusRows> exec(const std::string&);
  bool record();
  int loadOrSaveDb(sqlite3*, const char*, int);

  // DB ops :
  void insert(OBS::Entity*);
  void flush(OBS::Entity*);
  void remove(OBS::Entity*);
  void insert(OBS::Character*);
  void flush(OBS::Character*);
  void remove(OBS::Character*);

  std::vector<ObscurusSave> vSavegames;
  std::mutex mutex; // global mutex on db connection
  bool bOperational;

private:
  void init();
  char* pErr;
  sqlite3* db;
  std::map<OBS::ObjectType, sqlite3_stmt*> inserts;
  std::map<OBS::ObjectType, sqlite3_stmt*> updates;
  std::map<OBS::ObjectType, sqlite3_stmt*> deletes;
};

#define gDB    ObscurusDB::getSingleton()
#define LOCK_DB  std::lock_guard<std::mutex> lock_database(gDB->mutex)

#endif