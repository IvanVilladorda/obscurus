#ifndef _OBS_WORLDDATA_H_
#define _OBS_WORLDDATA_H_
#include "data/data.object.h"
#include "data/data.epoch.h"

class ObscurusWorldData : public OBS::Object
{
public:
  std::string         world_map;
  short               origin[2];
  unsigned char       sea_level;
  unsigned char       elevation_factor;
  std::string         epoch;
  ObscurusEpochData*  pEpoch;

  ObscurusWorldData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_WORLD) : Object(_name, _type) //--- default values ---
  {
    origin[0] = origin[1] = 0;
    sea_level = 0;
    elevation_factor = 1;
    pEpoch = 0;
    unique = true;
  }

  bool link()
  {
    if(elevation_factor == 0)
      elevation_factor = 1;
    if(world_map.empty() || !fs::exists(world_map))
    {
      gLog("World map %s for %s object \"%s\" does not exists.", 'E', world_map.c_str(), DataTypeNames(type), data_name.c_str());
      world_map.clear();
    }
    pEpoch = static_cast<ObscurusEpochData*>(getObjectIndexByName(OBS::OBJ_EPOCH, epoch));
    if(!pEpoch)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "world_map")              world_map = _parser->sPath + "/" + _parser->sModule + "/" + _parser->vParameters[0];
    else if(_parser->sCommand == "sea_level")         sea_level = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "elevation_factor")  elevation_factor = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "epoch")             epoch = _parser->vParameters[0];
    else if(_parser->sCommand == "origin")
    {
      origin[0] = (short)interpretULong(_parser->vParameters[0]);
      if(_parser->vParameters.size() > 1)
        origin[1] = (short)interpretULong(_parser->vParameters[1]);
      else
        origin[1] = origin[0];
    }
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};
#endif