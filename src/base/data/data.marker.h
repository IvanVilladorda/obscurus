#ifndef _OBS_MARKERDATA_H_
#define _OBS_MARKERDATA_H_
#include "data/data.object.h"
#include "data/data.world.h"

class ObscurusMarkerData : public OBS::Object
{
public:
  std::string  world_name;
  std::string  mesh_file;
  ObscurusWorldData* pWorld;
  double  x;
  double  y;
  double  z;
  float   o;
  bool    visible;

  ObscurusMarkerData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_MARKER) : Object(_name, _type) //--- default values ---
  {
    x = y = z = 0.0;
    o = 0.0f;
    pWorld = 0;
    visible = false;
  }

  bool link()
  {
    pWorld = static_cast<ObscurusWorldData*>(getObjectIndexByName(OBS::OBJ_WORLD, world_name));
    if(!pWorld)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    if(!mesh_file.empty() && !fs::exists(mesh_file))
    {
      gLog("Mesh file %s for %s object \"%s\" does not exists.", 'E', mesh_file.c_str(), DataTypeNames(type), data_name.c_str());
      mesh_file.clear();
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "world_name")      world_name = _parser->vParameters[0];
    else if(_parser->sCommand == "mesh_file")  mesh_file = _parser->sPath + "/" + _parser->sModule + "/" + _parser->vParameters[0];
    else if(_parser->sCommand == "x")          x = interpretDouble(_parser->vParameters[0]);
    else if(_parser->sCommand == "y")          y = interpretDouble(_parser->vParameters[0]);
    else if(_parser->sCommand == "z")          z = interpretDouble(_parser->vParameters[0]);
    else if(_parser->sCommand == "o")          o = interpretFloat(_parser->vParameters[0]);
    else if(_parser->sCommand == "visible")    visible = interpretBoolean(_parser->vParameters[0]);
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif