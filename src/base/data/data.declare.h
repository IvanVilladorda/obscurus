#ifndef _OBS_DECLAREDATA_H_
#define _OBS_DECLAREDATA_H_
#include "data/data.object.h"
#include "data/data.script.h"

class ObscurusDeclareData : public OBS::Object
{
public:
  std::string          script_name;
  ObscurusScriptData*  pScript;

  ObscurusDeclareData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_DECLARE) : Object(_name, _type) //--- default values ---
  {
    pScript = 0;
  }

  bool link()
  {
    pScript = static_cast<ObscurusScriptData*>(getObjectIndexByName(OBS::OBJ_SCRIPT, script_name));
    if(!pScript)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->vParameters.size() > 1)  script_name = _parser->vParameters[1];
    else                                 gLog("%s named %s must have a script in parameter.", 'E', _parser->sCommand.c_str(), data_name.c_str());
  }
};

#endif