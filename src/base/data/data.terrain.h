#ifndef _OBS_TERRAINDATA_H_
#define _OBS_TERRAINDATA_H_
#include "data/data.object.h"
#include "data/data.formula.h"
#include "data/data.soil.h"

class ObscurusTerrainData : public OBS::Object
{
public:
  std::string           formula;
  ObscurusFormulaData*  pFormula;
  std::string           soil0;
  std::string           soil1;
  ObscurusSoilData*     pSoil0;
  ObscurusSoilData*     pSoil1;
  float                 roughness;
  float                 slope;
  unsigned char         elevation;
  unsigned char         color[3];

  ObscurusTerrainData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_TERRAIN) : Object(_name, _type) //--- default values ---
  {
    pFormula = 0;
    pSoil0 = 0;
    pSoil1 = 0;
    roughness = 1.0f;
    slope = 1.0f;
    elevation = 0;
    color[0] = color[1] = color[2] = 0;
  }

  bool link()
  {
    if(!formula.empty())
    {
      pFormula = static_cast<ObscurusFormulaData*>(getObjectIndexByName(OBS::OBJ_FORMULA, formula));
      if(!pFormula)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(!soil0.empty())
    {
      pSoil0 = static_cast<ObscurusSoilData*>(getObjectIndexByName(OBS::OBJ_SOIL, soil0));
      if(!pSoil0)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(!soil1.empty())
    {
      pSoil1 = static_cast<ObscurusSoilData*>(getObjectIndexByName(OBS::OBJ_SOIL, soil1));
      if(!pSoil1)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(roughness < 1.0f)  roughness = 1.0f;
    if(roughness > 64.0f)  roughness = 64.0f;
    if(slope < -2.0f)      slope = -2.0f;
    if(slope >  2.0f)      slope =  2.0f;
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "formula")         formula = _parser->vParameters[0];
    else if(_parser->sCommand == "soil0")      soil0 = _parser->vParameters[0];
    else if(_parser->sCommand == "soil1")      soil1 = _parser->vParameters[0];
    else if(_parser->sCommand == "roughness")  roughness = interpretFloat(_parser->vParameters[0]);
    else if(_parser->sCommand == "slope")      slope = interpretFloat(_parser->vParameters[0]);
    else if(_parser->sCommand == "elevation")  elevation = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "color")
    {
      if(_parser->vParameters[0][0] == '0' && (_parser->vParameters[0][1] == 'x' || _parser->vParameters[0][1] == 'X'))
        interpretRBGx(_parser->vParameters[0], color);
      else
        gLog("Invalid hexadecimal color for \"%s\" object at line %d in file %s.", 'E', DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif