#ifndef _OBS_EPOCHDATA_H_
#define _OBS_EPOCHDATA_H_
#include "timer.h"
#include "data/data.object.h"

class ObscurusEpochData : public OBS::Object
{
public:
  unsigned short  proto_year[2];
  unsigned char   secs_in_min;
  unsigned char   mins_in_hour;
  unsigned char   hours_in_day;
  unsigned short  days_in_year;

  ObscurusEpochData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_EPOCH) : Object(_name, _type) //--- default values ---
  {
    proto_year[0] = 2000;
    proto_year[1] = 1;
    secs_in_min = 5;
    mins_in_hour = 60;
    hours_in_day = 24;
    days_in_year = 365;
    unique = true;
  }

  bool link()
  {
    if(proto_year[0] < 1900)
    {
      gLog("%s \"%s\" : Real year in proto year cannot be set before 1900. Adjusting to 1900.", 'W', DataTypeNames(type), data_name.c_str());
      proto_year[1] = 1900;
    }
    unsigned short current_year = (unsigned short)gTimer->getCurrentYear();
    if(proto_year[0] > current_year)
    {
      gLog("%s \"%s\" : Proto year cannot be set in the future. Adjusting to current year.", 'W', DataTypeNames(type), data_name.c_str());
      proto_year[1] = current_year;
    }
    if(proto_year[1] == 0)
    {
      gLog("%s \"%s\" : Game year in proto year cannot be set to zero. Adjusting to 1.", 'W', DataTypeNames(type), data_name.c_str());
      proto_year[1] = 1;
    }
    if(secs_in_min == 0)
    {
      gLog("%s \"%s\" : Real secs in one game minute cannot be set to zero. Adjusting to 1.", 'W', DataTypeNames(type), data_name.c_str());
      secs_in_min = 1;
    }
    if(mins_in_hour == 0)
    {
      gLog("%s \"%s\" : minutes in one game hour cannot be set to zero. Adjusting to 1.", 'W', DataTypeNames(type), data_name.c_str());
      mins_in_hour = 1;
    }
    if(hours_in_day == 0)
    {
      gLog("%s \"%s\" : hours in one game day cannot be set to zero. Adjusting to 1.", 'W', DataTypeNames(type), data_name.c_str());
      hours_in_day = 1;
    }
    if(days_in_year == 0)
    {
      gLog("%s \"%s\" : days in one game year cannot be set to zero. Adjusting to 1.", 'W', DataTypeNames(type), data_name.c_str());
      days_in_year = 1;
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "proto_year")
    {
      proto_year[0] = (unsigned short)interpretULong(_parser->vParameters[0]);
      if(_parser->vParameters.size() > 1)  proto_year[1] = (short)interpretULong(_parser->vParameters[1]);
      else gLog("No game year in proto year for \"%s\" object at line %d in file %s.", 'E', DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else if(_parser->sCommand == "secs_in_min")   secs_in_min = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "mins_in_hour")  mins_in_hour = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "hours_in_day")  hours_in_day = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "days_in_year")  days_in_year = (unsigned short)interpretULong(_parser->vParameters[0]);
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};
#endif