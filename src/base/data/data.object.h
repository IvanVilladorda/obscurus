#ifndef _OBSCURUS_OBJECT_H_
#define _OBSCURUS_OBJECT_H_
#include "log.h"
#include "data.parser.h"
#include <map>
#include <boost/filesystem.hpp>
#include <algorithm>
namespace fs = boost::filesystem;

namespace OBS
{
  enum ObjectType
  {
    OBJ_ENTITY, // special, do not use it in DATA
    OBJ_DEFINE,
    OBJ_DECLARE,
    OBJ_EPOCH,
    OBJ_WORLD,
    OBJ_SCRIPT,
    OBJ_CHARACTER,
    OBJ_MARKER,
    OBJ_LANDMARK,
    OBJ_CULTURE,
    OBJ_LANGUAGE,
    OBJ_TERRAIN,
    OBJ_SOIL,
    OBJ_WORLDREGION,
    OBJ_REGIONALTERRAIN,
    OBJ_FORMULA,
    OBJ_UNKNOWN // - last
  };
  enum Sex
  {
    SEX_UNKNOWN = -1,
    FEMALE,
    MALE
  };
  enum Attribute // hard-coded ones (2 other are free)
  {
    A_AGI,  // AGILITY
    A_APP,  // APPEARANCE
    A_CON,  // CONSTITUTION
    A_EMO,  // EMOTION
    A_STR,  // STRENGTH
    A_INT,  // INTELLIGENCE
    A_PAR,  // PARADIGM
    A_SOC    // SOCIABILITY
  };

  void* getObjectIndexByName(ObjectType _objType, const std::string& _objName); // prototype of global function used by objects
  static const char* DataTypeNames(ObjectType _objType) // all names of object types
  {
    switch(_objType)
    {
    case OBJ_ENTITY: return "ENTITY";
    case OBJ_DEFINE: return "DEFINE";
    case OBJ_DECLARE: return "DECLARE";
    case OBJ_EPOCH: return "EPOCH";
    case OBJ_WORLD: return "WORLD";
    case OBJ_SCRIPT: return "SCRIPT";
    case OBJ_CHARACTER: return "CHARACTER";
    case OBJ_MARKER: return "MARKER";
    case OBJ_LANDMARK: return "LANDMARK";
    case OBJ_CULTURE: return "CULTURE";
    case OBJ_LANGUAGE: return "LANGUAGE";
    case OBJ_TERRAIN: return "TERRAIN";
    case OBJ_SOIL: return "SOIL";
    case OBJ_FORMULA: return "FORMULA";
    case OBJ_WORLDREGION: return "REGION";
    case OBJ_REGIONALTERRAIN: return "REGIONALTERRAIN";
    case OBJ_UNKNOWN:
    default: break;
    }
    return "UNKNOWN";
  }

  class Object
  {
  public:
    Object(const std::string& _name = "Object", ObjectType _type = OBJ_UNKNOWN) { data_name = _name;  type = _type; unique = false; }
    std::string data_name;
    std::string last_module;  // last module touching it
    std::string last_file;    // last file touching it
    ObjectType type;
    bool unique;              // create one and only one
    virtual bool link() = 0;
    virtual void interpret(ObscurusDataParser* _parser) final
    {
      last_module = _parser->sModule;
      last_file = _parser->sFile;
      _interpret(_parser);
    }
    bool interpretBoolean(std::string& _value)
    {
      std::transform(_value.begin(), _value.end(), _value.begin(), ::tolower);
      if(_value == "yes" || _value == "true" || _value == "1")  return true;
      return false;
    }
    Sex interpretSex(std::string& _value)
    {
      std::transform(_value.begin(), _value.end(), _value.begin(), ::tolower);
      if(_value == "male" || _value == "1")  return MALE;
      if(_value == "female" || _value == "0")  return FEMALE;
      return SEX_UNKNOWN;
    }
    void interpretRBGx(std::string& _value, unsigned char* _color)
    {
      std::string R = "0x" + _value.substr(2, 2);
      std::string G = "0x" + _value.substr(4, 2);
      std::string B = "0x" + _value.substr(6, 2);
      _color[0] = strtoul(R.c_str(), nullptr, 0);
      _color[1] = strtoul(G.c_str(), nullptr, 0);
      _color[2] = strtoul(B.c_str(), nullptr, 0);
    }
    double interpretDouble(std::string _value) { return strtod(_value.c_str(), nullptr); }
    float interpretFloat(std::string _value) { return strtof(_value.c_str(), nullptr); }
    unsigned long interpretULong(std::string _value) { return strtoul(_value.c_str(), nullptr, 0); }
    long interpretLong(std::string _value) { return strtol(_value.c_str(), nullptr, 0); }

  private:
    virtual void _interpret(ObscurusDataParser*) = 0;
  };

} // -- namespace OBS
#endif