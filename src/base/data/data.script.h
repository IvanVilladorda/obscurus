#ifndef _OBS_SCRIPTDATA_H_
#define _OBS_SCRIPTDATA_H_
#include "data/data.object.h"

class ObscurusScriptData : public OBS::Object
{
public:
  std::string file_name;
  std::string compiled;
  bool        has_been_outputted;

  ObscurusScriptData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_SCRIPT) : Object(_name, _type) //--- default values ---
  {
    has_been_outputted = false;
  }

  bool link()
  {
    if(file_name.empty() || !fs::exists(file_name))
    {
      gLog("File %s for %s \"%s\" does not exists.", 'E', file_name.c_str(), DataTypeNames(type), data_name.c_str());
      file_name.clear();
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->vParameters.size() > 1)
      file_name = _parser->vParameters[1];
    else
      gLog("%s \"%s\" have no file in parameter.", 'E', DataTypeNames(type), data_name.c_str());
  }
};

#endif