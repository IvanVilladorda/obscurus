#ifndef _OBS_SOILDATA_H_
#define _OBS_SOILDATA_H_

class ObscurusSoilData : public OBS::Object
{
public:

  ObscurusSoilData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_LANGUAGE) : Object(_name, _type) //--- default values ---
  {
  }

  bool link()
  {
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
  }
};

#endif