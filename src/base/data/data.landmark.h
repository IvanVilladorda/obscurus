#ifndef _OBS_LANDMARKDATA_H_
#define _OBS_LANDMARKDATA_H_
#include "data/data.object.h"
#include "data/data.world.h"

class ObscurusLandmarkData : public OBS::Object
{
public:
  std::string         world_name;
  ObscurusWorldData*  pWorld;
  unsigned int        cell;
  char                chunk;
  char                parcel;
  char                tile;
  char                square;
  char                spot;
  unsigned char       persistance_level;
  bool                settlement;

  ObscurusLandmarkData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_LANDMARK) : Object(_name, _type) //--- default values ---
  {
    cell = 0;
    chunk = parcel = tile = square = spot = -1;
    pWorld = 0;
    persistance_level = 0;
    settlement = false;
  }

  bool link()
  {
    pWorld = static_cast<ObscurusWorldData*>(getObjectIndexByName(OBS::OBJ_WORLD, world_name));
    if(!pWorld)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    if(chunk < -1)  chunk = -1;    else if(chunk > 15)   chunk = 15;
    if(parcel < -1)  parcel = -1;  else if(parcel > 15)  parcel = 15;
    if(tile < -1)    tile = -1;    else if(tile > 15)    tile = 15;
    if(square < -1)  square = -1;  else if(square > 15)  square = 15;
    if(spot < -1)    spot = -1;    else if(spot > 15)    spot = 15;
    if(persistance_level > 2)
    {
      gLog("Persistance level for %s object named \"%s\" cannot exceed 2. Setting to zero.", 'W', DataTypeNames(type), data_name.c_str());
      persistance_level = 0;
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "world_name")              world_name = _parser->vParameters[0];
    else if(_parser->sCommand == "cell")               cell = (unsigned int)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "chunk")              chunk = (char)interpretLong(_parser->vParameters[0]);
    else if(_parser->sCommand == "parcel")             parcel = (char)interpretLong(_parser->vParameters[0]);
    else if(_parser->sCommand == "tile")               tile = (char)interpretLong(_parser->vParameters[0]);
    else if(_parser->sCommand == "square")             square = (char)interpretLong(_parser->vParameters[0]);
    else if(_parser->sCommand == "spot")               spot = (char)interpretLong(_parser->vParameters[0]);
    else if(_parser->sCommand == "persistance_level")  persistance_level = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "settlement")         settlement = interpretBoolean(_parser->vParameters[0]);
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif