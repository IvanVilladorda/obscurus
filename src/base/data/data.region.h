#ifndef _OBS_REGIONDATA_H_
#define _OBS_REGIONDATA_H_

class ObscurusRegionData : public OBS::Object
{
public:
  unsigned char    code[3];

  ObscurusRegionData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_WORLDREGION) : Object(_name, _type) //--- default values ---
  {
    code[0] = code[1] = code[2] = 0;
  }

  bool link()
  {
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
  }
};

#endif