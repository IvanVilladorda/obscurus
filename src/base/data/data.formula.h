#ifndef _OBS_FORMULADATA_H_
#define _OBS_FORMULADATA_H_
#include <boost/algorithm/string.hpp>
#include "data/data.object.h"

namespace OBS
{
  enum Module
  {
    N_NULL,
    N_ABS,
    N_ADD,
    N_BILLOW,
    N_CLAMP,
    N_CONST,
    N_CURVE,
    N_MAX,
    N_MIN,
    N_MUL,
    N_PERLIN,
    N_POW,
    N_RIDGED,
    N_SCALE,
    N_SELECT,
    N_TERRACE,
    N_VORONOI
  };
}

class ObscurusFormulaData : public OBS::Object
{
public:
  OBS::Module                         base_module;
  std::map<std::string, std::string>  params;
  std::string                         module1;
  ObscurusFormulaData*                pModule1;
  std::string                         module2;
  ObscurusFormulaData*                pModule2;
  std::string                         module3;
  ObscurusFormulaData*                pModule3;

  ObscurusFormulaData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_FORMULA) : Object(_name, _type) //--- default values ---
  {
    base_module = OBS::N_NULL;
    pModule1 = pModule2 = pModule3 = 0;
  }

  bool link()
  {
    if(!module1.empty())
    {
      pModule1 = static_cast<ObscurusFormulaData*>(getObjectIndexByName(OBS::OBJ_FORMULA, module1));
      if(!pModule1)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(!module2.empty())
    {
      pModule2 = static_cast<ObscurusFormulaData*>(getObjectIndexByName(OBS::OBJ_FORMULA, module2));
      if(!pModule2)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(!module3.empty())
    {
      pModule3 = static_cast<ObscurusFormulaData*>(getObjectIndexByName(OBS::OBJ_FORMULA, module3));
      if(!pModule3)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "base_module")
    {
      if(_parser->vParameters[0] == "ABS")           base_module = OBS::N_ABS;
      else if(_parser->vParameters[0] == "ADD")      base_module = OBS::N_ADD;
      else if(_parser->vParameters[0] == "BILLOW")   base_module = OBS::N_BILLOW;
      else if(_parser->vParameters[0] == "CLAMP")    base_module = OBS::N_CLAMP;
      else if(_parser->vParameters[0] == "CONST")    base_module = OBS::N_CONST;
      else if(_parser->vParameters[0] == "CURVE")    base_module = OBS::N_CURVE;
      else if(_parser->vParameters[0] == "MAX")      base_module = OBS::N_MAX;
      else if(_parser->vParameters[0] == "MIN")      base_module = OBS::N_MIN;
      else if(_parser->vParameters[0] == "MUL")      base_module = OBS::N_MUL;
      else if(_parser->vParameters[0] == "PERLIN")   base_module = OBS::N_PERLIN;
      else if(_parser->vParameters[0] == "POW")      base_module = OBS::N_POW;
      else if(_parser->vParameters[0] == "RIDGED")   base_module = OBS::N_RIDGED;
      else if(_parser->vParameters[0] == "SCALE")    base_module = OBS::N_SCALE;
      else if(_parser->vParameters[0] == "SELECT")   base_module = OBS::N_SELECT;
      else if(_parser->vParameters[0] == "TERRACE")  base_module = OBS::N_TERRACE;
      else if(_parser->vParameters[0] == "VORONOI")  base_module = OBS::N_VORONOI;
      else
        gLog("Unknown noise module \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->vParameters[0].c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else if(_parser->sCommand == "params")
    {
      std::vector<std::string> vParams, vTab;
      boost::split(vParams, _parser->vParameters[0], boost::is_any_of(";"));
      for(size_t i = 0; i < vParams.size(); i++)
      {
        boost::split(vTab, vParams[i], boost::is_any_of(":"));
        if(vTab.size() > 1) params[vTab[0]] = vTab[1];
      }
    }
    else if(_parser->sCommand == "module1")  module1 = _parser->vParameters[0];
    else if(_parser->sCommand == "module2")  module2 = _parser->vParameters[0];
    else if(_parser->sCommand == "module3")  module3 = _parser->vParameters[0];
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif