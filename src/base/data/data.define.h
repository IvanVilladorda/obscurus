#ifndef _OBS_DEFINEDATA_H_
#define _OBS_DEFINEDATA_H_

class ObscurusDefineData : public OBS::Object
{
public:
  std::string  value;
  bool         global; // syn = modifiable ?

  ObscurusDefineData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_DEFINE) : Object(_name, _type) //--- default values ---
  {
    global = false; // constant by default (#define)
  }

  bool link()
  {
    // nothing
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "#global")   global = true;
    if(_parser->vParameters.size() > 1)  value = _parser->vParameters[1];
    else                                 gLog("%s named \"%s\" have no value.", 'W', _parser->sCommand.c_str(), data_name.c_str());
  }
};

#endif