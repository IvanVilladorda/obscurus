#ifndef _OBS_REGIONALTERRAINDATA_H_
#define _OBS_REGIONALTERRAINDATA_H_
#include "data/data.object.h"
#include "data/data.region.h"
#include "data/data.terrain.h"

class ObscurusRegionalTerrainData : public OBS::Object
{
public:
  std::string            region;
  ObscurusRegionData*    pRegion;
  std::string            terrain;
  ObscurusTerrainData*  pTerrain;

  ObscurusRegionalTerrainData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_REGIONALTERRAIN) : Object(_name, _type) //--- default values ---
  {
    pRegion = 0;
    pTerrain = 0;
  }

  bool link()
  {
    if(!region.empty())
    {
      pRegion = static_cast<ObscurusRegionData*>(getObjectIndexByName(OBS::OBJ_WORLDREGION, region));
      if(!pRegion)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    if(!terrain.empty())
    {
      pTerrain = static_cast<ObscurusTerrainData*>(getObjectIndexByName(OBS::OBJ_TERRAIN, terrain));
      if(!pTerrain)
        gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    }
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "region")        region = _parser->vParameters[0];
    else if(_parser->sCommand == "terrain")  terrain = _parser->vParameters[0];
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif