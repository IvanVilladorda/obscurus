#ifndef _OBS_CULTUREDATA_H_
#define _OBS_CULTUREDATA_H_
#include "data/data.object.h"
#include "data/data.language.h"

class ObscurusCultureData : public OBS::Object
{
public:
  std::string            parent;
  std::string            language;
  ObscurusCultureData*   pParent;
  ObscurusLanguageData*  pLanguage;
  unsigned short         indice;

  ObscurusCultureData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_CULTURE) : Object(_name, _type) //--- default values ---
  {
    pParent = 0;
    indice = 0;
    pLanguage = 0;
  }

  bool link()
  {
    pParent = static_cast<ObscurusCultureData*>(getObjectIndexByName(OBS::OBJ_CULTURE, parent));
    if(!pParent)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    pLanguage = static_cast<ObscurusLanguageData*>(getObjectIndexByName(OBS::OBJ_LANGUAGE, language));
    if(!pLanguage)
      gLog("\tin %s named \"%s\"", 'E', DataTypeNames(type), data_name.c_str()); // pursue error message from getObjectIndexByName => get
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "parent")         parent = _parser->vParameters[0];
    else if(_parser->sCommand == "language")  language = _parser->vParameters[0];
    else if(_parser->sCommand == "indice")    indice = (unsigned short)interpretULong(_parser->vParameters[0]);
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif