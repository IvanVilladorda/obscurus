#ifndef _OBS_LANGUAGEDATA_H_
#define _OBS_LANGUAGEDATA_H_

class ObscurusLanguageData : public OBS::Object
{
public:

  ObscurusLanguageData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_LANGUAGE) : Object(_name, _type) //--- default values ---
  {
  }

  bool link()
  {
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
  }
};

#endif