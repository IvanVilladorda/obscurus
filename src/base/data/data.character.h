#ifndef _OBS_CHARACTERDATA_H_
#define _OBS_CHARACTERDATA_H_
#include "data/data.object.h"
#include "data/data.culture.h"
#include "data/data.landmark.h"

class ObscurusCharacterData : public OBS::Object
{
public:
  std::string             name;
  OBS::Sex                sex;
  unsigned char           generation;
  unsigned char           redemption;
  std::string             user;
  std::string             father;
  ObscurusCharacterData*  pFather;
  std::string             mother;
  ObscurusCharacterData*  pMother;
  std::string             culture;
  ObscurusCultureData*    pCulture;
  unsigned short          integrity;
  std::string             birth_landmark;
  ObscurusLandmarkData*   pBirthLandmark;
  short                   birth_year; // indicative
  std::map<std::string, char>           symbols;
  std::map<std::string, unsigned char>  attributes;
  std::map<std::string, unsigned char>  skills;
  std::map<std::string, unsigned char>  counters;
  unsigned char           body[2];
  float                   body_mass;
  float                   body_size;
  std::string             face; // temp
  std::string             model; // temp

  ObscurusCharacterData(const std::string& _name, OBS::ObjectType _type = OBS::OBJ_CHARACTER) : Object(_name, _type) //--- default values ---
  {
    sex = OBS::SEX_UNKNOWN;
    generation = 0;
    redemption = 0;
    pFather = pMother = 0;
    pCulture = 0;
    pBirthLandmark = 0;
    integrity = 0;
    birth_year = 1;
  }

  bool link()
  {
    return true;
  }

private:
  void _interpret(ObscurusDataParser* _parser)
  {
    if(_parser->sCommand == "name")                 name = _parser->vParameters[0];
    else if(_parser->sCommand == "unique")          unique = interpretBoolean(_parser->vParameters[0]);
    else if(_parser->sCommand == "sex")             sex = interpretSex(_parser->vParameters[0]);
    else if(_parser->sCommand == "generation")      generation = (unsigned char)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "redemption")      redemption = interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "user")            user = _parser->vParameters[0];
    else if(_parser->sCommand == "father")          father = _parser->vParameters[0];
    else if(_parser->sCommand == "mother")          mother = _parser->vParameters[0];
    else if(_parser->sCommand == "culture")         culture = _parser->vParameters[0];
    else if(_parser->sCommand == "integrity")       integrity = (unsigned short)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "birth_landmark")  birth_landmark = _parser->vParameters[0];
    else if(_parser->sCommand == "birth_year")      birth_year = (unsigned short)interpretULong(_parser->vParameters[0]);
    else if(_parser->sCommand == "symbol")
    {
      if(_parser->vParameters.size() > 1)
        symbols[_parser->vParameters[1]] = (char)interpretLong(_parser->vParameters[2]);
      else
        gLog("Symbol \"%s\" for \"%s\" object at line %d in file %s has no value.", 'E', _parser->vParameters[1].c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else if(_parser->sCommand == "attribute")
    {
      if(_parser->vParameters.size() > 1)
        attributes[_parser->vParameters[1]] = (unsigned char)interpretULong(_parser->vParameters[2]);
      else
        gLog("Attribute \"%s\" for \"%s\" object at line %d in file %s has no value.", 'E', _parser->vParameters[1].c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else if(_parser->sCommand == "skill")
    {
      if(_parser->vParameters.size() > 1)
        skills[_parser->vParameters[1]] = (unsigned char)interpretULong(_parser->vParameters[2]);
      else
        gLog("Skill \"%s\" for \"%s\" object at line %d in file %s has no value.", 'E', _parser->vParameters[1].c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else if(_parser->sCommand == "counter")
    {
      if(_parser->vParameters.size() > 1)
        counters[_parser->vParameters[1]] = (unsigned char)interpretULong(_parser->vParameters[2]);
      else
        gLog("Counter \"%s\" for \"%s\" object at line %d in file %s has no value.", 'E', _parser->vParameters[1].c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
    }
    else
      gLog("Inappropriate command \"%s\" for \"%s\" object at line %d in file %s.", 'E', _parser->sCommand.c_str(), DataTypeNames(type), _parser->nLine, _parser->sFile.c_str());
  }
};

#endif