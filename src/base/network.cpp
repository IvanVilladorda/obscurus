#include "network.h"
#include "log.h"
#include <RakNetVersion.h>
template<> ObscurusNetwork* Singleton<ObscurusNetwork>::inst = 0;
ObscurusNetwork* ObscurusNetwork::getSingleton(){ return inst; }

// ~~ CONSTRUCTOR
ObscurusNetwork::ObscurusNetwork()
  : peer(0), packet(0)
{
  gLog("-*-*- Network Startup.", 'I');
  gLog("Network is using Raknet %s.", 'I', RAKNET_VERSION);

  peer = RakNet::RakPeerInterface::GetInstance();
  if(!gPrefs->get("server_port")) gPrefs->set("server_port", std::to_string(SERVER_PORT)); // default : 65320
#ifdef _OBS_SERVER_
  if(!gPrefs->get("server_max_clients")) gPrefs->set("server_max_clients", std::to_string(MAX_CLIENTS)); // default : 10
  RakNet::SocketDescriptor sd(std::atoi(gPrefs->get("server_port").c_str()), 0);
  peer->Startup(std::atoi(gPrefs->get("server_port").c_str()), &sd, 1);
  peer->SetMaximumIncomingConnections(std::atoi(gPrefs->get("server_max_clients").c_str()));
  gLog("Starting server on port : %s.", 'I', gPrefs->get("server_port").c_str());
  gLog("max allowed connections : %s.", 'I', gPrefs->get("server_max_clients").c_str());
#else
  if(!gPrefs->get("server_address")) gPrefs->set("server_address", SERVER_ADDR); // default : 127.0.0.1
#endif
}

// ~~ DESTRUCTOR
ObscurusNetwork::~ObscurusNetwork()
{
  gLog("*-*-* Network Shutdown.", 'I');
  RakNet::RakPeerInterface::DestroyInstance(peer);
}

// ~~ CONNECT : connect to a server (client func)
void ObscurusNetwork::connect()
{
  // change prefs before this call to connect with good credentials
  RakNet::SocketDescriptor sd;
  gLog("Connecting to server...", 'I');
  peer->Startup(1, &sd, 1);
  peer->Connect(gPrefs->get("server_address").c_str(), std::atoi(gPrefs->get("server_port").c_str()), 0, 0);
  gLog("\tserver address : %s.", 'I', gPrefs->get("server_address").c_str());
  gLog("\tserver port : %s.", 'I', gPrefs->get("server_port").c_str());
}

// ~~ LISTEN : call a time by loop, essentially to get new comers on server
void ObscurusNetwork::listen()
{
  return;
/*
  for(packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
  {
    switch(packet->data[0])
    {
#ifdef _OBS_SERVER_
#else
#endif
      //lobby, peer2peer :
      case ID_REMOTE_DISCONNECTION_NOTIFICATION:
        gLog("ID_REMOTE_DISCONNECTION_NOTIFICATION : Another client has disconnected.", 'D');
        break;
      case ID_REMOTE_CONNECTION_LOST:
        gLog("ID_REMOTE_CONNECTION_LOST : Another client has lost the connection.", 'D');
        break;
      case ID_REMOTE_NEW_INCOMING_CONNECTION:
        gLog("ID_REMOTE_NEW_INCOMING_CONNECTION : Another client has connected.", 'D');
        break;
      //CS :
      case ID_CONNECTION_REQUEST_ACCEPTED:
        {
          gLog("ID_CONNECTION_REQUEST_ACCEPTED : Our connection request has been accepted.", 'D');
          // Use a BitStream to write a custom user message
          // Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
          RakNet::BitStream bsOut;
          bsOut.Write((RakNet::MessageID)OBS::NET_GAME_NULL);
          bsOut.Write("Welcomed in Obscurus Communication Layer");
          peer->Send(&bsOut,HIGH_PRIORITY,RELIABLE_ORDERED,0,packet->systemAddress,false);
        }
        break;
      case ID_NEW_INCOMING_CONNECTION:
        gLog("ID_NEW_INCOMING_CONNECTION : A connection is incoming.", 'D');
        // check credentials
        // create a thread for this socket and forget
        break;
      case ID_NO_FREE_INCOMING_CONNECTIONS:
        gLog("ID_NO_FREE_INCOMING_CONNECTIONS : The server is full.", 'D');
        break;
      case ID_DISCONNECTION_NOTIFICATION:
#ifdef _OBS_SERVER_
        gLog("ID_DISCONNECTION_NOTIFICATION : A client has disconnected.", 'D');
#else
        gLog("ID_DISCONNECTION_NOTIFICATION : We have been disconnected.", 'D');
#endif
        break;
      case ID_CONNECTION_LOST:
#ifdef _OBS_SERVER_
        gLog("ID_CONNECTION_LOST : A client lost the connection.", 'D');
#else
        gLog("ID_CONNECTION_LOST : Connection lost.", 'D');
#endif
        break;
      // GM :
      case OBS::NET_GAME_NULL:
        {
          RakNet::RakString rs;
          RakNet::BitStream bsIn(packet->data,packet->length,false);
          bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
          bsIn.Read(rs);
          gLog("ID_GAME_NULL_MESSAGE : Message : %s.", 'D', rs.C_String());
        }
        break;
      default:
        gLog("!!! NETWORK : UNKNOWN ID IN MESSAGE : %i.", 'W', packet->data[0]);
        break;
    }
  }
  */
}

// ~~ TALK : 
void ObscurusNetwork::talk()
{
}
