#ifndef _OBSCURUS_SINGLETON_
#define _OBSCURUS_SINGLETON_
// -- SINGLETON TEMPLATE
template <typename T> class Singleton
{
  public:
    static T* getSingleton() { return inst; }
    Singleton() { inst = static_cast<T*>(this); }
    ~Singleton() { inst = 0; }

  protected:
    static T* inst;

  private:
    Singleton(const Singleton<T> &);
    void operator=(const Singleton<T> &);
};
#endif