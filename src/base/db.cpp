#include "db.h"
#include "log.h"
#include "preferences.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
template<> ObscurusDB* Singleton<ObscurusDB>::inst = 0;
ObscurusDB* ObscurusDB::getSingleton(){ return inst; }

static int DBcallback(void* _param, int argc, char **argv, char **azColName)
{
  ObscurusRows* r = (ObscurusRows*)_param;
  return r->callback(_param, argc, argv, azColName);
}

ObscurusDB::ObscurusDB()
  : db(0),
  bOperational(false)
{
  gLog("-*-*- DB Startup.", 'I');
  gLog("DB is using SQLite %s.", 'I', SQLITE_VERSION);
  // init by opening db
  init();
  // get SQL statements
  gLog("Parsing SQL statements.", 'I');
  std::ifstream oStream(gPrefs->get("_FullPath") + "dat/sql/statements.ini", std::ifstream::in);
  std::string sLine, sSection, sObject, sStatement;
  while(!oStream.eof())
  {
    std::getline(oStream, sLine);
    if(sLine.length() && sLine.at(0) != '#')
    {
      if(sLine.at(0) == '[' && sLine.at(sLine.length() - 1) == ']')
      {
        sSection = sLine.substr(1, sLine.length() - 2);
        boost::trim(sSection);
      }
      else
      {
        size_t nPos = sLine.find_first_of('=');
        if(nPos != std::string::npos)
        {
          sObject = sLine.substr(0, nPos);
          sStatement = sLine.substr(nPos + 1);
          boost::trim(sObject);
          boost::trim(sStatement);
          if(sSection == "INSERT")
          {
            if(sObject == "entity") sqlite3_prepare(db, sStatement.c_str(), -1, &inserts[OBS::OBJ_ENTITY], 0);
            else if(sObject == "character") sqlite3_prepare(db, sStatement.c_str(), -1, &inserts[OBS::OBJ_CHARACTER], 0);
          }
          else if(sSection == "UPDATE")
          {
            if(sObject == "entity") sqlite3_prepare(db, sStatement.c_str(), -1, &updates[OBS::OBJ_ENTITY], 0);
            else if(sObject == "character") sqlite3_prepare(db, sStatement.c_str(), -1, &updates[OBS::OBJ_CHARACTER], 0);
          }
          else if(sSection == "DELETE")
          {
            if(sObject == "entity") sqlite3_prepare(db, sStatement.c_str(), -1, &deletes[OBS::OBJ_ENTITY], 0);
            else if(sObject == "character") sqlite3_prepare(db, sStatement.c_str(), -1, &deletes[OBS::OBJ_CHARACTER], 0);
          }
        }
      }
    }
  }
  oStream.close();
}

ObscurusDB::~ObscurusDB()
{
  gLog("*-*-* DB Shutdown.", 'I');
  (void)sqlite3_close(db);
}

void ObscurusDB::init() // or reinit
{
  if(db)  (void)sqlite3_close(db);
  bOperational = false;
  gLog("Creating partial in-memory database.", 'I');
  //if(SQLITE_OK == sqlite3_open(":memory:", &db))
  if(SQLITE_OK != sqlite3_open("", &db)) // temporary : in-memory, with flushes on disk when large
  {
    gLog("Error while creating database.", 'E');
    return;
  }
  gLog("Getting database schema.", 'I');
  std::string sSchema;
  std::string sSchemaFile = gPrefs->get("_FullPath") + "dat/sql/schema.sql";
  FILE* oFile = fopen(sSchemaFile.c_str(), "rb");
  if(oFile)
  {
    fseek(oFile, 0, SEEK_END);
    sSchema.resize(ftell(oFile));
    fseek(oFile, 0, SEEK_SET);
    fread(&sSchema[0], sizeof(sSchema[0]), sSchema.size(), oFile);
    fclose(oFile);
  }
  else
  {
    gLog("Unable to get database schema (%s).", 'E', sSchemaFile.c_str());
    return;
  }
  gLog("Injecting SQL.", 'I');
  if(SQLITE_OK != sqlite3_exec(db, sSchema.c_str(), 0, 0, &pErr))
  {
    gLog("Error while injecting database schema :\n\n%s\n", 'E', pErr);
    sqlite3_free(pErr);
    return;
  }
  bOperational = true;
  /*
  gLog("Database schema :", 'D');
  auto rows = exec("SELECT name FROM sqlite_master WHERE type='table'");
  sSchema = "\n";
  for(size_t i = 0; i < rows->numRows; i++)
  {
    for(auto val : rows->rows[i]) sSchema = sSchema + val.first + " => " + val.second + " ; ";
    sSchema = sSchema + "\n";
  }
  gLog("%s", 'D', sSchema.c_str());
  */
  gLog("Database ready to use.", 'I');
}

std::unique_ptr<ObscurusRows> ObscurusDB::exec(const std::string& sql)
{
  std::unique_ptr<ObscurusRows> rows(new ObscurusRows());
  if(!bOperational)
  {
    gLog("Database is not operational, exec denied.", 'E');
    return rows;
  }
  LOCK_DB;
  if(SQLITE_OK != sqlite3_exec(db, sql.c_str(), DBcallback, reinterpret_cast<void *>(rows.get()), &pErr))
  {
    gLog("Error while excuting SQL : %s\n\n%s\n", 'E', sql.c_str(), pErr);
    sqlite3_free(pErr);
  }
  return rows;
}

// -------------------------------------------------------------------------------------------- INSERT ENTITY
void ObscurusDB::insert(OBS::Entity* _entity)
{
  if(!bOperational) { gLog("Database is not operational, insert entity denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(inserts[OBS::OBJ_ENTITY]);
    sqlite3_bind_int64(inserts[OBS::OBJ_ENTITY], 1, _entity->id);
    sqlite3_bind_text(inserts[OBS::OBJ_ENTITY], 2, _entity->typeName.c_str(), _entity->typeName.size(), 0);
    sqlite3_bind_text(inserts[OBS::OBJ_ENTITY], 3, _entity->dataName.c_str(), _entity->dataName.size(), 0);
    sqlite3_bind_text(inserts[OBS::OBJ_ENTITY], 4, _entity->worldName.c_str(), _entity->worldName.size(), 0);
    sqlite3_bind_int(inserts[OBS::OBJ_ENTITY], 5, _entity->gridX);
    sqlite3_bind_int(inserts[OBS::OBJ_ENTITY], 6, _entity->gridY);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 7, _entity->posX);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 8, _entity->posY);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 9, _entity->posZ);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 10, _entity->yaw);
    if(sqlite3_step(inserts[OBS::OBJ_ENTITY]) != SQLITE_DONE) gLog("Database could not execute INSERT statement.", 'E');
  }
}

// -------------------------------------------------------------------------------------------- UPDATE ENTITY
void ObscurusDB::flush(OBS::Entity* _entity)
{
  if(!bOperational) { gLog("Database is not operational, flush entity denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(updates[OBS::OBJ_ENTITY]);
    sqlite3_bind_text(updates[OBS::OBJ_ENTITY], 1, _entity->typeName.c_str(), _entity->typeName.size(), 0);
    sqlite3_bind_text(updates[OBS::OBJ_ENTITY], 2, _entity->dataName.c_str(), _entity->dataName.size(), 0);
    sqlite3_bind_text(updates[OBS::OBJ_ENTITY], 3, _entity->worldName.c_str(), _entity->worldName.size(), 0);
    sqlite3_bind_int(updates[OBS::OBJ_ENTITY], 4, _entity->gridX);
    sqlite3_bind_int(updates[OBS::OBJ_ENTITY], 5, _entity->gridY);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 6, _entity->posX);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 7, _entity->posY);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 8, _entity->posZ);
    sqlite3_bind_double(inserts[OBS::OBJ_ENTITY], 9, _entity->yaw);
    sqlite3_bind_int64(updates[OBS::OBJ_ENTITY], 10, _entity->id);
    if(sqlite3_step(updates[OBS::OBJ_ENTITY]) != SQLITE_DONE) gLog("Database could not execute UPDATE statement.", 'E');
  }
}

// -------------------------------------------------------------------------------------------- DELETE ENTITY
void ObscurusDB::remove(OBS::Entity* _entity)
{
  if(!bOperational) { gLog("Database is not operational, remove entity denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(deletes[OBS::OBJ_ENTITY]);
    sqlite3_bind_int64(deletes[OBS::OBJ_ENTITY], 1, _entity->id);
    if(sqlite3_step(deletes[OBS::OBJ_ENTITY]) != SQLITE_DONE) gLog("Database could not execute DELETE statement.", 'E');
  }
}
// -------------------------------------------------------------------------------------------- INSERT CHARACTER
void ObscurusDB::insert(OBS::Character* _character)
{
  if(!bOperational) { gLog("Database is not operational, insert character denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(inserts[OBS::OBJ_CHARACTER]);
    sqlite3_bind_int64(inserts[OBS::OBJ_CHARACTER], 1, _character->id);
    sqlite3_bind_text(inserts[OBS::OBJ_CHARACTER], 2, _character->name.c_str(), _character->name.size(), 0);
    sqlite3_bind_int(inserts[OBS::OBJ_CHARACTER], 3, _character->sex);
    sqlite3_bind_text(inserts[OBS::OBJ_CHARACTER], 4, _character->user.c_str(), _character->user.size(), 0);
    if(sqlite3_step(inserts[OBS::OBJ_CHARACTER]) != SQLITE_DONE) gLog("Database could not execute INSERT statement.", 'E');
  }
  insert((OBS::Entity*)_character);
}

// -------------------------------------------------------------------------------------------- UPDATE CHARACTER
void ObscurusDB::flush(OBS::Character* _character)
{
  if(!bOperational) { gLog("Database is not operational, flush character denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(updates[OBS::OBJ_CHARACTER]);
    sqlite3_bind_text(updates[OBS::OBJ_CHARACTER], 1, _character->name.c_str(), _character->name.size(), 0);
    sqlite3_bind_int(inserts[OBS::OBJ_CHARACTER], 2, _character->sex);
    sqlite3_bind_text(inserts[OBS::OBJ_CHARACTER], 3, _character->user.c_str(), _character->user.size(), 0);
    sqlite3_bind_int64(updates[OBS::OBJ_CHARACTER], 4, _character->id);
    if(sqlite3_step(updates[OBS::OBJ_CHARACTER]) != SQLITE_DONE) gLog("Database could not execute UPDATE statement.", 'E');
  }
  flush((OBS::Entity*)_character);
}

// -------------------------------------------------------------------------------------------- DELETE CHARACTER
void ObscurusDB::remove(OBS::Character* _character)
{
  if(!bOperational) { gLog("Database is not operational, remove character denied.", 'E'); return; }
  else
  {
    LOCK_DB;
    sqlite3_reset(deletes[OBS::OBJ_CHARACTER]);
    sqlite3_bind_int64(deletes[OBS::OBJ_CHARACTER], 1, _character->id);
    if(sqlite3_step(deletes[OBS::OBJ_CHARACTER]) != SQLITE_DONE) gLog("Database could not execute DELETE statement.", 'E');
  }
  remove((OBS::Entity*)_character);
}


void ObscurusDB::listSaves()
{
}

void ObscurusDB::open(ObscurusSave _save)
{
}

std::string ObscurusDB::getMasterMod()
{
  return "";
}

/*
** This function is used to load the contents of a database file on disk
** into the "main" database of open database connection pInMemory, or
** to save the current contents of the database opened by pInMemory into
** a database file on disk. pInMemory is probably an in-memory database,
** but this function will also work fine if it is not.
**
** Parameter zFilename points to a nul-terminated string containing the
** name of the database file on disk to load from or save to. If parameter
** isSave is non-zero, then the contents of the file zFilename are
** overwritten with the contents of the database opened by pInMemory. If
** parameter isSave is zero, then the contents of the database opened by
** pInMemory are replaced by data loaded from the file zFilename.
**
** If the operation is successful, SQLITE_OK is returned. Otherwise, if
** an error occurs, an SQLite error code is returned.
*/
int ObscurusDB::loadOrSaveDb(sqlite3 *pInMemory, const char *zFilename, int isSave)
{
  int rc;                   /* Function return code */
  sqlite3 *pFile;           /* Database connection opened on zFilename */
  sqlite3_backup *pBackup;  /* Backup object used to copy data */
  sqlite3 *pTo;             /* Database to copy to (pFile or pInMemory) */
  sqlite3 *pFrom;           /* Database to copy from (pFile or pInMemory) */

  /* Open the database file identified by zFilename. Exit early if this fails
  ** for any reason. */
  rc = sqlite3_open(zFilename, &pFile);
  if(rc == SQLITE_OK)
  {

    /* If this is a 'load' operation (isSave==0), then data is copied
    ** from the database file just opened to database pInMemory.
    ** Otherwise, if this is a 'save' operation (isSave==1), then data
    ** is copied from pInMemory to pFile.  Set the variables pFrom and
    ** pTo accordingly. */
    pFrom = (isSave ? pInMemory : pFile);
    pTo = (isSave ? pFile : pInMemory);

    /* Set up the backup procedure to copy from the "main" database of
    ** connection pFile to the main database of connection pInMemory.
    ** If something goes wrong, pBackup will be set to NULL and an error
    ** code and message left in connection pTo.
    **
    ** If the backup object is successfully created, call backup_step()
    ** to copy data from pFile to pInMemory. Then call backup_finish()
    ** to release resources associated with the pBackup object.  If an
    ** error occurred, then an error code and message will be left in
    ** connection pTo. If no error occurred, then the error code belonging
    ** to pTo is set to SQLITE_OK.
    */
    pBackup = sqlite3_backup_init(pTo, "main", pFrom, "main");
    if(pBackup){
      (void)sqlite3_backup_step(pBackup, -1);
      (void)sqlite3_backup_finish(pBackup);
    }
    rc = sqlite3_errcode(pTo);
  }

  /* Close the database connection opened on database file zFilename
  ** and return the result of this function. */
  (void)sqlite3_close(pFile);
  return rc;
}