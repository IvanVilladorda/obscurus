// -- INPUT.H
#include "input.h"
#include <sstream>
template<> ObscurusInput* Singleton<ObscurusInput>::inst = 0;
ObscurusInput* ObscurusInput::getSingleton(){ return inst; }

ObscurusInput::ObscurusInput()
  : oisInputManager(0),
  oisKeyboard(0),
  oisMouse(0),
  eMouseMode(OBS::OBSI_MENU)
{
  gLog("-*-*- Input Startup.", 'I');
  gLog("Input is using OIS %s.", 'I', OIS_VERSION_NAME);
  memset(&keys, 0, sizeof(keys));
  memset(&mouseb, 0, sizeof(mouseb));
  memset(&oMouse, 0, sizeof(oMouse));
  init();
}

ObscurusInput::~ObscurusInput()
{
  gLog("*-*-* Input Shutdown.", 'I');
  if(oisInputManager)
  {
    if(oisKeyboard)  oisInputManager->destroyInputObject(oisKeyboard);
    if(oisMouse)    oisInputManager->destroyInputObject(oisMouse);
    OIS::InputManager::destroyInputSystem(oisInputManager);
  }
}

void ObscurusInput::init()
{
  gLog("Initializing OIS.", 'I');
  OIS::ParamList mParams;
  size_t windowHnd = gPrefs->getAsLong("_RenderWindow");
  std::ostringstream windowHndStr;
  windowHndStr << windowHnd;
  mParams.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

  if(gPrefs->getAsBool("Fullscreen") == false) // do not grant exclusive while in windowed mode
  {
#if defined OIS_WIN32_PLATFORM
    mParams.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND")));
    mParams.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
    mParams.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
    mParams.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM
    mParams.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
    mParams.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
    mParams.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
    mParams.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
#endif
  }
  oisInputManager = OIS::InputManager::createInputSystem(mParams);
  if(oisInputManager->getNumberOfDevices(OIS::OISKeyboard) > 0)
  {
    oisKeyboard = static_cast<OIS::Keyboard*>(oisInputManager->createInputObject(OIS::OISKeyboard, true));
    oisKeyboard->setEventCallback(this);
  }
  if(oisInputManager->getNumberOfDevices(OIS::OISMouse) > 0)
  {
    oisMouse = static_cast<OIS::Mouse*>(oisInputManager->createInputObject(OIS::OISMouse, true));
    oisMouse->setEventCallback(this);
    //set mouse-to-screen properties
    OIS::MouseState &ms = const_cast<OIS::MouseState &>(oisMouse->getMouseState());
    ms.width = std::atoi(gPrefs->get("_ActiveWidth").c_str());
    ms.height = std::atoi(gPrefs->get("_ActiveHeight").c_str());
    oMouse.absx = ms.width / 2;
    oMouse.absy = ms.height / 2;
  }
}

void ObscurusInput::addListener(ObscurusInputListener* _listener)
{
  for(size_t i = 0; i < vListeners.size(); i++)
  {
    if(vListeners[i] == _listener)
      return;
  }
  vListeners.push_back(_listener);
}

void ObscurusInput::capture()
{
  if(oisKeyboard)  oisKeyboard->capture();
  if(oisMouse)    oisMouse->capture();
}

bool ObscurusInput::getCtrl(std::string const& _action)
{
  // speckey required for this action ?
  switch(mActionTable[_action].eShift)
  { // right side key is automatically set (or unset -- see below keyPressed()), so merely test the left key
  case OBS::OBSI_SHIFT:  if(!keys[OIS::KC_LSHIFT]) return false;
  case OBS::OBSI_CTRL: if(!keys[OIS::KC_LCONTROL]) return false;
  case OBS::OBSI_ALT:  if(!keys[OIS::KC_LMENU]) return false;
  case OBS::OBSI_SHIFT_CTRL:  if(!keys[OIS::KC_LSHIFT] && !keys[OIS::KC_LCONTROL]) return false;
  case OBS::OBSI_SHIFT_ALT: if(!keys[OIS::KC_LSHIFT] && !keys[OIS::KC_LMENU]) return false;
  case OBS::OBSI_CTRL_ALT: if(!keys[OIS::KC_LCONTROL] && !keys[OIS::KC_LMENU]) return false;
  case OBS::OBSI_SHIT_CTRL_ALT: if(!keys[OIS::KC_LSHIFT] && !keys[OIS::KC_LCONTROL] && !keys[OIS::KC_LMENU]) return false;
    default: break;
  }
  // mouse requested
  switch(mActionTable[_action].eMouse)
  {
  case OBS::OBSI_MOUSE_LEFT: return mouseb[OIS::MB_Left];
  case OBS::OBSI_MOUSE_RIGHT: return mouseb[OIS::MB_Right];
  case OBS::OBSI_MOUSE_DELTAUP:  if(oMouse.delta > 0) { oMouse.delta = 0; return true; }
                            else return false; // mouse.delta = 0 is not at its place
  case OBS::OBSI_MOUSE_DELTADOWN:  if(oMouse.delta < 0) { oMouse.delta = 0; return true; }
                              else return false;
  case OBS::OBSI_MOUSE_MIDDLE: return mouseb[OIS::MB_Middle];
    default: break;
  }
  // keyboard requested
  if(mActionTable[_action].key == OIS::KC_UNASSIGNED) return true; // fire for simple shift/ctrl/alt
  return keys[mActionTable[_action].key];
}

void ObscurusInput::setCtrl(std::string const& _action, std::string const& _control)
{
  size_t nPos;

  //search shift/ctrl/alt
  int spec_keys = OBS::OBSI_NONE;
  nPos = _control.find("shift+");  if(nPos != std::string::npos) spec_keys += OBS::OBSI_SHIFT;
  nPos = _control.find("ctrl+");  if(nPos != std::string::npos) spec_keys += OBS::OBSI_CTRL;
  nPos = _control.find("alt+");    if(nPos != std::string::npos) spec_keys += OBS::OBSI_ALT;

  std::string ss = _control;
  if(spec_keys)
  {
    nPos = _control.find_last_of("+") + 1;
    ss = _control.substr(nPos);
  }
  // ss contains now a key or a button

  // -- enjoy the translation, all codes below are those to use as : gInput->setCtrl("zoom out", "wheel-");
  //    after that, just loop through your actions and apply results : if(gInput->getCtrl("zoom out")) zoomOut();
  // mouse
  if(ss == "wheel-")             mActionTable[_action].eMouse = OBS::OBSI_MOUSE_DELTADOWN;
  else if(ss == "wheel+")        mActionTable[_action].eMouse = OBS::OBSI_MOUSE_DELTAUP;
  else if(ss == "left click")    mActionTable[_action].eMouse = OBS::OBSI_MOUSE_LEFT;
  else if(ss == "middle click")  mActionTable[_action].eMouse = OBS::OBSI_MOUSE_MIDDLE;
  else if(ss == "right click")   mActionTable[_action].eMouse = OBS::OBSI_MOUSE_RIGHT;
  //central keys
  else if(ss == "escape")        mActionTable[_action].key = OIS::KC_ESCAPE;
  else if(ss == "backspace")     mActionTable[_action].key = OIS::KC_BACK;
  else if(ss == "tab")           mActionTable[_action].key = OIS::KC_TAB;
  else if(ss == "enter")         mActionTable[_action].key = OIS::KC_RETURN;
  else if(ss == "pause")         mActionTable[_action].key = OIS::KC_PAUSE;
  else if(ss == "caps lock")     mActionTable[_action].key = OIS::KC_CAPITAL;
  else if(ss == "space")         mActionTable[_action].key = OIS::KC_SPACE;
  else if(ss == "pg up")         mActionTable[_action].key = OIS::KC_PGUP;
  else if(ss == "pg down")       mActionTable[_action].key = OIS::KC_PGDOWN;
  else if(ss == "end")           mActionTable[_action].key = OIS::KC_END;
  else if(ss == "home")          mActionTable[_action].key = OIS::KC_HOME;
  else if(ss == "left")          mActionTable[_action].key = OIS::KC_LEFT;
  else if(ss == "up")            mActionTable[_action].key = OIS::KC_UP;
  else if(ss == "right")         mActionTable[_action].key = OIS::KC_RIGHT;
  else if(ss == "down")          mActionTable[_action].key = OIS::KC_DOWN;
  else if(ss == "insert")        mActionTable[_action].key = OIS::KC_INSERT;
  else if(ss == "delete")        mActionTable[_action].key = OIS::KC_DELETE;
  else if(ss == "scroll lock")   mActionTable[_action].key = OIS::KC_SCROLL;
  //numpad
  else if(ss == "numpad 0")  mActionTable[_action].key = OIS::KC_NUMPAD0;
  else if(ss == "numpad 1")  mActionTable[_action].key = OIS::KC_NUMPAD1;
  else if(ss == "numpad 2")  mActionTable[_action].key = OIS::KC_NUMPAD2;
  else if(ss == "numpad 3")  mActionTable[_action].key = OIS::KC_NUMPAD3;
  else if(ss == "numpad 4")  mActionTable[_action].key = OIS::KC_NUMPAD4;
  else if(ss == "numpad 5")  mActionTable[_action].key = OIS::KC_NUMPAD5;
  else if(ss == "numpad 6")  mActionTable[_action].key = OIS::KC_NUMPAD6;
  else if(ss == "numpad 7")  mActionTable[_action].key = OIS::KC_NUMPAD7;
  else if(ss == "numpad 8")  mActionTable[_action].key = OIS::KC_NUMPAD8;
  else if(ss == "numpad 9")  mActionTable[_action].key = OIS::KC_NUMPAD9;
  else if(ss == "numpad .")  mActionTable[_action].key = OIS::KC_DECIMAL;
  else if(ss == "/")         mActionTable[_action].key = OIS::KC_DIVIDE;
  else if(ss == "*")         mActionTable[_action].key = OIS::KC_MULTIPLY;
  else if(ss == "-")         mActionTable[_action].key = OIS::KC_SUBTRACT;
  else if(ss == "+")         mActionTable[_action].key = OIS::KC_ADD;
  else if(ss == "num lock")  mActionTable[_action].key = OIS::KC_NUMLOCK;
  else if(ss == "numpad enter")  mActionTable[_action].key = OIS::KC_NUMPADENTER;
  //func
  else if(ss == "F1")   mActionTable[_action].key = OIS::KC_F1;
  else if(ss == "F2")   mActionTable[_action].key = OIS::KC_F2;
  else if(ss == "F3")   mActionTable[_action].key = OIS::KC_F3;
  else if(ss == "F4")   mActionTable[_action].key = OIS::KC_F4;
  else if(ss == "F5")   mActionTable[_action].key = OIS::KC_F5;
  else if(ss == "F6")   mActionTable[_action].key = OIS::KC_F6;
  else if(ss == "F7")   mActionTable[_action].key = OIS::KC_F7;
  else if(ss == "F8")   mActionTable[_action].key = OIS::KC_F8;
  else if(ss == "F9")   mActionTable[_action].key = OIS::KC_F9;
  else if(ss == "F10")  mActionTable[_action].key = OIS::KC_F10;
  else if(ss == "F11")  mActionTable[_action].key = OIS::KC_F11;
  else if(ss == "F12")  mActionTable[_action].key = OIS::KC_F12;
  //things
  else if(ss == ";")  mActionTable[_action].key = OIS::KC_SEMICOLON;
  else if(ss == "=")  mActionTable[_action].key = OIS::KC_EQUALS;
  else if(ss == ",")  mActionTable[_action].key = OIS::KC_COMMA;
  else if(ss == "-")  mActionTable[_action].key = OIS::KC_MINUS;
  else if(ss == ".")  mActionTable[_action].key = OIS::KC_PERIOD;
  else if(ss == "/")  mActionTable[_action].key = OIS::KC_SLASH;
  else if(ss == "`")  mActionTable[_action].key = OIS::KC_GRAVE;
  else if(ss == "[")  mActionTable[_action].key = OIS::KC_LBRACKET;
  else if(ss == "\\") mActionTable[_action].key = OIS::KC_BACKSLASH;
  else if(ss == "]")  mActionTable[_action].key = OIS::KC_RBRACKET;
  //natural keys
  else if(ss == "A" || ss == "a") mActionTable[_action].key = OIS::KC_A;
  else if(ss == "B" || ss == "b") mActionTable[_action].key = OIS::KC_B;
  else if(ss == "C" || ss == "c") mActionTable[_action].key = OIS::KC_C;
  else if(ss == "D" || ss == "d") mActionTable[_action].key = OIS::KC_D;
  else if(ss == "E" || ss == "e") mActionTable[_action].key = OIS::KC_E;
  else if(ss == "F" || ss == "f") mActionTable[_action].key = OIS::KC_F;
  else if(ss == "G" || ss == "g") mActionTable[_action].key = OIS::KC_G;
  else if(ss == "H" || ss == "h") mActionTable[_action].key = OIS::KC_H;
  else if(ss == "I" || ss == "i") mActionTable[_action].key = OIS::KC_I;
  else if(ss == "J" || ss == "j") mActionTable[_action].key = OIS::KC_J;
  else if(ss == "K" || ss == "k") mActionTable[_action].key = OIS::KC_K;
  else if(ss == "L" || ss == "l") mActionTable[_action].key = OIS::KC_L;
  else if(ss == "M" || ss == "m") mActionTable[_action].key = OIS::KC_M;
  else if(ss == "N" || ss == "n") mActionTable[_action].key = OIS::KC_N;
  else if(ss == "O" || ss == "o") mActionTable[_action].key = OIS::KC_O;
  else if(ss == "P" || ss == "p") mActionTable[_action].key = OIS::KC_P;
  else if(ss == "Q" || ss == "q") mActionTable[_action].key = OIS::KC_Q;
  else if(ss == "R" || ss == "r") mActionTable[_action].key = OIS::KC_R;
  else if(ss == "S" || ss == "s") mActionTable[_action].key = OIS::KC_S;
  else if(ss == "T" || ss == "t") mActionTable[_action].key = OIS::KC_T;
  else if(ss == "U" || ss == "u") mActionTable[_action].key = OIS::KC_U;
  else if(ss == "V" || ss == "v") mActionTable[_action].key = OIS::KC_V;
  else if(ss == "W" || ss == "w") mActionTable[_action].key = OIS::KC_W;
  else if(ss == "X" || ss == "x") mActionTable[_action].key = OIS::KC_X;
  else if(ss == "Y" || ss == "y") mActionTable[_action].key = OIS::KC_Y;
  else if(ss == "Z" || ss == "z") mActionTable[_action].key = OIS::KC_Z;
  //numerical keys (use shift)
  else if(ss == "0") mActionTable[_action].key = OIS::KC_0;
  else if(ss == "1") mActionTable[_action].key = OIS::KC_1;
  else if(ss == "2") mActionTable[_action].key = OIS::KC_2;
  else if(ss == "3") mActionTable[_action].key = OIS::KC_3;
  else if(ss == "4") mActionTable[_action].key = OIS::KC_4;
  else if(ss == "5") mActionTable[_action].key = OIS::KC_5;
  else if(ss == "6") mActionTable[_action].key = OIS::KC_6;
  else if(ss == "7") mActionTable[_action].key = OIS::KC_7;
  else if(ss == "8") mActionTable[_action].key = OIS::KC_8;
  else if(ss == "9") mActionTable[_action].key = OIS::KC_9;
  //default (non western keyboards aren't supported) :
  else mActionTable[_action].key = OIS::KC_UNASSIGNED;  //in case shift/ctrl/alt alone
  // flag special key
  mActionTable[_action].eShift = OBS::Shift(spec_keys);
}

void ObscurusInput::setMouseMode(OBS::MouseMode _mode)
{
  eMouseMode = _mode;
}

bool ObscurusInput::mouseMoved(const OIS::MouseEvent &arg)
{
  oMouse.oldx = oMouse.absx;
  oMouse.oldy = oMouse.absy;
  if(eMouseMode != OBS::OBSI_VIEW) // while in view, keep last cursor position
  {
    oMouse.absx = arg.state.X.abs;
    oMouse.absy = arg.state.Y.abs;
  }
  else
  {
    oMouse.relx = arg.state.X.rel;
    oMouse.rely = arg.state.Y.rel;
  }
  oMouse.delta = arg.state.Z.rel;

  /* fullscreen mode only : add sensibility here and clamp to screen in case the mouse exits :
  OIS::MouseState &ms = const_cast<OIS::MouseState &>(oisMouse->getMouseState());
  ms.X.abs = mousex_old = mousex;
  ms.Y.abs = mousey_old = mousey;
  */
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->mouseMoved(arg);
  return true;
}

bool ObscurusInput::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
  mouseb[id] = true;
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->mousePressed(arg, id);
  return true;
}

bool ObscurusInput::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
  mouseb[id] = false;
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->mouseReleased(arg, id);
  return true;
}

bool ObscurusInput::keyPressed(const OIS::KeyEvent &arg)
{
  keys[arg.key] = true;
  if(arg.key == OIS::KC_LCONTROL)       keys[OIS::KC_RCONTROL] = true;
  else if(arg.key == OIS::KC_RCONTROL)  keys[OIS::KC_LCONTROL] = true;
  else if(arg.key == OIS::KC_LSHIFT)    keys[OIS::KC_RSHIFT] = true;
  else if(arg.key == OIS::KC_RSHIFT)    keys[OIS::KC_LSHIFT] = true;
  else if(arg.key == OIS::KC_LMENU)     keys[OIS::KC_RMENU] = true;
  else if(arg.key == OIS::KC_RMENU)     keys[OIS::KC_LMENU] = true;
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->keyPressed(arg);
  return true;
}

bool ObscurusInput::keyReleased(const OIS::KeyEvent &arg)
{
  keys[arg.key] = false;
  if(arg.key == OIS::KC_LCONTROL)       keys[OIS::KC_RCONTROL] = false;
  else if(arg.key == OIS::KC_RCONTROL)  keys[OIS::KC_LCONTROL] = false;
  else if(arg.key == OIS::KC_LSHIFT)    keys[OIS::KC_RSHIFT] = false;
  else if(arg.key == OIS::KC_RSHIFT)    keys[OIS::KC_LSHIFT] = false;
  else if(arg.key == OIS::KC_LMENU)     keys[OIS::KC_RMENU] = false;
  else if(arg.key == OIS::KC_RMENU)     keys[OIS::KC_LMENU] = false;
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->keyReleased(arg);
  return true;
}
