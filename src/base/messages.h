#ifndef _OBS_LOGICS_MESSAGES_H_
#define _OBS_LOGICS_MESSAGES_H_
#include <string>

namespace OBS
{
  enum LogicDest
  {
    DEST_LOGICS,
    DEST_PLAYER,
    DEST_CORE,
    DEST_RENDERER,
    DEST_UNKNOWN
  };
  enum LogicMsgId
  {
    MSG_NULL,
    // LOGICS MESSAGES :
    MSG_LOG,
    MSG_SCRIPT_STATUS,
    // CORE MESSAGES :
    MSG_ABORT_EXECUTION,
    MSG_RETURN_TO_MAIN_MENU,
    MSG_START_GAME,
    // PLAYER MESSAGES :
    MSG_ASSIGN_CHARACTER,
    MSG_EMBODY_CHARACTER,
    // RENDERER MESSAGES :
    MSG_CELL_BUILT,
    // ---
    MSG_LAST_ID
  };
  struct LogicsMsg
  {
    LogicMsgId eId;
  };
  struct LogicsMsgLog : LogicsMsg
  {
    LogicsMsgLog(const std::string& _m, char _l) { eId = MSG_LOG; sMessage = _m; level = _l; }
    std::string sMessage;
    char level;
  };
  struct LogicsMsgAbortExecution : LogicsMsg
  {
    LogicsMsgAbortExecution() { eId = MSG_ABORT_EXECUTION; }
  };
  struct LogicsMsgReturnToMainMenu : LogicsMsg
  {
    LogicsMsgReturnToMainMenu() { eId = MSG_RETURN_TO_MAIN_MENU; }
  };
  struct LogicsMsgGameStart : LogicsMsg
  {
    LogicsMsgGameStart() { eId = MSG_START_GAME; }
  };
  struct LogicsMsgCellBuilt : LogicsMsg
  {
    LogicsMsgCellBuilt(short _x, short _y) { eId = MSG_CELL_BUILT; coordX = _x; coordY = _y; }
    short coordX, coordY;
  };
}
#endif