#include "logics.h"
#include "log.h"
#include "preferences.h"
template<> ObscurusLogics* Singleton<ObscurusLogics>::inst = 0;
ObscurusLogics* ObscurusLogics::getSingleton(){ return inst; }

ObscurusLogics::ObscurusLogics()
{
  gLog("-*-*- Logics Startup.", 'I');
  fCellTimeout = gPrefs->getAsFloat("CellTimeout"); if(fCellTimeout < 30.0f) fCellTimeout = 30.0f;
  // init capacities
  vEntities.reserve(1000);
  vWorlds.reserve(1);
  vCells.reserve(25);
  vScripts.reserve(10);
  vUsers.reserve(1);
}

ObscurusLogics::~ObscurusLogics()
{
  gLog("*-*-* Logics Shutdown.", 'I');
  // first, all threads must stop
  for(size_t i = 0; i < vScripts.size(); i++)
  {
    vScripts[i]->interrupt();
    vScripts[i]->oThread.join();
  }
  // now, kill 'em all
  for(size_t i = 0; i < vScripts.size(); i++)    { if(vScripts[i])  delete vScripts[i];  }
  for(size_t i = 0; i < vEntities.size(); i++)   { if(vEntities[i]) delete vEntities[i]; }
  for(size_t i = 0; i < vUsers.size(); i++)      { if(vUsers[i])    delete vUsers[i];    }
  for(size_t i = 0; i < vCells.size(); i++)      { if(vCells[i])    delete vCells[i];    }
  for(size_t i = 0; i < vWorlds.size(); i++)     { if(vWorlds[i])   delete vWorlds[i];   }
  for(size_t i = OBS::DEST_LOGICS; i < OBS::DEST_UNKNOWN; i++)
  {
    while(mMsgQueue[(OBS::LogicDest)i].size())
    {
      delete mMsgQueue[(OBS::LogicDest)i].front();
      mMsgQueue[(OBS::LogicDest)i].pop();
    }
  }
}

void ObscurusLogics::update()
{
  return;
  processMessages();
  // process timeout for all cells
  for(size_t i = 0; i < vCells.size(); i++)
  {
    if(vCells[i])
    {
      vCells[i]->timeOut += gTime;
      if(vCells[i]->timeOut > fCellTimeout)
      {
        // remove neighboring info
        for(size_t j = 0; j < vCells.size(); j++)
        {
          if(vCells[j])
          {
            for(size_t k = 0; k < OBS::DIR_LASTAZIMUT; k++)
            {
              if(vCells[j]->neighbors[k] == vCells[i])
                vCells[j]->neighbors[k] = 0;
            }
          }
        }
        // delete
        delete vCells[i];
        vCells[i] = 0;
      }
    }
  }
  // check activity zones
  for(size_t i = 0; i < vEntityActives.size(); i++)
  {
    if(vEntityActives[i])
    {
      // make sure the cells surrounding the entity are active
      OBS::Entity* entity = _getEntity(vEntityActives[i]); // vEntities[mEntityIds[vEntityActives[i]]];
      OBS::Cell* cell;
      // system coordinates, cell coords and image coords are (all) North+ and East+
      short cellX = entity->gridX / 1024;
      short cellY = entity->gridY / 1024;
      for(short y = cellY - 1; y < cellY + 2; y++)
      {
        for(short x = cellX - 1; x < cellX + 2; x++)
        {
          cell = vCells[_getCell(entity->worldName, x, y)]; // create if not exists
          cell->timeOut = 0.0f; // reset timeout
          if(cell->tube < OBS::CL_READY)
          {
            if(cell->state > OBS::CL_BUILDING && cell->tube < OBS::CL_UNWRAPPED)
            {
              cell->tube = OBS::CL_UNWRAPPED; // inform renderer to build renderable cell :
              addMsgToQueue(new OBS::LogicsMsgCellBuilt(x, y), OBS::DEST_RENDERER);
            }
            else if(cell->tube == OBS::CL_WRAPPED)
              cell->tube = OBS::CL_READY;
          }
        }
      }
      // set neighbors
      cell = vCells[_getCell(entity->worldName, cellX, cellY)];
      if(cell->state < OBS::CL_EDGING)
      {
        if(!cell->neighbors[OBS::DIR_NW])  cell->neighbors[OBS::DIR_NW] = vCells[_getCell(entity->worldName, (short)(cellX - 1), (short)(cellY + 1))];
        if(!cell->neighbors[OBS::DIR_N])  cell->neighbors[OBS::DIR_N] = vCells[_getCell(entity->worldName, cellX, (short)(cellY + 1))];
        if(!cell->neighbors[OBS::DIR_NE])  cell->neighbors[OBS::DIR_NE] = vCells[_getCell(entity->worldName, (short)(cellX + 1), (short)(cellY + 1))];
        if(!cell->neighbors[OBS::DIR_W])  cell->neighbors[OBS::DIR_W] = vCells[_getCell(entity->worldName, (short)(cellX - 1), cellY)];
        if(!cell->neighbors[OBS::DIR_E])  cell->neighbors[OBS::DIR_E] = vCells[_getCell(entity->worldName, (short)(cellX + 1), cellY)];
        if(!cell->neighbors[OBS::DIR_SW])  cell->neighbors[OBS::DIR_SW] = vCells[_getCell(entity->worldName, (short)(cellX - 1), (short)(cellY - 1))];
        if(!cell->neighbors[OBS::DIR_S])  cell->neighbors[OBS::DIR_S] = vCells[_getCell(entity->worldName, cellX, (short)(cellY - 1))];
        if(!cell->neighbors[OBS::DIR_SE])  cell->neighbors[OBS::DIR_SE] = vCells[_getCell(entity->worldName, (short)(cellX + 1), (short)(cellY - 1))];
        if(cell->state == OBS::CL_UNBUILT) cell->buildCell(); // use main thread in this case only
        cell->state = OBS::CL_EDGING;

        if(!cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_E])  cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_E] = cell->neighbors[OBS::DIR_N];
        if(!cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_SE])  cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_SE] = cell;
        if(!cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_S])  cell->neighbors[OBS::DIR_NW]->neighbors[OBS::DIR_S] = cell->neighbors[OBS::DIR_W];
        if(cell->neighbors[OBS::DIR_NW]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_NW]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_NW]);

        if(!cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_W])  cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_W] = cell->neighbors[OBS::DIR_N];
        if(!cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_SW])  cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_SW] = cell;
        if(!cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_S])  cell->neighbors[OBS::DIR_NE]->neighbors[OBS::DIR_S] = cell->neighbors[OBS::DIR_E];
        if(cell->neighbors[OBS::DIR_NE]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_NE]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_NE]);

        if(!cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_E])  cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_E] = cell->neighbors[OBS::DIR_S];
        if(!cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_NE])  cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_NE] = cell;
        if(!cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_N])  cell->neighbors[OBS::DIR_SW]->neighbors[OBS::DIR_N] = cell->neighbors[OBS::DIR_W];
        if(cell->neighbors[OBS::DIR_SW]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_SW]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_SW]);

        if(!cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_W])  cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_W] = cell->neighbors[OBS::DIR_S];
        if(!cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_NW])  cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_NW] = cell;
        if(!cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_N])  cell->neighbors[OBS::DIR_SE]->neighbors[OBS::DIR_N] = cell->neighbors[OBS::DIR_E];
        if(cell->neighbors[OBS::DIR_SE]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_SE]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_SE]);

        if(!cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_W])    cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_W] = cell->neighbors[OBS::DIR_NW];
        if(!cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_SW])  cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_SW] = cell->neighbors[OBS::DIR_W];
        if(!cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_S])    cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_S] = cell;
        if(!cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_SE])  cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_SE] = cell->neighbors[OBS::DIR_E];
        if(!cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_E])    cell->neighbors[OBS::DIR_N]->neighbors[OBS::DIR_E] = cell->neighbors[OBS::DIR_NE];
        if(cell->neighbors[OBS::DIR_N]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_N]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_N]);

        if(!cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_W])    cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_W] = cell->neighbors[OBS::DIR_SW];
        if(!cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_NW])  cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_NW] = cell->neighbors[OBS::DIR_W];
        if(!cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_N])    cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_N] = cell;
        if(!cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_NE])  cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_NE] = cell->neighbors[OBS::DIR_E];
        if(!cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_E])    cell->neighbors[OBS::DIR_S]->neighbors[OBS::DIR_E] = cell->neighbors[OBS::DIR_SE];
        if(cell->neighbors[OBS::DIR_S]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_S]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_S]);

        if(!cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_N])    cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_N] = cell->neighbors[OBS::DIR_NW];
        if(!cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_NE])  cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_NE] = cell->neighbors[OBS::DIR_N];
        if(!cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_E])    cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_E] = cell;
        if(!cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_SE])  cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_SE] = cell->neighbors[OBS::DIR_S];
        if(!cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_S])    cell->neighbors[OBS::DIR_W]->neighbors[OBS::DIR_S] = cell->neighbors[OBS::DIR_SW];
        if(cell->neighbors[OBS::DIR_W]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_W]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_W]);

        if(!cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_N])    cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_N] = cell->neighbors[OBS::DIR_NE];
        if(!cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_NW])  cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_NW] = cell->neighbors[OBS::DIR_N];
        if(!cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_W])    cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_W] = cell;
        if(!cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_SW])  cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_SW] = cell->neighbors[OBS::DIR_S];
        if(!cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_S])    cell->neighbors[OBS::DIR_E]->neighbors[OBS::DIR_S] = cell->neighbors[OBS::DIR_SE];
        if(cell->neighbors[OBS::DIR_E]->state == OBS::CL_UNBUILT) cell->neighbors[OBS::DIR_E]->thread = std::thread(&OBS::Cell::buildCell, cell->neighbors[OBS::DIR_E]);
      }
    }
  }
}

void ObscurusLogics::processMessages()
{
  OBS::LogicsMsg* pMsg = getMessage(OBS::DEST_LOGICS);
  if(pMsg)
  {
    if(pMsg->eId == OBS::MSG_LOG)
    {
      OBS::LogicsMsgLog* ppMsg = (OBS::LogicsMsgLog*) pMsg;
      gLog(ppMsg->sMessage, ppMsg->level);
    }
    if(pMsg->eId == OBS::MSG_SCRIPT_STATUS)
    {
    }
    else
      gLog("LOGICS : Bad message routing.", 'W');
    delete pMsg;
  }
}

OBS::LogicsMsg* ObscurusLogics::getMessage(OBS::LogicDest _dest)
{
  std::lock_guard<std::mutex> lock(mMsgQueue_mutex);
  // only one message per tick
  if(mMsgQueue[_dest].size())
  {
    mMsg[_dest] = mMsgQueue[_dest].front();
    mMsgQueue[_dest].pop();
    return mMsg[_dest];
  }
  return 0;
}

void ObscurusLogics::addMsgToQueue(OBS::LogicsMsg* _msg, OBS::LogicDest _dest)
{
  std::lock_guard<std::mutex> lock(mMsgQueue_mutex);
  mMsgQueue[_dest].push(_msg);
}

OBS::Script* ObscurusLogics::startScript(const std::string& _scriptName, bool _threaded)
{
  size_t scriptIdx = SIZE_MAX;
  OBS::Script* pScript;
  try
  {
    scriptIdx = mScriptNames.at(_scriptName);
    pScript = vScripts[scriptIdx];
  }
  catch(...)
  {
    pScript = new OBS::Script(_scriptName);
    LOCK_SCRIPTS;
    // find a free slot
    for(size_t i = 0; i < vScripts.size(); i++)
    {
      if(vScripts[i] == 0)
      {
        scriptIdx = i;
        vScripts[i] = pScript;
        mScriptNames[_scriptName] = i;
        break;
      }
    }
    if(scriptIdx == SIZE_MAX)
    {
      scriptIdx = vScripts.size();
      vScripts.push_back(pScript);
      mScriptNames[_scriptName] = scriptIdx;
    }
  }
  // the script will send a message when finish
  if(_threaded)
    pScript->exec();
  else
    pScript->call();
  return pScript;
}

void ObscurusLogics::loadGame()
{
  // global unique id counter must be set (entity.h)
}

void ObscurusLogics::registerUser(OBS::User* _user)
{
  LOCK_USERS;
  size_t userIdx = SIZE_MAX;
  // find a free slot
  for(size_t i = 0; i < vUsers.size(); i++)
  {
    if(vUsers[i] == 0)
    {
      userIdx = i;
      vUsers[i] = _user;
      vUserCharacters[i].clear();
      vUserCurrentCharacter[i] = SIZE_MAX;
      return;
    }
  }
  // no free slot, must increase size of the containers
  userIdx = vUsers.size();
  vUsers.push_back(_user);
  vUserCharacters.push_back({ 0 });
  vUserCurrentCharacter.push_back(0);
}

