#ifndef _OBS_TIME_H_
#define _OBS_TIME_H_

#include "singleton.h"
#include <chrono>

namespace OBS
{
  struct Calendar
  {
    short year;
    char month;
    char week;
    char day;
    char hour;
    char min;
    char sec;
    int iProtoYear;          // starting virtual year => year 1 from imperial calendar
    int iRealYear;          // starting real year => 2000/01/01 year
    unsigned char iSecInMin;    // number of seconds per virtual minute => timescale
    unsigned long iSecInHour;    // number of seconds per virtual hour
    unsigned long iSecInDay;    // number of seconds per virtual day
    unsigned long iSecInWeek;    // number of seconds per virtual week
    unsigned long iSecInMonth;    // number of seconds per virtual month
    unsigned long iSecInYear;    // number of seconds per virtual year
    void setProtoYear(int, int, unsigned char);
    long getTimestamp();
  };
}

class ObscurusTimer : public Singleton<ObscurusTimer>
{
public:
  ObscurusTimer();
  ~ObscurusTimer();
  static ObscurusTimer* ObscurusTimer::getSingleton();

  // application timing
  double getElapsed(bool _switch = false);
  double getAppTime()  { return dAppTime; }
  void setElapsed(double _t) { dElapsed = _t; }
  int getCurrentYear();

  // calendar methods
  //void getCalendarTime(ImperialTime*);

private:
  double dElapsed;
  double dAppTime;
  std::chrono::system_clock::time_point oTime;
};
#define gTimer  ObscurusTimer::getSingleton()
#define gTime    ObscurusTimer::getSingleton()->getElapsed()

#endif
