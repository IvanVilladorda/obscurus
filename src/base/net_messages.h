// -- NET_TYPES

#ifndef _OBS_NETWORK_TYPES_H_
#define _OBS_NETWORK_TYPES_H_

/// You should not edit the file MessageIdentifiers.h as it is a part of RakNet static library
/// To define your own message id, define an enum following the code example that follows. 
///
/// enum {
///   ID_MYPROJECT_MSG_1 = ID_USER_PACKET_ENUM,
///   ID_MYPROJECT_MSG_2, 
///    ... 
/// };
///
/// \note All these enumerations should be casted to (unsigned char) before writing them to RakNet::BitStream

namespace OBS
{
  enum NetMessageTypes
  {
    // NULL MSG
    NET_GAME_NULL = ID_USER_PACKET_ENUM,
  };
}

#endif
