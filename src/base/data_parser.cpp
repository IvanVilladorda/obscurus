#include "data.h"
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <algorithm>

using boost::tokenizer;
using boost::escaped_list_separator;

ObscurusDataParser::ObscurusDataParser(ZZIP_FILE* _stream, const std::string& _moduleName, const std::string& _fileName, const std::string& _pathName)
{
  pFile = _stream;
  sFile = _fileName;
  sModule = _moduleName;
  sPath = _pathName;
  nLine = 0;
}

bool ObscurusDataParser::getCommand()
{
  bool bEndComment = false;
  bool bParseFile = true;
  size_t nPos, nEnd;
  zzip_size_t nCount;
  char vBuffer[32];
  sCommand.erase();
  vParameters.clear();

  while(bParseFile)
  {
    // get a line
    std::string sLine;
    while(nCount = zzip_read(pFile, vBuffer, sizeof(vBuffer)-1))
    {
      vBuffer[nCount] = '\0';
      char* eol = strchr(vBuffer, '\n');
      if(eol)
      {
        long offset = (long)(eol - vBuffer + 1 - nCount);
        vBuffer[eol - vBuffer] = '\0';
        zzip_seek(pFile, offset, SEEK_CUR);
        *eol = '\0';
      }
      sLine += vBuffer;
      if(eol) break;
    }
    if(!nCount) return false; // nothing read, so EOF
    nLine++;

    // 0. ending multi lines comment
    if(bEndComment)
    {
      nEnd = sLine.find("*/");
      if(nEnd != std::string::npos)
      {
        sLine.erase(sLine.begin(), sLine.begin() + nEnd);
        bEndComment = false;
      }
      else continue;
    }
    else
    {
      // 1 : trim empty lines
      boost::trim_if(sLine, boost::is_any_of(" \n\r\t"));
      if(!sLine.length()) continue;

      // 2 : single line comments
      nPos = sLine.find("//");
      if(nPos != std::string::npos)
      {
        sLine.erase(sLine.begin() + nPos, sLine.end());
        boost::trim_if(sLine, boost::is_any_of(" \n\r\t"));
        if(!sLine.length()) continue;
      }

      // 3 : multi lines comments
      nPos = sLine.find("/*");
      if(nPos != std::string::npos)
      {
        // single lined ?
        nEnd = sLine.find("*/");
        if(nEnd != std::string::npos)
          sLine.erase(nPos, nEnd);
        else
        {
          sLine.erase(sLine.begin() + nPos, sLine.end());
          bEndComment = true;
        }
        boost::trim_if(sLine, boost::is_any_of(" \n\r\t"));
        if(!sLine.length()) continue;
      }

      // 4 : something has been found
      boost::replace_all(sLine, ",", " "); // replace all commas as space delimiters
      // eliminate multiple spaces
      sLine.erase(std::unique(std::begin(sLine), std::end(sLine), [](char c, char c2) {
        return ::isspace(c) && ::isspace(c2);
      }), std::end(sLine));
      tokenizer<escaped_list_separator<char>> tokens(sLine, escaped_list_separator<char>('\\', ' ', '\"'));
      sCommand = *tokens.begin();
      for(tokenizer<escaped_list_separator<char> >::iterator it = tokens.begin(); it != tokens.end(); ++it)
      {
        if(sCommand != *it) vParameters.push_back(*it);
      }

      // 5 : stop reading here
      bParseFile = false;
    }
  }
  return true;
}
