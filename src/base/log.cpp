// -- LOGGER
#include "log.h"
#include "preferences.h"
#include <ctime>
#include <cstdarg>
#if defined(_MSC_VER)
#  define snprintf _snprintf_s //vc tells it's more secure ; like windows is
#endif

template<> ObscurusLog* Singleton<ObscurusLog>::inst = 0;
ObscurusLog* ObscurusLog::getSingleton(){ return inst; }

ObscurusLog::ObscurusLog(const std::string& _wheretolog)
  : bDebug(false),
  bReady(false)
{
  memset(&vPath, 0, sizeof(vPath));
  memset(&vMsg, 0, sizeof(vMsg));
  memset(&vBuf, 0, sizeof(vBuf));
  memset(&vLvl, 0, sizeof(vLvl));
  memset(&vBufferD, 0, sizeof(vBufferD));
  memset(&vBufferT, 0, sizeof(vBufferT));
  memset(&vBuffer, 0, sizeof(vBuffer));
  strncpy(vPath, _wheretolog.c_str(), sizeof(vPath));
}

ObscurusLog::~ObscurusLog()
{
  gLog("*-*-* Log Shutdown.", 'I');
}

void ObscurusLog::init()
{
  bReady = true;
  // recreate file between executions
  FILE* file = fopen(vPath, "w");
  if(!file)  return;

  prepare();
  fprintf(file, "Log opened @ %s\n", vBuf);
  fclose(file);
}

void ObscurusLog::prepare(char _lvl)
{
  if(!bReady) init();

  switch(_lvl)
  {
    case 'i':
    case 'I': sprintf(vLvl, "INFO"); break;
    case 'w':
    case 'W': sprintf(vLvl, "WARN"); break;
    case 'e':
    case 'E': sprintf(vLvl, "ERRR"); break;
    case 'd':
    case 'D': sprintf(vLvl, "DEBG"); break;
    default:  sprintf(vLvl, "UNKN"); break;
  }

  time_t rawtime;
  struct tm * timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(vBufferD, sizeof(vBufferD), "%d/%m/%Y", timeinfo);
  strftime(vBufferT, sizeof(vBufferT), "%H:%M:%S", timeinfo);
  snprintf(vBuf, sizeof(vBuf), "%s - %s", vBufferD, vBufferT);
}

void ObscurusLog::log(const std::string& _message, char _lvl, ...)
{
  prepare(_lvl);
  if(_lvl == 'D' && !bDebug) return;

  // grab string
  strncpy(vMsg, _message.c_str(), sizeof(vMsg));
  va_list ap;
  va_start(ap, _lvl);
  vsnprintf(vBuffer, sizeof(vBuffer), vMsg, ap);
  va_end(ap);

  write();
}

void ObscurusLog::log(char* _message, char _lvl, ...)
{
  prepare(_lvl);
  if(_lvl == 'D' && !bDebug) return;

  // grab string
  va_list ap;
  va_start(ap, _lvl);
  vsnprintf(vBuffer, sizeof(vBuffer), _message, ap);
  va_end(ap);

  write();
}

void ObscurusLog::setDebugOutput(bool _mode)
{
  bDebug = _mode;
}

void ObscurusLog::write()
{
  for(size_t i = 0; i < vListeners.size(); i++)
    vListeners[i]->log(vLvl, vBuffer);
  FILE* file = fopen(vPath, "a");
  if(!file) return;
  fprintf(file, "%s [%s]: %s\n", vBuf, vLvl, vBuffer);
  fclose(file);
}

void ObscurusLog::addListener(ObscurusLogListener* _listener)
{
  vListeners.push_back(_listener);
}
void ObscurusLog::removeListener(ObscurusLogListener* _listener)
{
  for(size_t i = 0; i < vListeners.size(); i++)
  {
    if(vListeners[i] == _listener)
      vListeners.erase(vListeners.begin()+i);
  }
}
