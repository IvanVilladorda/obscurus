#ifndef _OBS_NETWORK_H_
#define _OBS_NETWORK_H_

#include "preferences.h"

#include "singleton.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h> // RAKNET MessageID
#include <BitStream.h>
#include <RakNetTypes.h>

#include "net_messages.h" // OBSCURUS MessageID

// default values
#define SERVER_PORT 65320
#ifdef _OBS_SERVER_
  #define MAX_CLIENTS 10
#else
  #define SERVER_ADDR "127.0.0.1"
#endif

class ObscurusNetwork : public Singleton<ObscurusNetwork>
{
public:
  ObscurusNetwork();
  ~ObscurusNetwork();
  static ObscurusNetwork* ObscurusNetwork::getSingleton();

  void connect();
  void listen();
  void talk();

private:
  RakNet::RakPeerInterface *peer;
  RakNet::Packet *packet;
};

#define gNetwork    ObscurusNetwork::getSingleton()

#endif