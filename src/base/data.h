#ifndef _OBS_DATA_H_
#define _OBS_DATA_H_
#include "singleton.h"
#include "data.objects.h" // parser + all objects headers

struct ObscurusMod
{
  std::string sName;
  std::vector<std::string> vFiles;
};

class ObscurusData : public Singleton<ObscurusData>
{
public:
  ObscurusData();
  ~ObscurusData();
  static ObscurusData* ObscurusData::getSingleton();

  void listMasterMods();
  void preloadModule(const std::string&, bool _restart = false);
  void preloadIndex(size_t);
  bool processLoad();
  float getLoadProgress();
  void loadNextFile();
  bool linkAllObjects();
  OBS::Object* get(OBS::ObjectType, const std::string&);
  OBS::Object* selectNullObject(OBS::ObjectType);
  ObscurusTerrainData* getTerrainByColor(unsigned char, unsigned char, unsigned char);
  ObscurusRegionData* getRegionByColor(unsigned char, unsigned char, unsigned char);
  std::string loadScript(const std::string&);

  std::string getStartScript();
  std::vector<std::string>  vMasterMods;
  std::vector<ObscurusMod>  vMods;

private:
  std::string getFileContents(const std::string&);
  OBS::Object* selectDataObject(OBS::ObjectType, const std::string&);

  size_t                    nCurrentMod;
  size_t                    nCurrentFile;
  size_t                    nItemsToLoad;
  size_t                    nItemsLoaded;
  std::string                sModDir;
  bool                      bLoading;

  std::map<OBS::ObjectType, std::map<std::string, OBS::Object*>> vObj;
  std::map<std::string, OBS::Object*>::iterator itFind;
};

#define gData    ObscurusData::getSingleton()

#endif
