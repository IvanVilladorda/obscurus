#ifndef _OBS_LOGICS_H_
#define _OBS_LOGICS_H_
#include "singleton.h"
#include "messages.h"
#include "data.h"
#include "db.h"
#include "logics/header.h"
#include <vector>
#include <queue>
#include <unordered_map>
#include <thread>
#include <mutex>

class ObscurusLogics : public Singleton<ObscurusLogics>
{
public:
  ObscurusLogics();
  ~ObscurusLogics();
  static ObscurusLogics* ObscurusLogics::getSingleton();

  OBS::Script* startScript(const std::string&, bool _threaded = true); // use startScript("GameStart") for a new game
  void loadGame();
  void registerUser(OBS::User*);
  void update();

  // OBJECTS CONTAINERS AND ACCESSES
public:
  std::vector<OBS::Entity*> vEntities;
  std::vector<OBS::World*> vWorlds;
  std::vector<OBS::Cell*> vCells;
  std::vector<OBS::Script*> vScripts;
  std::vector<OBS::User*> vUsers; // single player = 0
  std::mutex vEntities_mutex;
  std::mutex vWorlds_mutex;
  std::mutex vCells_mutex;
  std::mutex vScripts_mutex;
  std::mutex vUsers_mutex;
  std::mutex gData_mutex;
private:
  std::vector<long long> vEntityActives;
  std::unordered_map<long long, size_t> mEntityIds; // bridge over vEntities
  std::unordered_map<std::string, size_t> mWorldNames; // bridge over vWorlds
  std::unordered_map<std::string, size_t> mScriptNames; // bridge over vScripts
  std::unordered_map<std::string, size_t> mUserNames; // bridge over vUsers
  std::vector<std::vector<long long>> vUserCharacters;
  std::vector<long long> vUserCurrentCharacter;
  float fCellTimeout;

  // MESSAGE SYSTEM
public:
  void addMsgToQueue(OBS::LogicsMsg*, OBS::LogicDest);
  OBS::LogicsMsg* getMessage(OBS::LogicDest);
private:
  void processMessages();
  std::mutex mMsgQueue_mutex;
  std::map<OBS::LogicDest, std::queue<OBS::LogicsMsg*>> mMsgQueue;
  std::map<OBS::LogicDest, OBS::LogicsMsg*> mMsg; // copy

  // SCRIPTING SYSTEM
public:
  void _debugOutput(const std::string&);
  long long _createCharacter(const std::string&, const std::string&, int, int);
  void _grantCharacterToPlayer(long long, const std::string& = "PLAYER");
  void _forcePlayerToIncarnate(long long, const std::string& = "PLAYER");
  bool _isActive(long long);
  bool _isInActiveRange(int, int);
  size_t _loadWorld(const std::string&);
  void _startGame(const std::string&);
  size_t _getCell(const std::string&, short, short);
  size_t _getCell(const std::string& _w, int _x, int _y) { return _getCell(_w, (short)(_x / 1024), (short)(_y / 1024)); }
  size_t _getCell(const std::string& _w, double _x, double _y) { return _getCell(_w, (short)(_x/1024.0), (short)(_y/1024.0)); }
  OBS::Entity* _getEntity(long long);
  void _addEntity(OBS::Entity*);
};

#define gLogics    ObscurusLogics::getSingleton()
// should not use them...
#define LOCK_ENTITIES  std::lock_guard<std::mutex> lock_entities(gLogics->vEntities_mutex)
#define LOCK_CELLS     std::lock_guard<std::mutex> lock_cells(gLogics->vCells_mutex)
#define LOCK_WORLDS    std::lock_guard<std::mutex> lock_worlds(gLogics->vWorlds_mutex)
#define LOCK_SCRIPTS   std::lock_guard<std::mutex> lock_scripts(gLogics->vScripts_mutex)
#define LOCK_USERS     std::lock_guard<std::mutex> lock_users(gLogics->vUsers_mutex)
#define LOCK_DATA      std::lock_guard<std::mutex> lock_data(gLogics->gData_mutex)
#endif