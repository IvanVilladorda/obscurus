#ifndef _OBS_DATA_OBJECTS_H_
#define _OBS_DATA_OBJECTS_H_

// following headers
#include "data/data.object.h"
#include "data/data.script.h"
#include "data/data.declare.h"
#include "data/data.define.h"
#include "data/data.epoch.h"
#include "data/data.world.h"
#include "data/data.landmark.h"
#include "data/data.language.h"
#include "data/data.culture.h"
#include "data/data.character.h"
#include "data/data.marker.h"
#include "data/data.formula.h"
#include "data/data.soil.h"
#include "data/data.region.h"
#include "data/data.terrain.h"
#include "data/data.regionalterrain.h"

//#include "data/archetype.h"
//#include "data/item.h"
//#include "data/resource.h"
//#include "data/server.h"
//#include "data/tile.h"
//#include "data/transform.h"
//#include "data/user.h"
//#include "data/weather.h"

#endif