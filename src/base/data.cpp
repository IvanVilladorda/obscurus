#include "data.h"
#include "preferences.h"
#include "log.h"
#include <boost/filesystem.hpp>
#include <algorithm>
namespace fs = boost::filesystem;
template<> ObscurusData* Singleton<ObscurusData>::inst = 0;
ObscurusData* ObscurusData::getSingleton(){return inst;}

// -- CONSTRUCTOR
ObscurusData::ObscurusData()
{
  gLog("-*-*- Data Startup.", 'I');
  gLog("Data is using zZip %s.", 'I', ZZIP_VERSION);
  bLoading = false;
  // set base path
  sModDir = gPrefs->get("_FullPath") + "mod/";
  if(!fs::exists(sModDir))
    gLog("Modules directory (%s) does not exist.", 'E',sModDir.c_str());
  else
    gLog("Modules directory is : %s.", 'I', sModDir.c_str());
}

// -- DESTRUCTOR
ObscurusData::~ObscurusData()
{
  gLog("*-*-* Data Shutdown.", 'I');
  for(std::map<OBS::ObjectType, std::map<std::string, OBS::Object*>>::iterator it = vObj.begin(); it != vObj.end(); ++it)
  {
    for(std::map<std::string, OBS::Object*>::iterator it2 = vObj[it->first].begin(); it2 != vObj[it->first].end(); ++it2)
    {
      if(it2->second)
      {
        gLog("Deleting %s %s.", 'D', DataTypeNames(it2->second->type), it2->second->data_name.c_str());
        delete it2->second;
      }
    }
  }
}

// -- LISTMASTERMODS
void ObscurusData::listMasterMods()
{
  vMasterMods.clear();
  gLog("(Re)Building master modules list...", 'I');
  fs::path oModDir(sModDir);
  fs::directory_iterator end_iter;

  for(fs::directory_iterator dir_iter(oModDir); dir_iter != end_iter; ++dir_iter)
  {
    // we're looking for any zip or a directory containing a file named index.h and
    // where it is written : #start xxxxx
    bool bOk = false;
    std::string sModName;
    if(fs::is_directory(dir_iter->path()))
    {
      sModName = dir_iter->path().filename().generic_string();
      bOk = true;
    }
    else
    {
      std::string sExt = dir_iter->path().extension().generic_string();
      std::transform(sExt.begin(), sExt.end(), sExt.begin(), ::tolower);
      if(sExt == ".zip")
      {
        sModName = dir_iter->path().filename().generic_string().substr(0, dir_iter->path().filename().generic_string().length()-4);
        bOk = true;
      }
    }
    if(bOk)
    {
      std::string sIndex = sModDir + sModName + "/index.h";
      ZZIP_FILE* oZip = zzip_open(sIndex.c_str(), ZZIP_CASELESS | O_RDONLY | O_BINARY);
      if(oZip)
      {
        ObscurusDataParser parser(oZip, sModName, sIndex, sModDir);
        bool bFound = false;
        while(parser.getCommand() && !bFound)
        {
          if(parser.sCommand == "#start" && parser.vParameters.size() == 1)
          {
            gLog(" - found master mod %s", 'I', sModName.c_str());
            vMasterMods.push_back(sModName);
            bFound = true;
          }
        }
        zzip_close(oZip);
      } // -- oZip
    } // -- bOk
  } // -- for
}

// -- PRELOADMODULE
void ObscurusData::preloadModule(const std::string& _moduleName, bool _restart)
{
  if(_restart)
    vMods.clear();
  gLog("Preloading module %s...", 'I', _moduleName.c_str());
  std::string sIndex = sModDir + _moduleName + "/index.h";
  ZZIP_FILE* oZip = zzip_open(sIndex.c_str(), ZZIP_CASELESS | O_RDONLY | O_BINARY);
  if(!oZip)
  {
    gLog("No index.h in module %s.", 'E', _moduleName.c_str());
    return;
  }
  zzip_close(oZip);

  bool bAlreadyHere = false;
  for(size_t i = 0; i < vMods.size(); i++)
  {
    if(vMods[i].sName == _moduleName)
      bAlreadyHere = true;
  }
  if(!bAlreadyHere)
  {
    // do not reload a mod already loaded !
    size_t modIdx = vMods.size();
    ObscurusMod mod;
    mod.sName = _moduleName;
    vMods.push_back(mod);
    preloadIndex(modIdx);
  }
}

// -- PRELOADINDEX
void ObscurusData::preloadIndex(size_t _modIdx)
{
  std::string fileName(sModDir + vMods[_modIdx].sName + "/index.h");
  vMods[_modIdx].vFiles.push_back(fileName);
  ZZIP_FILE* oZip = zzip_open(fileName.c_str(), ZZIP_CASELESS | O_RDONLY | O_BINARY);
  if(oZip)
  {
    ObscurusDataParser parser(oZip, vMods[_modIdx].sName, fileName, sModDir);
    while(parser.getCommand())
    {
      if(parser.sCommand == "#require")
      {
        if(parser.vParameters[0].length())  preloadModule(parser.vParameters[0]);
        else  gLog("Inconsistent #require command at line %d in file %s.", 'W', parser.nLine, fileName.c_str());
      }
      else if(parser.sCommand == "#include")
      {
        if(parser.vParameters[0].length())
        {
          std::string sFileName = sModDir + vMods[_modIdx].sName + "/" + parser.vParameters[0];
          ZZIP_FILE* oInclude = zzip_open(sFileName.c_str(), ZZIP_CASELESS);
          if(oInclude)
          {
            vMods[_modIdx].vFiles.push_back(sFileName);
            zzip_close(oInclude);
          }
          else
            gLog("#include %s in file %s at line %d : file not found.", 'E', parser.vParameters[0].c_str(), fileName.c_str(), parser.nLine);
        }
        else
          gLog("Empty #include command at line %d in file %s.", 'W', parser.nLine, fileName.c_str());
      }
      else if(parser.sCommand == "#start") continue;
      else if(parser.sCommand == "#define") continue;
      else if(parser.sCommand == "#declare") continue;
      else if(!(((parser.sCommand.at(0)>='a') && (parser.sCommand.at(0)<='z')) || ((parser.sCommand.at(0)>='A') && (parser.sCommand.at(0)<'Z'))))
        gLog("Bad command at line %d in file %s.", 'W', parser.nLine, fileName.c_str());
    }
    zzip_close(oZip);
  }
}

// -- PROCESSLOAD
bool ObscurusData::processLoad()
{
  bLoading = true;
  nItemsToLoad = 0;
  // mods list
  gLog("Modules load order :", 'I');
  for(size_t i = 0; i < vMods.size(); i++)
    gLog("\t- %s", 'I', vMods[i].sName.c_str());
  // files list
  gLog("Files load order :", 'I');
  for(size_t j = 0; j < vMods.size(); j++)
  {
    for(size_t i = 0; i < vMods[j].vFiles.size(); i++)
    {
      nItemsToLoad++;
      gLog("\t- %s", 'I', vMods[j].vFiles[i].c_str());
    }
  }
  nItemsLoaded = 0;
  nCurrentMod = 0;
  nCurrentFile = 0;
  return true;
}

// -- GETLOADPROGRESS
float ObscurusData::getLoadProgress()
{
  if(nItemsLoaded >= nItemsToLoad) return 1.0f;
  return (float)nItemsLoaded / std::max(nItemsToLoad, size_t(1));
}

// -- LOADNEXTFILE
void ObscurusData::loadNextFile()
{
  if(vMods[nCurrentMod].vFiles.size() <= nCurrentFile)
  {
    nCurrentMod++;
    nCurrentFile = 0;
  }
  if(vMods.size() <= nCurrentMod) return;
  gLog("Parsing %s...", 'D', vMods[nCurrentMod].vFiles[nCurrentFile].c_str());

  OBS::Object* objIdx = 0;
  ZZIP_FILE* oFile = zzip_open(vMods[nCurrentMod].vFiles[nCurrentFile].c_str(), ZZIP_CASELESS | O_RDONLY | O_BINARY);
  if(oFile)
  {
    ObscurusDataParser parser(oFile, vMods[nCurrentMod].sName, vMods[nCurrentMod].vFiles[nCurrentFile], sModDir);
    while(parser.getCommand())
    {
      // make sure at least 1 parameter exists
      if(!parser.vParameters.size())
      {
        // ignore commands without parameters, but notify log
        gLog("Ignoring command \"%s\" without parameter at line %d in file %s.", 'W', parser.sCommand.c_str(), parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
        continue;
      }

      if(parser.sCommand == "#require")                            { objIdx = 0; continue; }
      else if(parser.sCommand == "#include")                       { objIdx = 0; continue; }
      else if(parser.sCommand == "#start")
      {
        objIdx = 0;
        if(!get(OBS::OBJ_SCRIPT, "GameStart"))
        {
          if(parser.vParameters.size() == 1)
          {
            gLog("#START directive found at line %d in file %s. Registering now.", 'I', parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
            std::string sStart = sModDir + vMods[nCurrentMod].sName + "/" + parser.vParameters[0];
            parser.vParameters.push_back(sStart);
            parser.vParameters[0] = "GameStart";
            objIdx = 0; selectDataObject(OBS::OBJ_SCRIPT, parser.vParameters[0])->interpret(&parser);
          }
          else
            gLog("#START directive found at line %d in file %s must point to a script. Ignoring.", 'E', parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
        }
        else
          gLog("Found already registered #START directive at line %d in file %s. Ignoring.", 'W', parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
      }
      else if(parser.sCommand == "#define" || parser.sCommand == "#global")  { objIdx = 0; selectDataObject(OBS::OBJ_DEFINE, parser.vParameters[0])->interpret(&parser); }
      else if(parser.sCommand == "#declare")                                 { objIdx = 0; selectDataObject(OBS::OBJ_DECLARE, parser.vParameters[0])->interpret(&parser); }
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_SCRIPT))
      {
        if(parser.vParameters[0] == "GameStart")
          gLog("Use of reserved name \"GameStart\" for script at line %d in file %s. Ignoring.", 'W', parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
        else
          objIdx = 0; selectDataObject(OBS::OBJ_SCRIPT, parser.vParameters[0])->interpret(&parser);
      }
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_EPOCH))            objIdx = selectDataObject(OBS::OBJ_EPOCH, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_WORLD))            objIdx = selectDataObject(OBS::OBJ_WORLD, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_CHARACTER))        objIdx = selectDataObject(OBS::OBJ_CHARACTER, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_MARKER))           objIdx = selectDataObject(OBS::OBJ_MARKER, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_LANDMARK))         objIdx = selectDataObject(OBS::OBJ_LANDMARK, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_CULTURE))          objIdx = selectDataObject(OBS::OBJ_CULTURE, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_LANGUAGE))         objIdx = selectDataObject(OBS::OBJ_LANGUAGE, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_TERRAIN))          objIdx = selectDataObject(OBS::OBJ_TERRAIN, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_SOIL))             objIdx = selectDataObject(OBS::OBJ_SOIL, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_FORMULA))          objIdx = selectDataObject(OBS::OBJ_FORMULA, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_WORLDREGION))      objIdx = selectDataObject(OBS::OBJ_WORLDREGION, parser.vParameters[0]);
      else if(parser.sCommand == DataTypeNames(OBS::OBJ_REGIONALTERRAIN))  objIdx = selectDataObject(OBS::OBJ_REGIONALTERRAIN, parser.vParameters[0]);
      else if(((parser.sCommand[0] >= 'a') && (parser.sCommand[0] <= 'z')) || ((parser.sCommand[0] >= 'A') && (parser.sCommand[0] < 'Z')))
      {
        if(objIdx)  objIdx->interpret(&parser);
        else        gLog("Ignoring line %d in file %s : not currently reading any object.", 'W', parser.sCommand.c_str(), parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
      }
      else
        gLog("Ignoring bad command \"%s\" at line %d in file %s.", 'W', parser.sCommand.c_str(), parser.nLine, vMods[nCurrentMod].vFiles[nCurrentFile].c_str());
    }
    nCurrentFile++;
    nItemsLoaded++;
    zzip_close(oFile);
  }
}

// -- SELECTNULLOBJECT
// manage null objects (special cases)
OBS::Object* ObscurusData::selectNullObject(OBS::ObjectType _objType)
{
  return selectDataObject(_objType, "NULL"); // null obj are default, unparsed and unlinked
}

  // -- SELECTDATAOBJECT
// this function creates an object if it is missing
OBS::Object* ObscurusData::selectDataObject(OBS::ObjectType _objType, const std::string& _objName)
{
  OBS::Object* obj = get(_objType, _objName);
  if(!obj)
  {
    // create it
    switch(_objType)
    {
    case OBS::OBJ_EPOCH:            obj = vObj[OBS::OBJ_EPOCH][_objName]            = new ObscurusEpochData(_objName); break;
    case OBS::OBJ_WORLD:            obj = vObj[OBS::OBJ_WORLD][_objName]            = new ObscurusWorldData(_objName); break;
    case OBS::OBJ_DEFINE:           obj = vObj[OBS::OBJ_DEFINE][_objName]           = new ObscurusDefineData(_objName); break;
    case OBS::OBJ_DECLARE:          obj = vObj[OBS::OBJ_DECLARE][_objName]          = new ObscurusDeclareData(_objName); break;
    case OBS::OBJ_SCRIPT:           obj = vObj[OBS::OBJ_SCRIPT][_objName]           = new ObscurusScriptData(_objName); break;
    case OBS::OBJ_CHARACTER:        obj = vObj[OBS::OBJ_CHARACTER][_objName]        = new ObscurusCharacterData(_objName); break;
    case OBS::OBJ_MARKER:           obj = vObj[OBS::OBJ_MARKER][_objName]           = new ObscurusMarkerData(_objName); break;
    case OBS::OBJ_LANDMARK:         obj = vObj[OBS::OBJ_LANDMARK][_objName]         = new ObscurusLandmarkData(_objName); break;
    case OBS::OBJ_CULTURE:          obj = vObj[OBS::OBJ_CULTURE][_objName]          = new ObscurusCultureData(_objName); break;
    case OBS::OBJ_LANGUAGE:         obj = vObj[OBS::OBJ_LANGUAGE][_objName]         = new ObscurusLanguageData(_objName); break;
    case OBS::OBJ_TERRAIN:          obj = vObj[OBS::OBJ_TERRAIN][_objName]          = new ObscurusTerrainData(_objName); break;
    case OBS::OBJ_SOIL:             obj = vObj[OBS::OBJ_SOIL][_objName]             = new ObscurusSoilData(_objName); break;
    case OBS::OBJ_FORMULA:          obj = vObj[OBS::OBJ_FORMULA][_objName]          = new ObscurusFormulaData(_objName); break;
    case OBS::OBJ_WORLDREGION:      obj = vObj[OBS::OBJ_WORLDREGION][_objName]      = new ObscurusRegionData(_objName); break;
    case OBS::OBJ_REGIONALTERRAIN:  obj = vObj[OBS::OBJ_REGIONALTERRAIN][_objName]  = new ObscurusRegionalTerrainData(_objName); break;
    default: gLog("Creating unknown object \"%s\" of type %s forbidden.", 'E', _objName.c_str(), DataTypeNames(_objType)); break;
    }
  }
  return obj;
}

// -- GET
OBS::Object* ObscurusData::get(OBS::ObjectType _objType, const std::string& _objName)
{
  itFind = vObj[_objType].find(_objName);
  if(itFind == vObj[_objType].end())
  {
    if(!bLoading)
      gLog("Object \"%s\" of type %s not found.", 'E', _objName.c_str(), DataTypeNames(_objType));
    return nullptr;
  }
  return itFind->second;
}

// -- LINKALLOBJECTS
bool ObscurusData::linkAllObjects()
{
  bLoading = false;
  bool bOk = true;
  gLog("Linking all data objects...", 'I');
  for(std::map<OBS::ObjectType, std::map<std::string, OBS::Object*>>::iterator it = vObj.begin(); it != vObj.end(); ++it)
  {
    gLog("Linking all %s objects...", 'I', DataTypeNames(it->first));
    for(std::map<std::string, OBS::Object*>::iterator it2 = vObj[it->first].begin(); it2 != vObj[it->first].end(); ++it2)
    {
      if(it2->second)
      {
        if(!it2->second->link())
          bOk = false;
      }
    }
  }
  return bOk;
}

// -- GETFILECONTENTS
std::string ObscurusData::getFileContents(const std::string& _fileName)
{
  std::string sContents;
  try
  {
    ZZIP_FILE* oFile = zzip_open(_fileName.c_str(), ZZIP_CASELESS | O_RDONLY | O_BINARY);
    if(oFile)
    {
      zzip_seek(oFile, 0, SEEK_END);
      sContents.resize(zzip_tell(oFile));
      zzip_seek(oFile, 0, SEEK_SET);
      zzip_read(oFile, &sContents[0], sContents.size());
      zzip_close(oFile);
    }
  }
  catch(...)
  {
    gLog("Error in get file contents for %s.", 'E', _fileName.c_str());
  }
  return(sContents);
}

// -- LOADSCRIPT
std::string ObscurusData::loadScript(const std::string& _scriptName)
{
  std::string sStr;
  ObscurusScriptData* _script = (ObscurusScriptData*)get(OBS::OBJ_SCRIPT, _scriptName);
  if(_script)
    sStr = getFileContents(_script->file_name);
  return sStr;
}

// -- GETTERRAINBYCOLOR
ObscurusTerrainData* ObscurusData::getTerrainByColor(unsigned char _r, unsigned char _g, unsigned char _b)
{
  ObscurusTerrainData* other = 0;
  unsigned char code[3];
  // truncate right
  code[0] = (_r >> 4) << 4;
  code[1] = (_g >> 4) << 4;
  code[2] = (_b >> 4) << 4;
  for(std::map<std::string, OBS::Object*>::iterator it = vObj[OBS::OBJ_TERRAIN].begin(); it != vObj[OBS::OBJ_TERRAIN].end(); ++it)
  {
    if(it->second)
    {
      ObscurusTerrainData* data = (ObscurusTerrainData*)it->second;
      if(data->color[0] == _r && data->color[1] == _g && data->color[2] == _b) return data;
      else if(data->color[0] == code[0] && data->color[1] == code[1] && data->color[2] == code[2]) other = data;
    }
  }
  if(other) return other; // returns terrain template without region
  return (ObscurusTerrainData*)selectNullObject(OBS::OBJ_TERRAIN);
}

// -- GETREGIONBYCOLOR
ObscurusRegionData* ObscurusData::getRegionByColor(unsigned char _r, unsigned char _g, unsigned char _b)
{
  unsigned char code[3];
  code[0] = _r ^ 0x10;
  code[1] = _g ^ 0x10;
  code[2] = _b ^ 0x10;
  for(std::map<std::string, OBS::Object*>::iterator it = vObj[OBS::OBJ_TERRAIN].begin(); it != vObj[OBS::OBJ_TERRAIN].end(); ++it)
  {
    if(it->second)
    {
      ObscurusRegionData* data = (ObscurusRegionData*)it->second;
      if(data->code[0] == code[0] && data->code[1] == code[1] && data->code[2] == code[2]) return data;
    }
  }
  return (ObscurusRegionData*)selectNullObject(OBS::OBJ_WORLDREGION);
}

namespace OBS
{
  void* getObjectIndexByName(ObjectType _objType, const std::string& _objName)
  {
    return (void*)gData->get(_objType, _objName);
  }
}
