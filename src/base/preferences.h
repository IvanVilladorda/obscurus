#ifndef _OBS_PREFS_H_
#define _OBS_PREFS_H_

#include "singleton.h"
#include <string>
#include <map>

inline bool operator== (const std::string& x, const std::string& y) { return strcmp(x.c_str(), y.c_str()) == 0; }
inline bool operator== (const std::string& x, const char* y)        { return strcmp(x.c_str(), y) == 0; }
inline bool operator== (const char* x, const std::string& y)        { return strcmp(x, y.c_str()) == 0; }
inline bool operator!= (const std::string& x, const std::string& y) { return strcmp(x.c_str(), y.c_str()) != 0; }
inline bool operator!= (const std::string& x, const char* y)        { return strcmp(x.c_str(), y) != 0; }
inline bool operator!= (const char* x, const std::string& y)        { return strcmp(x, y.c_str()) != 0; }
inline bool operator!  (const std::string& y)                       { return y.size() ? false: true; } // inverted due to if trigger (if(!)=>true)
inline bool operator== (const std::string& x, bool y)               { return y == (("true" == x | "1" == x) ? true : false); } // if x empty = false
inline bool operator!= (const std::string& x, bool y)               { return y != (("true" == x | "1" == x) ? true : false); }

//  Prefenreces starting with a _ are set during execution and not intended to be saved
class ObscurusPrefs : public Singleton<ObscurusPrefs>
{
public:
  ObscurusPrefs(const std::string& _cfgFileApp = "", const std::string& _cfgFileUser = "");
  ~ObscurusPrefs();
  static ObscurusPrefs* ObscurusPrefs::getSingleton();

  void loadConfigFile(const std::string&);
  void saveConfigFile(const std::string&);
  void set(const std::string&, const std::string&);
  std::string get(const std::string&);
  int getAsInt(const std::string&);
  long getAsLong(const std::string&);
  bool getAsBool(const std::string&);
  float getAsFloat(const std::string&);
  double getAsDouble(const std::string&);
  const char* getAsString(const std::string&);

private:
  std::map<std::string, std::string> vPreferences;
};
#define gPrefs    ObscurusPrefs::getSingleton()

#endif