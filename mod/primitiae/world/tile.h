/*
TILE TEMPLATE :
---------------
TILE        "a tile"										// string to retrieve this TILE elsewhere (or to update it from another mod)
	texture   "tx/world/a_tile.tga"				// path from mod's root to diffused texture
	bumpmap   "tx/world/a_tile_norm.tga"	// facultative, normal map used by this tile
	real_size 5														// the whole texture coverage in world units (meters)
	soil      ROCK,DIRT,MUD								// tile's ground composition (each tile will randomly receive only one)
	transform ROCK, "another tile"				// when terraced, this tile will transform to "another tile" if its soil is ROCK
																				// note that soil never changes by terracing, so if "another tile" has no ROCK soil, it will get it
*/


TILE        "none"
	texture   "tx/world/none.tga"
	real_size 5
	soil      SAND

TILE        "ice"
	texture   "tx/world/ice.tga"
	real_size 5
	soil      SAND

TILE        "ice rock"



