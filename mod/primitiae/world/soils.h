/*
SOILS CONSTANTS :
-----------------

Soils are ground types. They are mainly used to place resources and manage plants/trees/forests.
Also each tile must have at least one possible soil.
When modifying the terrain, soil's hardness (along its passable status) slow down terracing.

Soils are hardcoded. Use these constants (without quotes) to refer to them :
- DIRT    : soft soil
- ROCK    : very hard soil
- SAND    : very soft soil
- ROOTS   : medium hardness, trees and animals soil
- MUD     : wet soil
- ICE     : hard soil
- WATER   : underwater soil (water is present)
- LAVA    : melting soil (lava is present)
- ASH     : burned soil
- UNKNOWN : eternal infertile indestructible smoothed soil (out of reality)

*/
