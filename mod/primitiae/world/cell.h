/*
CELL TEMPLATE :
---------------

A CELL is a single pixel on the world map.
It represents a quad of 1024 meters and has elevation data.

CELL        "a cell"									// string to retrieve this CELL elsewhere (or to update it from another mod)
	color     100030										// hexadecimal (RGB) pixel color on ANY world map loaded
	altitude  32												// basic elevation in planetary unit (* world.elevation_factor = real *native elevation*)
	slope     0.5												// elevation slope against neighbouring cells while applying native elevation (default = 0.5)
	roughness 16												// factor applyied to relative elevation obtained by below formula, in meters
	formula   ridged(freq=2)						// formula to get *relative elevation* in range -1.0;+1.0, multiplied by roughness, then applyied to *native elevation*
	tile      "my tile"									// without parameters : default tile applyed to this cell, regardless elevation
	tile      "a tile", -9, 0						// this tile will be applyied to any *relative* elevation below zero
	tile      "the tile", -0.1, -0.125	// this tile will only be applyied to area of range 0.1;0.125, covering last one
																			// the number of tiles should be kept low; in case of overhelming, default tile will be applyed (see tile file)
	resource  "kee", 15, -0.5						// max 15 animal spawners will be placed on tile with soil ROOTS if relative elevation is around -0.5
	resource  "iron", 5, -0.1						// max 5 iron resources will be placed on tile with soil ROCK, if relative elevation is around -0.1
	liquid    "water", 1, 0							// max 1 water spawner will be placed on a tile with soil MUD at 0 elevation and then dig a river (or a lake) !BEWARE!
	liquid    "pool", 3, -0.8						// max 3 pools of stagnant water will dig around -0.8 on soil MUD
																			// see resource file to get information about soils, diggers and spawners

About native elevation :
  The field "altitude" describes the wished elevation for the cell.
  This is multiplied by WORLD.elevation_factor (see WORLD) to get *native elevation* in meters.
  This native elevation is then interpolated with surrounding CELLS to get a *native* shape of the area (mostly angular at this step).

About relative elevation :
  The field "formula" generates value between -1.0 and +1.0 for each possible point of the cell.
  These values are multiplied by "roughness" to get *relative elevation* at this point.
  If the roughness is 16, then relative elevation will be between -16.0 and +16.0 in meters.
  Note that tiles and resources use *unrough* relative elevation, not final elevation.

About final elevation :
  By adding the relative elevation to native elevation at a point, you get the *final elevation* in meters.
  So, if interpolated native elevation at a point is 160 meters, and formula * rougness gives -12.5, the final elevation at this point is : 160 + (-12.5) = 147.5 meters.

*/

CELL        "ocean"
	color     102060
	altitude  0
	slope     0.5
	roughness 0
	formula   constant, 0

CELL        "high icy mountains"
	color     F0F0F0
	altitude  255
	slope     0.75
	roughness 64
	formula   ridged, 1

CELL        "testformula"
	color     404040
	formula   select, -0.1, 0.4, perlin, 2, ridged, 5, billow, 1

