/*
RESOURCE TEMPLATE :
-------------------
RESOURCE							// directive, indexed in order of appeareance (if name does not already exist)
	name    "kee"				// string to retrieve this RESOURCE elsewhere (or to update it from another mod)
	type    SPAWN				// types : ANIMAL, PLANT, TREE, ORE
	entity  "bird kee"	// the name of the entity to generate
	soil    ROOTS				// soil type needed, soil types : DIRT, ROCK, SAND, ROOTS, MUD, ICE, WATER, LAVA, ASH, UNKNOWN

RESOURCE
	name   "iron"
	type   ORE
	entity "iron deposit"
	soil   ROCK

Resources can be added to CELLS and REGIONS (adding them to their cells, see regions file)
*/


