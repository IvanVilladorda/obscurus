/*
LIQUID TEMPLATE :
-----------------

LIQUID
	name   "water"
	type   RIVER
	liquid "blue water river"
	soil   MUD

LIQUID
	name   "pool"
	type   POND
	liquid "stagnant mud"
	soil   MUD
	expand 1
	dig    3

*/