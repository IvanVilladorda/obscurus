SELECT_WORLD "northern continent" // macro to set all instructions in this file relative to the designated world

MARK "point a"
	coords 15233, 541
//world "northern continent" // useless

MARK "point b"
	coords 238, -84

// intruction PATH makes a path and records all of its points (edited or not) in save
//   also alters terrain and place adequate meshes
//   the type (ROAD/RIVER/PATH/CARVE) determines the mode for altering terrain and finding the path
//     - ROAD : tries to make a flat road with meshes
//     - RIVER : tries to make a downward river with meshes
//     - PATH : simply smoothes a path between 2 points (same as ROAD, without meshes)
//     - CARVE : simply carves a path between 2 points (same as RIVER, without meshes)
//   if no path can be found (too far especially), nothing is generated
//   pathes are made from QUAD to QUAD, with resolution, to prepare pathfinding at best (using SQL LIKE on specialized world coordinates)
PATH "from a to b"
	type ROAD // textures are taken from CELL def
	from "point a" // a landmark
	to "point b"

