/*
REGION TEMPLATE :
-----------------

A REGION is a collection of cells.
It allows local meteorology and local resources.
The "NO REGION" REGION is hard coded to hold all unassigned cells.

REGION
	name ""
	color 020508
	

*/

REGION
