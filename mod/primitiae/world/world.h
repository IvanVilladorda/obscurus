

WORLD              "Northern Continent"
	world_map        "world/nc_world.tga"
	regions_map      "world/nc_regions.tga"
	default_tile     "none"
	origin           265, 174
	sea_level        0x10
	elevation_factor 8
