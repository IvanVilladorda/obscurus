// --
// PRIMITIAE PARABOLA INDEX
// --

// script to start a new game
#start "script/game_start.c"

// object data includes
#include "data/epoch.h"
#include "data/world.h"
#include "data/terrain.h"
#include "data/region.h"
#include "data/soil.h"
#include "data/formula.h"

#include "data/character.h"

