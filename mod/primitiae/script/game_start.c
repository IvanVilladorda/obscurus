// ----------------------------------------------------------------------------------------------
// SCRIPT "GAMESTART" :
// description : manage a new game launch
// ----------------------------------------------------------------------------------------------

DebugOutput() // will output this script as compiled text with the name script/game_start.c.cpl

pc = CreateCharacter("Hero", "PP_northern_continent")
//pc.sex = 1
//pc.name = "Test Boy"

//SetAttribute(pc, @STR, 9)

//SetFather(pc, otherpc1)
//SetMother(pc, otherpc2)
//SetCulture(pc, "Dahkir")
//SetBirthLandmark(pc, "ALandmark)

//SetPos(pc, 12345, 98765) // absolute positionning ; will update address
//SetAdr(pc, 3210, 1, 2, 3, 4, 5) // placing by address (3210th cell, 1st chunk, 2nd parcel, 3rd tile, 4th spot and 5th point) ; will update position

GrantCharacterToPlayer(pc)
ForcePlayerToIncarnate(pc)
