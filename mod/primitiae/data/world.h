// ----------------------------------------------------------------------------------------------
// WORLD :
// description : give a map to a world, set rough planetary data
// scheme :      OBJECT -> WORLD -> EPOCH
// ----------------------------------------------------------------------------------------------
// WORLD            "internal_name" : use this name in any object data
//  world_map        "path_to_file" : path to an image using terrains (also called "chunks map")
//  origin           number number  : pixel position on the map to set an origin of the world
//  sea_level        number         : level of the sea [0-255]
//  elevation_factor number         : multiplies heights to get them in meters
//  epoch            "name"         : name of an epoch to use
// ----------------------------------------------------------------------------------------------
// default values :
// ----------------------------------------------------------------------------------------------
//  world_map        ""
//  origin           0 0
//  sea_level        0
//  elevation_factor 1
//  epoch            ""
// ----------------------------------------------------------------------------------------------
WORLD "PP_northern_continent"
	world_map        "world/nc_map.png" // 
	origin           0 0                // south west corner
	sea_level        0x10               // 16 * 8 meters = sea is 128 meters deep
	elevation_factor 8                  // 8 meters * 255 = 2040 m - 128 m (sea) = 1912 m max
	epoch            "PP_imperial_460"  // see epoch.h

WORLD "test_World"
	world_map        "world/test.png" // 
	origin           0 0                // south west corner
	sea_level        0x10               // 16 * 8 meters = sea is 128 meters deep
	elevation_factor 8                  // 8 meters * 255 = 2040 m - 128 m (sea) = 1912 m max
	epoch            "PP_imperial_460"  // see epoch.h
