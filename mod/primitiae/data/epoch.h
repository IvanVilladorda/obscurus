// ----------------------------------------------------------------------------------------------
// EPOCH :
// description : determine the timescale, the time connection between our epoch and game's, 
//                 the number of days in a year the others chrono numbers (minutes in hour, etc.)
// scheme :      WORLD -> EPOCH (many worlds can use the same epoch)
// notice :      calendars (used by cultures) are defined elsewhere
// ----------------------------------------------------------------------------------------------
// EPOCH        "internal_name"      : use this name in any world data
//  proto_year   real_year game_year : link real date (as year only) with game dame (as year only)
//  secs_in_min  number              : set how many real seconds make one game minute (time scale)
//  mins_in_hour number              : set how many game seconds make one game hour
//  hours_in_day number              : set how many game hours make one game day
//  days_in_year number              : set how many game days complete one game year
// ----------------------------------------------------------------------------------------------
// default values :
// ----------------------------------------------------------------------------------------------
//  proto_year   2000 1
//  secs_in_min  5
//  mins_in_hour 60
//  hours_in_day 24
//  days_in_year 365
// ----------------------------------------------------------------------------------------------
EPOCH "PP_imperial_460"
	proto_year   2016 460 // means Jan 1 2016 (real) is first day of year 460 (game)
	secs_in_min  15       // 15 real seconds means 1 game minute
	mins_in_hour 50       // 50 game minutes make an hour
	hours_in_day 20       // 20 game hours make a day
	days_in_year 200      // there is 200 days in a year
	// one game year = 34 real days, 17 real hours and 20 real minutes
