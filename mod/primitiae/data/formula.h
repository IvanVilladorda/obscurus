// ----------------------------------------------------------------------------------------------
// FORMULA :
// description : to generate relative elevation
// ----------------------------------------------------------------------------------------------
// FORMULA       "internal_name"           : use this name in any world data
//  base_module  module                    : type : NULL, ABS, ADD, BILLOW, CLAMP, CONST, CURVE, MAX, MIN, MUL, PERLIN, POW, RIDGED, SCALE, SELECT, TERRACE, VORONOI
//  params       "param:value;param:value" : specific to base_module
//  module1      "name"                    : name of parent module 1
//  module2      "name"                    : name of parent module 2
//  module3      "name"                    : name of parent module 3
// ----------------------------------------------------------------------------------------------
// default values :
// ----------------------------------------------------------------------------------------------
//  base_module NULL
//  params      ""
//  module1     ""
//  module2     ""
//  module3     ""
// ----------------------------------------------------------------------------------------------
FORMULA "little_hills"
	base_module BILLOW
	params      "octaves:5;frequency:0.8;lacunarity:1.9;persistence:0.5"
	module1     ""
	module2     ""
	module3     ""
