// ----------------------------------------------------------------------------------------------
// SOIL :
// description : give soil, resources and textures to terrains
// ----------------------------------------------------------------------------------------------
// SOIL        "internal_name"      : use this name in any world data
//  proto_year   real_year game_year : link real date (as year only) with game dame (as year only)
//  secs_in_min  number              : set how many real seconds make one game minute (time scale)
//  mins_in_hour number              : set how many game seconds make one game hour
//  hours_in_day number              : set how many game hours make one game day
//  days_in_year number              : set how many game days complete one game year
// ----------------------------------------------------------------------------------------------
// default values :
// ----------------------------------------------------------------------------------------------
//  proto_year   2000 1
//  secs_in_min  5
//  mins_in_hour 60
//  hours_in_day 24
//  days_in_year 365
// ----------------------------------------------------------------------------------------------
