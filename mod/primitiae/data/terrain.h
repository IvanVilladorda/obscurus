// ----------------------------------------------------------------------------------------------
// TERRAIN :
// description : give data to ground cells
// ----------------------------------------------------------------------------------------------
// TERRAIN     "internal_name" : use this name in any object data
//  color      number          : hexadecimal RGB pixel color
//  elevation  number          : 8bits elevation (multiply it by world's elevation_factor)
//  slope      number          : relief strength [-2.0 - 2.0]
//  roughness  number          : formula's relief impact [1.0 - 64.0]
//  formula    "name"          : name of a formula to use
//  soil       "name"          : name of a soil to use
// ----------------------------------------------------------------------------------------------
// default values :
// ----------------------------------------------------------------------------------------------
//  color      0x000000
//  elevation  0
//  slope      0.0
//  roughness  1.0
//  formula    ""
//  soil       ""
// ----------------------------------------------------------------------------------------------
TERRAIN "Gen_meadow"
	color     0x70f070       // light-green
	elevation 0x15           // 168m (40m above sea)
	slope     1.0            // smooth
	roughness 4.0            // 8 meters variance on local relief
	formula   "little_hills" // see formula.h
	soil      "dirt"         // see soil.h

TERRAIN "Gen_beach"
	color     0xb0f0b0       // very light-green/yellow
	elevation 0x11           // 136m (8m above sea)
	slope     0.5            // very smooth
	roughness 1.0            // 2 meters variance on local relief
	formula   "creepy_hills" // see formula.h
	soil      "sand"         // see soil.h

TERRAIN "Gen_low_forest"
	color     0x50a050       // green
	elevation 0x17           // 184m (56m above sea)
	slope     1.0            // smooth
	roughness 2.0            // 4 meters variance on local relief
	formula   "creepy_ridge" // see formula.h
	soil      "root"         // see soil.h

TERRAIN "Gen_forest"
	color     0x407040       // low green
	elevation 0x20           // 256m (128m above sea)
	slope     1.5            // hard
	roughness 4.0            // 8 meters variance on local relief
	formula   "little_hills" // see formula.h
	soil      "root"         // see soil.h

TERRAIN "Gen_forest"
	color     0x407040       // low green
	elevation 0x20           // 256m (128m above sea)
	slope     1.5            // hard
	roughness 4.0            // 8 meters variance on local relief
	formula   "little_hills" // see formula.h
	soil      "root"         // see soil.h

TERRAIN "Gen_hills"
	color     0x806040       // light maroon
	elevation 0x22           // 272m (144m above sea)
	slope     1.8            // hard
	roughness 12.0           // 24 meters variance on local relief
	formula   "medium_hills" // see formula.h
	soil      "dirt"         // see soil.h

TERRAIN "Gen_low_mountains"
	color     0x804040       // light maroon
	elevation 0x27           // 312m (184m above sea)
	slope     1.2            // normal
	roughness 28.0           // 56 meters variance on local relief
	formula   "medium_ridge" // see formula.h
	soil      "rock"         // see soil.h

TERRAIN "Gen_med_mountains"
	color     0x604040       // maroon
	elevation 0x45           // 552m (424m above sea)
	slope     1.9            // hard
	roughness 36.0           // 72 meters variance on local relief
	formula   "medium_ridge" // see formula.h
	soil      "rock"         // see soil.h

TERRAIN "Gen_ocean"
	color     0x102060       // low blue
	elevation 0x00           // 0m (128m below sea)
	slope     1.0            // normal
	roughness 0.0            // 0 meters variance on local relief
	formula   ""             // see formula.h
	soil      "rock"         // see soil.h

TERRAIN "Low_city"
	color     0x505050       // gray
	elevation 0x11           // 136m (8m below sea)
	slope     0.0            // crush
	roughness 1.0            // 0 meters variance on local relief
	formula   ""             // see formula.h
	soil      "rock"         // see soil.h

TERRAIN "High_city"
	color     0x909090       // light gray
	elevation 0x13           // 136m (8m below sea)
	slope     1.0            // smooth
	roughness 1.0            // 0 meters variance on local relief
	formula   ""             // see formula.h
	soil      "rock"         // see soil.h

TERRAIN "Gen_end"
	color     0xF0F0F0       // white
	elevation 0xF0           // 
	slope     1.0            // smooth
	roughness 64.0           // 128 meters variance on local relief
	formula   ""             // see formula.h
	soil      "ice"          // see soil.h

