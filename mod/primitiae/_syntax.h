/*
SYNTAX GUIDE :
--------------

0. modules
--------------
A module is a collection of files located below the directory "dat/".
A module can be a zip file, or a subdirectory, or both.
If both exist, the engine tries to read a file from a subdirectory before trying from a zip file.
A module must present a file, named "index.h", located at its root.
This root is a directory named as the module.
	ex : dat/my module/index.h <= index located in a root subdirectory
		   dat/my module.zip/my module/index.h <= index located in a root zipped file
		 if both exist, the engine will read the first one and ignore the second one.

"index.h" is the main module descriptor; the place where all files needed to be read are registered.
This is also the place to put special commands (see below, 3. special commands).

1. lines
--------------
Lines are shaped as : [command] [parameters]
	ex : sea_level 16

If more than one parameter is needed, use commas or mere spaces.
	ex : origin 10, 20
		or origin 10 20
Commas make the line more readable in most cases.

If a parameter has to be specified in hexadecimal value, use 0x prefix.
	ex : colour 0xFF5020

If a parameter contains a space, use quotes.
	ex : texture "path/my texture.tga"
Using quotes for each characters string, event with no space enclosed, make the line more readable.

2. objects
--------------
Objects are introduced by a directive in UPPERCASE followed by a name.
	ex: WORLD "my world"
This name marks the object. If used elsewhere, like a redifinition of the object, it will update the same object, not recreate it.

If an object has to been specified from another object, use its name.
	ex: default_tile "my tile"

Since the reading of an object has begun, all following lines are considerered as fields of this object.
The end of the file or a new object declaration in UPPERCASE will stop the reading.
	ex : WORLD "my world"
	       origin 100
	       sea_level 1
	     WORLD "new world" <= stop reading "my world", begin new object
	       origin 10,10
	     WORLD "my world" <= stop reading "new world", edit previous object named "my world"
	       sea_level 20 <= "my world" contains now : origin 100,100 and sea_level 20
	       default_tile "my tile" <= TILE "my tile" must exist prior the reading of this line

3. special commands
--------------
They are preceeded by #. Their number is 4 :
- #require <= only usable in index.h
- #include <= only usable in index.h
- #define
- #declare

#require stops the reading of the module to read another one specified in parameter.
	ex : #require "master mod" <= the directory "dat/master mod/" or the file "dat/master mod.zip" must exists
This command includes the content of a mod exactly where #require is used.
Modules load order are therefore significant.
Using #require outside index.h produces nothing.

#include breaks the reading of index.h to read the file specified in parameter.
This specified file in parameter is part of the current module and its path start from the root's module.
	ex : #include "desc/objects.txt" <= read the file "dat/[current module]/desc/objects.txt"
Files included by #include describe objects and fields, or scripts.
File extension can be anything, but the file must be readable as text.
Using #include outside index.h produces nothing.

#define registers a value (or a string) as GLOBAL.
	ex #define base_water_level 21
These values are truely global : they can be used in other subsequent modules.
To use a global value, preceed its name by @.
	ex : sea_level @base_water_level <= sea_level 21
#define can be used outside index.h (however, this is bad pratice)
Global values can be redefined (also a bad pratice).
Beware, old values are not changed.
	ex : #define base_water_level 0 <= redefine base_water_level from 21 to 0
		 however, the previous sea_level command remains at 21.
#define does not accept global values
	ex : #define newDefineValue @base_water_level <= create a new global value named "newDefineValue" with value... "@base_water_level" (a string)

#declare registers a function as GLOBAL.
Functions are scripts ready to be "spawned" from any script.
	ex : #declare myFunction() "scripts/myFunction.txt"
Scripts can be attached to objects, functions cannot. But scripts can call functions, which spawn scripts.
Functions use parameters (no need to define them in the declare statement), scripts use none.
Script have full access to their object, functions have no direct access.
BUT functions have wide access to the engine core and can get any property from any object.
In brief, functions are a powerfull tool and a misuse of them can compromise the engine.
To spawn a function, use the following syntax within a script :
	ex : result = myFunction(parameter1, parameter2, 0);

*/


/*
INDEX.H TEMPLATE :
------------------

Each mod must provide this file intended to describe its contents.
This is the sole file allowing #'command' lines, which are :
- #require "MOD" : break current mod loading at this point to launch mod "MOD" loading
- #include "path/relative/to/mod/root/the-file.txt" : include the designated file at this point
- #declare "FUNCTION" "path/relative/to/mod/root/script.txt" : create a global function (affects all mods)
- #define "CONSTANT" [value/"string"] : create a global constant (affects all mods)
- #global "VARIABLE" [value/"string"] : create a global variable (affects all mods)
- #start "path/relative/to/mod/root/script.txt" : set the script to start a new game => mark mod as master
Pay attention to commands' order by including needed files/mods before using their contents. Otherwise, errors will rise up and break loading.

Example :
  #start "scripts/game_start.txt"
  #require "master_mod"
  #include "inc/file1.h"
  #declare shortDistance "scripts/shortDistance.cpp"
  #include "inc/file2.txt"
  #define MAX_elevation 4096
  #global QST_UMTT_status 15
  #define DEFAULT_texture "textures/default.tga"
  #include "inc/file3"
  #require "my_tweaks"

*/

// BASIC WORLD DATA
//#include "world/resource.h"
//#include "world/tile.h"
//#include "world/cell.h"
//#include "world/region.h"
//#include "world/world.h"

//#define default_world "my world" <= first world loaded otherwise
//#define default_pos_x X <= replace WORLD.origin(x) as reference to enter the world
//#define default_pos_y Y <= replace WORLD.origin(y) as reference to enter the world

// Provide a server to set this mod as online; for a client, defining a server is sufficient (other files will be downloaded)
//SERVER "server name"
//	ip "127.0.0.1"
//	port 23456

//#define default_world "Northern Continent"
//#define default_pos 1,1
