// REGIONS.H

/*
REGION		: region name
  world		: world name
  alpha		: alpha value on world map
  weather	: temperature, moisture, wind (+ wind direction as N, S, E, W, NW, NE, SW, SE) ex : 0.2,-0.5,-0.1,S
			repeat as many weathers as willed to shape a year (each takes a fraction equals to : 1/number weathers)
			first weather is the first in the year, last the last
  weather_force		: durability of each weather against world weather
  weather_change	: substitute weather in first parameter to the second
*/

REGION "Imperial Lowland"
  world          "Northern Continent"
  alpha          fa
  weather        -0.2, 0.3, -0.2, S //Winter
  weather         0.0, 0.2, -0.3, SE //Spring
  weather         0.2, 0.0, -0.4, E //Summer
  weather        -0.1, 0.1, -0.3, SE //Autumn
  weather_force  0.75
  weather_change "Blizzard", "Snow"

REGION "Evorn"
  world          "Northern Continent"
  alpha          245
