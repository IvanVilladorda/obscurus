// WEATHER.H

/*
Abstract table :

       | TEMP. | MOIS. | WIND. |

CLEAR     -       -        -
CLEAR     -       -        0
CLEAR     -       -        +
FOGGY     -       0        -
CLOUDY    -       0        0
OVERCAST  -       0        +
SNOW      -       +        -
SNOW      -       +        0
BLIZZARD  -       +        +

CLEAR     0       -        -
CLEAR     0       -        0
CLOUDY    0       -        +
CLOUDY    0       0        -
OVERCAST  0       0        0
RAIN      0       0        +
FOGGY     0       +        -
OVERCAST  0       +        0
RAIN      0       +        +

CLEAR     +       -        -
CLEAR     +       -        0
CLOUDY    +       -        +
CLOUDY    +       0        -
CLEAR     +       0        0
CLEAR     +       0        +
CLOUDY    +       +        -
OVERCAST  +       +        0
THUNDER   +       +        +
*/

// CLEAR : 9 occurences = 0.1
WEATHER "Clear"
  temperature_min -0.3
  temperature_max 0.4
  moisture_min -0.7
  moisture_max 0
  wind_min -0.3
  wind_max 0.2
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000
  script "scr/weather/clear.scr"

// FOGGY : 2 occurences = 0.5
WEATHER "Foggy"
  temperature_min -0.5
  temperature_max 0
  moisture_min 0
  moisture_max 0.5
  wind_min -1
  wind_max -0.5
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// CLOUDY : 6 occurences = 0.15
WEATHER "Cloudy"
  temperature_min -0.15
  temperature_max 0.45
  moisture_min -0.3
  moisture_max 0.15
  wind_min -0.45
  wind_max 0.3
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// OVERCAST : 4 occurences = 0.25
WEATHER "Overcast"
  temperature_min -0.25
  temperature_max 0.25
  moisture_min 0
  moisture_max 0.5
  wind_min 0
  wind_max 0.25
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// RAIN : 2 occurences = 0.5
WEATHER "Rain"
  temperature_min -0.25
  temperature_max 0.25
  moisture_min 0
  moisture_max 0.5
  wind_min 0.5
  wind_max 1
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// THUNDER : 1 occurence = 1
WEATHER "Thunder"
  temperature_min 0.5
  temperature_max 1
  moisture_min 0.5
  moisture_max 1
  wind_min 0.5
  wind_max 1
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// SNOW : 2 occurences = 0.5
WEATHER "Snow"
  temperature_min -1
  temperature_max -0.5
  moisture_min 0.5
  moisture_max 1
  wind_min 0
  wind_max 0.5
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// BLIZZARD : 1 occurence = 1
WEATHER "Blizzard"
  temperature_min -1
  temperature_max -0.5
  moisture_min 0.5
  moisture_max 1
  wind_min 0.5
  wind_max 1
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

// Special weathers (out of range)
WEATHER "Ash"
  temperature_min 10
  temperature_max 10
  moisture_min 10
  moisture_max 10
  wind_min 10
  wind_max 10
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

WEATHER "Sand"
  temperature_min 10
  temperature_max 10
  moisture_min 10
  moisture_max 10
  wind_min 10
  wind_max 10
  day_color ffffff
  night_color 000000
  sun_color ffffff
  fog_color ffffff
  fog_near 100
  fog_far 10000

