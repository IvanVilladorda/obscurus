// CELLTYPES.H

/*

*/

//--------------------------------------------- CLIMATE_NONE
CLIMATE "NONE"
color		102060
altitude	0

slope		0.5
roughness	0
formula		NUL(0.0)

default_square ""

//--------------------------------------------- CLIMATE_END
CLIMATE "END"
color		F0F0F0
altitude	255

slope		0.75
roughness	64
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_SUBEND
CLIMATE "SUBEND"
color		404040
altitude	240

slope		0.7
roughness	32
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_SEA
CLIMATE "SEA"
color		3080F0
altitude	8

slope		0.5
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_MOUNTS
CLIMATE "MOUNTS"
color		604040
altitude	200

slope		0.45
roughness	32
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_SNOWMOUNTS
CLIMATE "SNOWMOUNTS"
color		C0A0A0
altitude	200

slope		0.55
roughness	64
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_ICE
CLIMATE "ICE"
color		80E0E0
altitude	16

slope		0.5
roughness	16
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_THOUNDRA
CLIMATE "THOUNDRA"
color		B0C0D0
altitude	20

slope		0.5
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_POLAR
CLIMATE "POLAR"
color		A0C0E0
altitude	50

slope		0.65
roughness	12
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_ICELAND
CLIMATE "ICELAND"
color		405060
altitude	150

slope		0.85
roughness	16
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_NORDIC
CLIMATE "NORDIC"
color		806040
altitude	95

slope		0.6
roughness	32
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_DARK
CLIMATE "DARK"
color		103010
altitude	45

slope		0.2
roughness	32
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_ALPINE
CLIMATE "ALPINE"
color		407040
altitude	120

slope		0.9
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_TEMPERATE
CLIMATE "TEMPERATE"
color		70F070
altitude	40

slope		0.5
roughness	16
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_FROZENFOREST
CLIMATE "FROZENFOREST"
color		80C080
altitude	30

slope		0.55
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_FORBIDDENFOREST
CLIMATE "FORBIDDENFOREST"
color		E020E0
altitude	35

slope		0.55
roughness	6
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_FOREST
CLIMATE "FOREST"
color		50A050
altitude	40

slope		0.55
roughness	12
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_SOUTHFOREST
CLIMATE "SOUTHFOREST"
color		10C010
altitude	30

slope		0.5
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_IRON
CLIMATE "IRON"
color		705050
altitude	100

slope		0.4
roughness	48
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_HILL
CLIMATE "HILL"
color		B07040
altitude	75

slope		0.65
roughness	32
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_MEADOW
CLIMATE "MEADOW"
color		A0F0A0
altitude	25

slope		0.5
roughness	6
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_FARM
CLIMATE "FARM"
color		F0F0A0
altitude	20

slope		0.75
roughness	4
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_BEACH
CLIMATE "BEACH"
color		F0F060
altitude	18

slope		0.5
roughness	2
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_DARKSAND
CLIMATE "DARKSAND"
color		A0A020
altitude	24

slope		0.5
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_STEPPE
CLIMATE "STEPPE"
color		901010
altitude	28

slope		0.5
roughness	2
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_HIGHSTEPPE
CLIMATE "HIGHSTEPPE"
color		A01040
altitude	32

slope		0.55
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_SWAMP
CLIMATE "SWAMP"
color		405010
altitude	18

slope		0.5
roughness	8
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_WILD
CLIMATE "WILD"
color		D0B050
altitude	20

slope		0.5
roughness	4
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_PERIGEE
CLIMATE "PERIGEE"
color		000000
altitude	16

slope		0.5
roughness	0
formula		NUL(0.0)

//--------------------------------------------- CLIMATE_APOGEE
CLIMATE "APOGEE"
color		FFFFFF
altitude	255

slope		0.5
roughness	0
formula		NUL(0.0)
