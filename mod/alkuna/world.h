// WORLD.H

// IMPERATORIUM NORTH CONTINENT
WORLD "Northern Continent"
  map           "northern_continent.png"
  origin        265,174
  elevation_min -16
  elevation_max 4096
  cell_size     1024
  chunk_size    512
  tile_size     64
  square_size   8

  wind_freq       100
  wind            y
  moisture_freq   20
  moisture        cos(y*PI)/3 + x/3
  temperatur_freq 10
  temperature     y/2 - sin(x*PI)/2

  
