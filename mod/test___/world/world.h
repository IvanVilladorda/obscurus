// WORLD.H

// TEST LAND
WORLD              "Test Land"
	world_map        "world/testland_world.tga"
	regions_map      "world/testland_regions.tga"
	default_tile     "dirt"
	origin           265, 174
	sea_level        0x10
	elevation_factor 8
