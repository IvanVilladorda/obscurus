OBSCURUS MOD SPECIFICATION :
----------------------------

1. File System :
----------------
A mod is either a zip file or/and a sub-directory.
Example :
  ../mod/test/      => mod named "test"
  ../mod/mymod.zip  => mod named "mymod"
  ../mod/mymod/     => mod named "mymod"
  ../mod/bigmod.zip => mod named "bigmod"
In this example, there are 3 mods available : test, mymod and bigmod.

The one named "mymod" exists in both permitted forms.
Obscurus will interrogate FIRST the sub-directory to get a file,
THEN the zip file if it cannot find it in the directory.
That means the sub-directory gets priority level over the zip file.
You can then "mod a zip mod" by adding files in a sub-directory with the same name.

2. Structure :
--------------
You can organize a mod as you like, use any file extension and many directories inside it.
However, a special file must be present : index.h.
This file must be located on the root of the mod.
Without this file, the mod will not be recognized and ignored.

3. Index.h :
------------
All the command expected in index.h use the # prefix :
#require "another_mod"
  => tells to load the mod named "another_mod", use it to make dependencies
     "another_mod" will be loaded exactly where the #require directive is
#include "a_file.txt"
  => tells to interpret this file, use it to store data descriptions (as characters, for example)
     the file "a_file.txt" will be included exactly where the #include directive is
#start "a_script.txt"
  => tells to execute this particular script to start a new game, use it to mark your mod as MASTER
     A MASTER mod can be selected by the player to start a game.
     #start grants the player with an entity, located in a world.
     Obscurus needs to know this information to launch properly.
     You can make it simple (no menu, direct action) or complex (menus, character creation, and so on)
#define "pi" "3.14159265359"
  => define a global variable named "pi" with value "3.14159265359"
     to use this variable in a script, mark it as @pi, example :
     _round = 2 * @pi * _ray
     be warned you can modify it : @pi = 1.23456789 will work
#declare "special_function" "special/script.txt"
  => declare a global function named "special_function" executing "script.txt" in subdir "special"
     if this global function already exists, it will be replaced
     global function affects all mods loaded
     like constants, they must be called by adding a @ :
     @special_function( _arg1, 8, false)

4. Data Descriptors :
---------------------
Files included by #include contains descriptions of various data.
A basic example :
  WORLD "my world"
    map "textures/maps/my_world.png"
    sea_level 4
    elevation_factor 8
Creates a world named "my world",
 which chunk map is located at "textures/maps/my_world.png",
 where the elevation factor is x8,
 and, therefore, the sea level is 32 (4 x 8) meters above NullGround.
View existing descriptors to get an idea.
