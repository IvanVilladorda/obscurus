var images = new Array();
images[0]='img/loader_start.png';
images[1]='img/loader_end.png';
images[2]='img/loader_middle.png';
images[3]='img/logo_nxogre.png';
images[4]='img/logo_ogre.png';
images[5]='img/logo_openal.png';
images[6]='img/logo_physx.png';
images[7]='img/logo_raknet.png';
images[8]='img/logo_sukkubja.png';
images[9]='img/logo_vorbis.png';
images[10]='img/logo_chromium.png';
images[11]='img/alkuna_big.png';
images[12]='img/background_titre.jpg';
images[13]='img/story.png';
images[14]='img/main_backdiv.png';
images[15]='img/main_buttongfx.png';
images[16]='img/main_buttonkey.png';
images[17]='img/main_buttonlogin1.png';
images[18]='img/main_buttonquit1.png';
images[19]='img/main_buttonsnd.png';
images[20]='img/main_darkleft.png';
images[21]='img/background_titre.jpg';
images[22]='img/main_darkup.png';
images[23]='img/main_totem.jpg';
images[24]='img/alkuna.png';
	outputMsg('intro_ended\n');

var j = 0;
var w = 0;
var d = 0;
function onIntroEnded() {
	intro_ended();
	//event.preventDefault();
}
function interrupt() {
	//don't care
}
function _terminateIntro() {
	onIntroEnded();
}

function jumpme(num)
{
	$('loaderbar').morph('width: '+w+'px;', { duration : 0.1, transition: 'linear' });
	w += j;
	num++;
	if(num < images.length)	setTimeout("loadme("+num+")", 100);
	else
	{
		$('preloader').morph('opacity:0.0;', { duration : 1.0 });
		//setTimeout("Client.introReadyToStart()", 1000);
		//launch intro here (js file required)
		setTimeout("intro_begin()", 1000);
	}
}

function loadme(num)
{
	var imageObj = new Image();
	imageObj.src = images[num];
	imageObj.onLoad = jumpme(num);
}

function loadimages()
{
	j = Math.ceil(968 / images.length) - 0;
	w = $('loaderbar').style.width - 0;
	d = 0.05 * images.length * 15 - 0;
	$('preloader').style.display = 'block';
	loadme(0);
}

function calc(percent, W)
{
	var t = percent / 100;
	if(W)	return Math.round($('width').value * t);
	else	return Math.round($('height').value * t);
}

function intro_begin()
{
	onIntroEnded();
	$('preloader').style.display = 'none';

	// timeline du fichier ogg de test :
	// 10.469 + 10.848 + 11.559 + 12.316 = premier chapelet de notes
	setTimeout("logos_01()", 10469);
	setTimeout("logos_02()", 10848);
	setTimeout("logos_03()", 11559);
	setTimeout("logos_04()", 12316);

	// 20.725 + 21.341 + 22.028 + 22.596 = premier chapelet de notes
	setTimeout("logos_11()", 20720);
	setTimeout("logos_12()", 21335);
	setTimeout("logos_13()", 22020);
	setTimeout("logos_14()", 22590);

	// 25.000 = affichage background
	setTimeout("back()", 25000);

	// 28.328 - 33.847 = temps avant le titre
	setTimeout("story()", 26976);

	// 34.304 = titre
	setTimeout("title()", 34304);

	// 40.147 = flash blanc
	setTimeout("flash()", 39647);
}

var theW, theH;

function flash()
{
	$('whitescreen').setStyle('opacity: 0.0; display: block; z-order: 4');
	$('whitescreen').morph('opacity: 1.0;', { duration : 0.5, transition: 'linear' });
	$('whitescreen').morph('opacity: 0.0;', { duration : 5.5, transition: 'linear' });
	$('title_background').morph('opacity: 0.0; left: -125px;', { duration : 0.5, transition: 'linear' });
	$('title_front').morph('opacity: 0.0;', { duration : 0.5, transition: 'linear' });
}

function title()
{
	$('story').setStyle('display: none');
	$('title_front').setStyle('opacity: 0.0; display: block; z-order: 3');
	$('title_front').morph('opacity: 1.0;', { duration : 2.0, transition: 'linear' });
}

function back()
{
	$('title_background').setStyle('opacity: 0.0; display: block; z-order: 0; left: -250px;');
	$('title_background').morph('opacity: 1.0; left: -230px;', { duration : 2.5, transition: 'linear' });
	$('title_background').morph('left: -130px;', { duration : 12.6, transition: 'linear' });
}

function story()
{
	$('pastille_ogre').setStyle('display: none');
	$('pastille_sukkubja').setStyle('display: none');
	$('pastille_openal').setStyle('display: none');
	$('pastille_vorbis').setStyle('display: none');
	$('pastille_chromium').setStyle('display: none');
	$('pastille_raknet').setStyle('display: none');
	$('pastille_physx').setStyle('display: none');
	$('pastille_nxogre').setStyle('display: none');
	$('story').setStyle('opacity: 0.0; display: block; z-order: 2');
	$('story').morph('opacity: 1.0; margin-left: -180px;', { duration : 0.5, transition: 'linear' });
	$('story').morph('margin-left: -360px;', { duration : 4.5, transition: 'linear' });
	$('story').morph('opacity: 0.0; margin-left: -400px;', { duration : 1.0, transition: 'linear' });
}

function logos_01()
{
	$('pastille_ogre').setStyle('opacity: 0.0; display: block;top: '+calc(100,0)+'px; left: '+calc(100,1)+'px');
	$('pastille_sukkubja').setStyle('opacity: 0.0; display: block;top: '+(calc(100,0)-200)+'px; left: '+calc(100,1)+'px');
	theH = calc(50,0) - calc(1.25,0) - 200;
	theW = calc(50,1) - calc(7.5,1) - 400;
	$('pastille_ogre').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'swingTo' });
	theH = theH - 0 + 200 + calc(2.5,0);
	$('pastille_sukkubja').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'webkitCubic' });
}

function logos_02()
{
	$('pastille_openal').setStyle('opacity: 0.0; display: block;top: '+calc(70,0)+'px; left: '+calc(100,1)+'px');
	$('pastille_vorbis').setStyle('opacity: 0.0; display: block;top: '+calc(52.5,0)+'px; left: '+calc(100,1)+'px');
	theH = calc(50,0) - calc(1.25,0) - 200;
	theW = calc(50,1) - calc(2.5,1) - 200;
	$('pastille_openal').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'webkitCubic' });
	theH = theH - 0 + 200 + calc(2.5,0);
	$('pastille_vorbis').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'swingTo' });
}

function logos_03()
{
	$('pastille_chromium').setStyle('opacity: 0.0; display: block;top: '+calc(35,0)+'px; left: '+calc(100,1)+'px');
	$('pastille_raknet').setStyle('opacity: 0.0; display: block;top: '+calc(17.5,0)+'px; left: '+calc(100,1)+'px');
	theH = calc(50,0) - calc(1.25,0) - 200;
	theW = calc(50,1) - 0 + calc(2.5,1);
	$('pastille_chromium').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'swingTo' });
	theH = theH - 0 + 200 + calc(2.5,0);
	$('pastille_raknet').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'webkitCubic' });
}

function logos_04()
{
	$('pastille_physx').setStyle('opacity: 0.0; display: block;top: '+calc(0,0)+'px; left: '+calc(100,1)+'px');
	$('pastille_nxogre').setStyle('opacity: 0.0; display: block;top: '+(calc(0,0)-200)+'px; left: '+calc(100,1)+'px');
	theH = calc(50,0) - calc(1.25,0) - 200;
	theW = calc(50,1) - 0 + calc(7.5,1) + 200;
	$('pastille_physx').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'webkitCubic' });
	theH = theH - 0 + 200 + calc(2.5,0);
	$('pastille_nxogre').morph('opacity: 1.0; top: '+theH+'px; left: '+theW+'px;', { duration : 0.75, transition: 'swingTo' });
}

function logos_11()
{
	theH = calc(100,0);
	$('pastille_ogre').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(100,1)+'px;', { duration : 0.75, transition: 'easeInBack' });
	$('pastille_sukkubja').morph('opacity: 0.0; top: '+theH+'px; left: '+(calc(100,1)-200)+'px;', { duration : 0.75, transition: 'swingFromTo' });
}

function logos_12()
{
	$('pastille_openal').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(70,1)+'px;', { duration : 0.75, transition: 'easeInBack' });
	$('pastille_vorbis').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(52.5,1)+'px;', { duration : 0.75, transition: 'swingFromTo' });
}

function logos_13()
{
	$('pastille_chromium').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(35,1)+'px;', { duration : 0.75, transition: 'easeInBack' });
	$('pastille_raknet').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(17.5,1)+'px;', { duration : 0.75, transition: 'swingFromTo' });
}

function logos_14()
{
	$('pastille_physx').morph('opacity: 0.0; top: '+theH+'px; left: '+calc(0,1)+'px;', { duration : 0.75, transition: 'easeInBack' });
	$('pastille_nxogre').morph('opacity: 0.0; top: '+theH+'px; left: '+(calc(0,1)-200)+'px;', { duration : 0.75, transition: 'swingFromTo' });
}
