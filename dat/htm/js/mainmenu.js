var es = new Array();
es['off_login'] = 0;
es['off_gfx'] = 0;
es['off_snd'] = 0;
es['off_key'] = 0;
es['off_quit'] = 0;
es['on_login'] = 1;
es['on_gfx'] = 0;
es['on_snd'] = 0;
es['on_key'] = 0;

function returntomenu()
{
	$('div_login_default').style.display = 'none';
	$('div_login_ingame').style.display = 'block';
}

function Brille(elgg)
{
	if(es['off_'+elgg] == 0)
	{
		es['off_'+elgg] = 1;
		var h = Math.round($('height').value / 10)+'px;';
		$('img_'+elgg).morph('opacity: 1.0; height: '+h+'; width: '+h, { duration : 0.1, transition: 'easeFromTo' });
	}
}

function Fonce(elgg)
{
	if(es['off_'+elgg] == 1)
	{
		es['off_'+elgg] = 0;
		var h = Math.round($('height').value / 12)+'px;';
		$('img_'+elgg).morph('opacity: 0.5; height: '+h+'; width: '+h, { duration : 0.5, transition: 'easeFromTo' });
	}
}

function _Rebat(who)
{
	es['on_login'] = 0;
	es['on_gfx'] = 0;
	es['on_snd'] = 0;
	es['on_key'] = 0;
	es['on_'+who] = 1;
}

function Scritch(who, how)
{
	if(es['on_'+who] == 0)
	{
		es['on_login'] = 1;
		es['on_gfx'] = 1;
		es['on_snd'] = 1;
		es['on_key'] = 1;
		//signal snd
		var t = how * (-100);
		$('totem').morph('top: '+t+'%;', { duration : 2.0, transition: 'easeFromTo' });
		setTimeout("_Rebat('"+who+"')", 2000);
		//Client.menuRumble();
	}
}

function start()
{
	var h;
	$('errormsgcon').setStyle('opacity: 0.0;');
	$('errormsgkey').setStyle('opacity: 0.0;');
	$('div_login_ingame').setStyle('display: none;');
	h = Math.round($('height').value / 10)+'px;';
	$('img_login').setStyle('opacity: 1.0; height: '+h+'; width: '+h);
	$('img_gfx').setStyle('opacity: 1.0; height: '+h+'; width: '+h);
	$('img_snd').setStyle('opacity: 1.0; height: '+h+'; width: '+h);
	$('img_key').setStyle('opacity: 1.0; height: '+h+'; width: '+h);
	$('img_quit').setStyle('opacity: 1.0; height: '+h+'; width: '+h);
	h = Math.round($('height').value / 12)+'px;';
	$('img_login').setStyle('opacity: 0.5; height: '+h+'; width: '+h);
	$('img_gfx').setStyle('opacity: 0.5; height: '+h+'; width: '+h);
	$('img_snd').setStyle('opacity: 0.5; height: '+h+'; width: '+h);
	$('img_key').setStyle('opacity: 0.5; height: '+h+'; width: '+h);
	$('img_quit').setStyle('opacity: 0.5; height: '+h+'; width: '+h);
	$('totem').setStyle('top: 0px;');
}

function init()
{
	$('img_totem').setStyle('width: '+Math.round(0.85 * $('width').value)+'px; height: '+($('height').value*4)+'px');
	$('img_darkup').setStyle('width: '+Math.round(2 * $('width').value / 3)+'px; height: '+Math.round($('height').value / 4)+'px');
	$('img_darkleft').setStyle('width: '+Math.round($('width').value/3)+'px; height: '+$('height').value+'px');
	setTimeout("start()", 500);
	//Client.menuReady();
}

function processlang()
{
	//cette fonction est appel� par l'application
	//Indexes, LangKeys et LangValues en sont issus
	if(!Indexes)	return;
	for(i=0; i<Indexes; i++)
	{
		$(langKeys[i]).innerHTML = langValues[i];
	}
}

function quit()
{
	sendCfg();
	//setTimeout("Client.menuQuit()", 1000);
}
function sendCfg()
{
	var fullscreen;
	if($('gfx_fullscreen1').checked)	fullscreen = "1";
	else								fullscreen = "0";
	/*Client.menuCfgDelivery(
		// Login
			$('id').value,
			$('pass').value,
		// Gfx
			fullscreen,
			$('gfx_resolutions').value,
			$('gfx_vsync').value,
		// Snd
		// Key
			$('key_mouse_sens').value,
			$('key_mouse_invert').value,
			$('key_move_forward').value,
			$('key_move_backward').value,
			$('key_move_left').value,
			$('key_move_right').value,
			$('key_run').value,
			$('key_jump').value,
			$('key_sneak').value,
			$('key_attack').value,
			$('key_draw_weapon').value,
			$('key_draw_tool').value,
			$('key_previous_draw').value,
			$('key_next_draw').value,
			$('key_activate').value,
			$('key_console').value,
			$('key_inventory').value,
			$('key_infos').value,
			$('key_map').value,
			$('key_switch_3rd').value
		);*/
}

function checkitnow(i)
{
	var ok = false;
	// keys are ready ?
	if($('key_move_forward').value == '')			ok = false;
	else if($('key_move_backward').value == '')	ok = false;
	else if($('key_move_left').value == '')		ok = false;
	else if($('key_move_right').value == '')		ok = false;
	else if($('key_run').value == '')				ok = false;
	else if($('key_jump').value == '')				ok = false;
	else if($('key_sneak').value == '')			ok = false;
	else if($('key_attack').value == '')			ok = false;
	else if($('key_draw_weapon').value == '')		ok = false;
	else if($('key_draw_tool').value == '')		ok = false;
	else if($('key_previous_draw').value == '')	ok = false;
	else if($('key_next_draw').value == '')		ok = false;
	else if($('key_activate').value == '')			ok = false;
	else if($('key_console').value == '')			ok = false;
	else if($('key_inventory').value == '')		ok = false;
	else if($('key_infos').value == '')			ok = false;
	else if($('key_map').value == '')				ok = false;
	else if($('key_switch_3rd').value == '')		ok = false;
	else											ok = true;

	if(ok == false)
	{
		$('errormsgkey').setStyle('opacity: 1.0;');
		$('errormsgkey').morph('opacity: 0.0;', { duration : 10, transition: 'easeFromTo' });
	}
	else
	{
		sendCfg();
//		if(i)	setTimeout("Client.menuConnect()", 1000);
//		else	setTimeout("Client.menuOffline()", 1000);
	}

	return ok;
}

var act = 0;
var tmp = "";

function sendwheel(i)
{
	// pour cause de bug, la molette est g�r�e depuis le Client
	if(act)
	{
		if(i < 0)	$(act).value = "wheel-"; 
		else		$(act).value = "wheel+";
	}
}

function kp(e)
{
	if(!act)	return false;
	if(e.ctrlKey) tmp = "ctrl+";
	else if(e.shiftKey) tmp = "shift+";
	else if(e.altKey) tmp = "alt+";
	else tmp = "";
	var charCode = (e.which) ? e.which : event.keyCode
	$(act).value = String.fromCharCode(charCode);
	 if (charCode == 8) $(act).value = tmp + "backspace"; //  backspace
	 if (charCode == 9) $(act).value = tmp + "tab"; //  tab
	 if (charCode == 13) $(act).value = tmp + "enter"; //  enter
	 if (charCode == 19) $(act).value = tmp + "pause"; //  pause/break
	 if (charCode == 20) $(act).value = tmp + "caps lock"; //  caps lock
	 if (charCode == 32) $(act).value = tmp + "space"; //  space
	 if (charCode == 33) $(act).value = tmp + "pg up"; // page up
	 if (charCode == 34) $(act).value = tmp + "pg down"; // page down
	 if (charCode == 35) $(act).value = tmp + "end"; // end
	 if (charCode == 36) $(act).value = tmp + "home"; // home
	 if (charCode == 37) $(act).value = tmp + "left"; // left arrow
	 if (charCode == 38) $(act).value = tmp + "up"; // up arrow
	 if (charCode == 39) $(act).value = tmp + "righ"; // right arrow
	 if (charCode == 40) $(act).value = tmp + "down"; // down arrow
	 if (charCode == 45) $(act).value = tmp + "insert"; // insert
	 if (charCode == 46) $(act).value = tmp + "delete"; // delete
	 if (charCode == 96) $(act).value = tmp + "numpad 0"; // numpad 0
	 if (charCode == 97) $(act).value = tmp + "numpad 1"; // numpad 1
	 if (charCode == 98) $(act).value = tmp + "numpad 2"; // numpad 2
	 if (charCode == 99) $(act).value = tmp + "numpad 3"; // numpad 3
	 if (charCode == 100) $(act).value = tmp + "numpad 4"; // numpad 4
	 if (charCode == 101) $(act).value = tmp + "numpad 5"; // numpad 5
	 if (charCode == 102) $(act).value = tmp + "numpad 6"; // numpad 6
	 if (charCode == 103) $(act).value = tmp + "numpad 7"; // numpad 7
	 if (charCode == 104) $(act).value = tmp + "numpad 8"; // numpad 8
	 if (charCode == 105) $(act).value = tmp + "numpad 9"; // numpad 9
	 if (charCode == 106) $(act).value = tmp + "*"; // multiply
	 if (charCode == 107) $(act).value = tmp + "+"; // add
	 if (charCode == 109) $(act).value = tmp + "-"; // subtract
	 if (charCode == 110) $(act).value = tmp + "numpad ."; // decimal point
	 if (charCode == 111) $(act).value = tmp + "/"; // divide
	 if (charCode == 144) $(act).value = tmp + "num lock"; // num lock
	 if (charCode == 145) $(act).value = tmp + "scroll lock"; // scroll lock
	 if (charCode == 186) $(act).value = tmp + ";"; // semi-colon
	 if (charCode == 187) $(act).value = tmp + "="; // equal-sign
	 if (charCode == 188) $(act).value = tmp + ","; // comma
	 if (charCode == 189) $(act).value = tmp + "-"; // dash
	 if (charCode == 190) $(act).value = tmp + "."; // period
	 if (charCode == 191) $(act).value = tmp + "/"; // forward slash
	 if (charCode == 192) $(act).value = tmp + "`"; // grave accent
	 if (charCode == 219) $(act).value = tmp + "["; // open bracket
	 if (charCode == 220) $(act).value = tmp + "\\"; // back slash
	 if (charCode == 221) $(act).value = tmp + "]"; // close bracket
	e.stopPropagation();
	e.preventDefault();
}

function md(e)
{
	if(!act)	return false;
	var button;
	if (event.which == null)	button = (event.button < 2) ? "left click" : ((event.button == 4) ? "middle click" : "right click");
	else						button = (event.which < 2) ? "left click" : ((event.which == 2) ? "middle click" : "right click");
	$(act).value = button;
	e.stopPropagation();
	e.preventDefault();
}

function KeyConfigDest(dest)
{
	//onblur
	if(dest == 0)
	{
		if(act)
		{
			$(act).className = "key_stroke";
			$(act).removeEventListener('keypress', kp, false);
			$(act).removeEventListener('mousedown', md, false);
			act = 0;
			tmp = "";
		}
	}
	else
	{
		act = dest;
		$(act).className = "key_TOstroke";
		$(act).addEventListener('keydown', kp, false);
		$(act).addEventListener('mousedown', md, false);
	}
}
